<?php
namespace api\modules\v1;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\v1\controllers';

    public function init()
    {
        parent::init();
    // initialize the module with the configuration loaded from config.php
   // \Yii::configure($this, require(__DIR__ . '/config.php'));
        
        \Yii::$app->user->enableSession = false;  
    }
}
