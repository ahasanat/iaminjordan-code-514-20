<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use frontend\models\CountryCode;



/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class CountryController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Country';
    
    public function actionSearch()
	{
       $new_user_id = $_GET['_id'];
       $result = CountryCode::find()->where(['_id' => $_GET['_id']])->one();
       return $result;
    }
    
    public function actionAllcountry()
	{
       $result = array();
       $result['status'] = true;
       $result['statusMsg'] = "Country List";
       $result['data'] = CountryCode::find()->all();
       return $result;
    }
    
    /*public function actionCreatecountry(){
        $country = new Country();
        $result = array();
        $country->code = $_POST['code'];
        $country->name = $_POST['name'];
        $country->population = $_POST['population'];
        $country->insert();
        $result['status'] = true;
        $result['statusMsg'] = "Country Added successfully";
        $result['data'] = Country::find()->all();
       
        return $result;
    }
    
    public function actionUpdatecountry(){
        $country = new Country();
        $result = array();
        $record = Country::find()->where(['_id' => $_POST['_id']])->one();
             if(!empty($record)){
                $record->name = $_POST['name'];
                $record->update();
             }
        $result['status'] = true;
        $result['statusMsg'] = "Country Updated successfully";
        $result['data'] = Country::find()->where(['_id' => $_POST['_id']])->one();
       
        return $result;
    }*/
}


