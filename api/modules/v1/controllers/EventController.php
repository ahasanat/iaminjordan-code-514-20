<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Expression;
use yii\web\UploadedFile;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use frontend\models\PageEvents;
use frontend\models\EventVisitors;
use frontend\models\PostForm;
use frontend\models\Notification;
/**
 * Site controller
 */
class EventController extends ActiveController {

    public $modelClass = 'api\modules\v1\models\UserForm';
   
	/*Event add Code start*/
    public function actionAddevent() 
	{
		$userid = $_POST['user_id'];
		if($userid)
        {
            $data = array();
			$date = time();
            $datein = date('Y-m-d h:i:s');
            $event_name = $_POST['event_name'];
            $event_tagline = $_POST['event_tagline'];
            $short_desc = $_POST['short_desc'];
            $event_date = $_POST['event_date'];
            $event_time = $_POST['event_time'];
            $event_privacy = $_POST['event_privacy'];
            $event_address = $_POST['event_address'];
            $event_wu = $_POST['event_wu'];
            $event_tu = $_POST['event_tu'];
            $event_yu = $_POST['event_yu'];
            $event_tpiu = $_POST['event_tpiu'];
            $pageevent = new PageEvents();
            if(isset($_POST['parent_id']))
			{
                $entity_id = $_POST['parent_id'];
				$type = $_POST['type'];
                $pageevent->parent_id = "$entity_id";
                $pageevent->type = "$type";
            }
			if(!empty($_FILES['images']))
			{
				$name = $_FILES["images"]["name"];
                $tmp_name = $_FILES["images"]["tmp_name"];
				$url = '../../frontend/web/uploads/events/';
                $image_extn = end(explode('.',$_FILES["images"]["name"]));
                $rand = rand(111,999);
				$image = $date.'.'.$image_extn;
				move_uploaded_file($_FILES["images"]["tmp_name"], $url.$date.'.'.$image_extn);
				$pageevent->event_thumb = $image;
            }
            $pageevent->event_name = ucfirst($event_name);
            $pageevent->tagline = ucfirst($event_tagline);
            $pageevent->address = $event_address;
            $pageevent->short_desc = ucfirst($short_desc);
            $pageevent->event_date = "$event_date";
            $event_date_ts = strtotime($event_date);
            $pageevent->event_date_ts = "$event_date_ts";
            $pageevent->event_time = "$event_time";
            $pageevent->event_privacy = "$event_privacy";
            $pageevent->created_date = "$date";
			$pageevent->created_in_date = "$datein";
			$pageevent->created_by = $userid;
            $pageevent->updated_date = "$date";
            $pageevent->updated_in_date = "$datein";
            $pageevent->updated_by = $userid;
            $pageevent->wu = "$event_wu";
            $pageevent->tu = "$event_tu";
            $pageevent->yu = "$event_yu";
            $pageevent->tpiu = "$event_tpiu";
            $pageevent->is_deleted = '1';
			$pageevent->insert();
            $last_insert_id = $pageevent->_id;
			if($last_insert_id)
			{
				$data['msg'] = 'success';
				$data['event_id'] = "$last_insert_id";
				$ev = new EventVisitors();
				$ev->user_id = $userid;
				$ev->event_id = "$last_insert_id";
				$ev->created_date = "$date";
				$ev->is_deleted = '1';
				$ev->status = '1';
				$ev->insert();
				$data['status'] = true;
				$data['event_id'] = "$last_insert_id";
			}
			else
			{
				$data['status'] = false;
			}
        }
        else
        {
           $data['status'] = false;
        }
		return $data;
    }
	
	/*Event edit Code start*/
    public function actionEventedit() 
	{
		$result = array();
		$user_id = $_POST['user_id'];
		$event_id = $_POST['event_id'];
		if($user_id)
        {
            $date = time();
            $datein = date('Y-m-d h:i:s');
            $event_name = $_POST['event_name'];
            $event_tagline = $_POST['event_tagline'];
            $short_desc = $_POST['short_desc'];
            $event_date = $_POST['event_date'];
            $event_time = $_POST['event_time'];
            $event_privacy = $_POST['event_privacy'];
            $event_address = $_POST['event_address'];
            $event_wu = $_POST['event_wu'];
            $event_tu = $_POST['event_tu'];
            $event_yu = $_POST['event_yu'];
            $event_tpiu = $_POST['event_tpiu'];
            $pageevent = new PageEvents();
            if(isset($_POST['parent_id']))
			{
                $entity_id = $_POST['parent_id'];
				$type = $_POST['type'];
                $pageevent->parent_id = "$entity_id";
                $pageevent->type = "$type";
            }
			if(!empty($_FILES['images']))
			{
				$name = $_FILES["images"]["name"];
                $tmp_name = $_FILES["images"]["tmp_name"];
				$url = '../../frontend/web/uploads/events/';
                $image_extn = end(explode('.',$_FILES["images"]["name"]));
                $rand = rand(111,999);
				$image = $date.'.'.$image_extn;
				move_uploaded_file($_FILES["images"]["tmp_name"], $url.$date.'.'.$image_extn);
				$pageevent->event_thumb = $image;
            }
			$update = PageEvents::find()->where(['_id' => "$event_id"])->one();
            $update->event_name = ucfirst($event_name);
            $update->tagline = ucfirst($event_tagline);
            $update->address = $event_address;
            $update->short_desc = ucfirst($short_desc);
            $update->event_date = "$event_date";
            $event_date_ts = strtotime($event_date);
            $update->event_date_ts = "$event_date_ts";
            $update->event_time = "$event_time";
            $update->event_privacy = "$event_privacy";
            $update->updated_date = "$date";
            $update->updated_in_date = "$datein";
            $update->updated_by = $user_id;
            $update->wu = "$event_wu";
            $update->tu = "$event_tu";
            $update->yu = "$event_yu";
            $update->tpiu = "$event_tpiu";
            $update->is_deleted = '1';
			if($update->update())
			{
				
				exit('done');
				$result['status'] = true;	
			} else {
				exit('notedone');
			}	$result['status'] = false;	
			
            
        }
        else
        {
           $result['status'] = false;
        }
		//return $result;
    }
	
	/*All event code start*/
	public function actionAllevent()
    {
        $result =array();
		$user_id = $_POST['user_id'];
		if ($user_id)
        {
            $events = PageEvents::getAllEvents();
			if(!empty($events))
			{
				$i=0;
				$ev = array();
				foreach($events as $event)
				{
					$user_id = $event['created_by'];
					$event_id = $event['_id'];
					$ev[$i]['id'] =  $event['_id'];
					$ev[$i]['event_name'] = $event['event_name'];
					$ev[$i]['event_date'] = $event['event_date'];
					$ev[$i]['event_privacy'] = $event['event_privacy'];
					$ev[$i]['created_date'] = $event['created_date'];
					if($event['event_thumb'])
					{
						$ev[$i]['event_photo'] = 'uploads/events/'.$event['event_thumb'];
					} else {
						$ev[$i]['event_photo'] = 'images/additem-commevents.png';
					}
					$ev[$i]['attending'] =  EventVisitors::getEventCounts("$event_id");
					$i++;
				}
				$result['data'] = $ev;
				$result['status'] = true;
			}
			else
			{
				$result['message'] = "No event found";
				$result['status'] = false;
			}
        }
        else
        {
            $result['message'] = "No event found";
			$result['status'] = false;
        }
		return $result;
    }
	
	/*your event code start*/
	public function actionYourevent()
    {
        $result =array();
		$user_id = $_POST['user_id'];
		if ($user_id)
        {
            $events = PageEvents::getMyEvents($user_id);
			if(!empty($events))
			{
				$i=0;
				$ev = array();
				foreach($events as $event)
				{
					$user_id = $event['created_by'];
					$event_id = $event['_id'];
					$ev[$i]['id'] =  $event['_id'];
					$ev[$i]['event_name'] = $event['event_name'];
					$ev[$i]['event_date'] = $event['event_date'];
					$ev[$i]['event_privacy'] = $event['event_privacy'];
					$ev[$i]['created_date'] = $event['created_date'];
					if($event['event_thumb'])
					{
						$ev[$i]['event_photo'] = 'uploads/events/'.$event['event_thumb'];
					} else {
						$ev[$i]['event_photo'] = 'images/additem-commevents.png';
					}
					$ev[$i]['attending'] =  EventVisitors::getEventCounts("$event_id");
					$i++;
				}
				$result['data'] = $ev;
				$result['status'] = true;
			}
			else
			{
				$result['message'] = "No event found";
				$result['status'] = false;
			}
        }
        else
        {
            $result['message'] = "No event found";
			$result['status'] = false;
        }
		return $result;
    }
	
	/*Event going code start*/
	public function actionEventgoing()
    {
        $result = array();
		$user_id = $_POST['user_id'];
        if ($user_id)
        {
            $goingevents = EventVisitors::getMyGoingEvents($user_id);
			if($goingevents)
			{
				$event = array();
				$i = 0;
				foreach($goingevents as $goingevent)
				{
					$event_id = $goingevent['event_id'];
					$event_details = PageEvents::getEventdetailsshare($event_id);
					$user_id = $event_details['created_by'];
					$event[$i]['id'] =  $event_details['_id'];
					$event[$i]['event_name'] = $event_details['event_name'];
					$event[$i]['event_date'] = $event_details['event_date'];
					$event[$i]['event_privacy'] = $event_details['event_privacy'];
					$event[$i]['created_date'] = $event_details['created_date'];
					if($event_details['event_thumb'])
					{
						$event[$i]['event_photo'] = 'uploads/events/'.$event_details['event_thumb'];
					} else {
						$event[$i]['event_photo'] = 'images/additem-commevents.png';
					}
					$event[$i]['attending'] =  EventVisitors::getEventCounts("$event_id");
					$i++;
				}
				$result['data']= $event;
				$result['status'] = true;	
			}
			else
			{
				$result['status'] = false;
			}		
            
        }
        else
        {
            $result['status'] = false;
        }
		return $result;
    }
	
	/*Event join code start*/
	public function actionEventjoin()
    {
        $result = array();
		$user_id = $_POST['user_id'];
        if($user_id)
        {
            $event_id = $_POST['event_id'];
            $date = time();
            $event_exist = PageEvents::find()->where(['_id' => "$event_id",'is_deleted' => '1'])->one();
            if($event_exist)
            {
				$ev = EventVisitors::find()->where(['event_id' => "$event_id",'user_id' => "$user_id"])->one();
                $status = $ev['is_deleted'];
                if($status == '1'){$nstatus = '1';}else{$nstatus = '0';}
                if($ev)
                {
				    if($ev['is_deleted']=='1'){$ev['is_deleted'] = '0';}
                    else{$ev['is_deleted'] = '1';}
                    $ev->is_deleted = $ev['is_deleted'];
                    $ev->created_date = "$date";
                    if($ev->delete())
                    {
                        $result['msg'] = 'Join';
						$result['status'] = true;
                    }
                    else
                    {
                        $result['status'] = false;
                    }
                }
                else
                {
				    $ev = new EventVisitors();
                    $ev->user_id = $user_id;
                    $ev->event_id = $event_id;
                    $ev->created_date = "$date";
                    $ev->is_deleted = '1';
                    $ev->status = '1';
                    if($ev->insert())
                    {
                        $result['msg'] = 'Going';
						$result['status'] = true;
                    }
                    else
                    {
                        $result['status'] = false;
                    }
                }

                $event_details = PageEvents::getEventdetails($event_id);
                $attending = Notification::find()->where(['user_id' => $user_id ,'post_owner_id' => $event_details['created_by'] ,'post_id' => $event_id ,'notification_type' => 'eventgoing'])->one();
                if($attending)
                {
                    $notification = Notification::find()->where(['user_id' => $user_id ,'post_owner_id' => $event_details['created_by'] ,'post_id' => $event_id ,'notification_type' => 'eventgoing'])->one();
                }
                else
                {
                    $notification =  new Notification();
                }
                $notification->post_owner_id = $event_details['created_by'];
                $notification->user_id = "$user_id";
                $notification->post_id = "$event_id";
                $notification->notification_type = 'eventgoing';
                $notification->is_deleted = "$nstatus";
                $notification->status = '1';
                $notification->created_date = "$date";
                $notification->updated_date = "$date";
                if($event_details['created_by'] != $user_id)
                {
                    if($attending)
                    {
                        $notification->delete();
                    }
                    else
                    {
                        $notification->insert();
                    }
                }
                $evcount = EventVisitors::getEventCounts($event_id);
				$result['goingcount'] = $evcount;
            }
            else
            {
               $result['status'] = false;
            }
        }
        else
        {
            $result['status'] = false;
        }
		return $result;
    }
	
	public function actionEventdetail()
	{
		$result = array();
		$user_id = $_POST['user_id'];
		$event_id = $_POST['event_id'];
		$event_details = PageEvents::getEventdetailsshare($event_id);
		$event = array();
		if($event_details)
		{
			$event['event_name'] = $event_details['event_name']; 
			if($event_details['event_thumb'])
			{
				$event['event_photo'] = 'uploads/events/'.$event_details['event_thumb'];
			} else {
				$event['event_photo'] = 'images/additem-commevents.png';
			}
			$event['tagline'] = $event_details['tagline']; 
			$event['address'] = $event_details['address']; 
			$event['short_desc'] = $event_details['short_desc']; 
			$event['event_date'] = $event_details['event_date']; 
			$event['event_time'] = $event_details['event_time']; 
			$event['event_privacy'] = $event_details['event_privacy']; 
			$event['created_by'] = $this->getuserdata($event_details['created_by'],'fullname');
			$event['created_by_image'] =$this->getimageAPI($event_details['created_by'],'thumb');
			$event['wu'] = $event_details['wu']; 
			$event['tu'] = $event_details['tu']; 
			$event['yu'] = $event_details['yu']; 
			$event['tpiu'] = $event_details['tpiu'];
			$event['attending'] = EventVisitors::getEventCounts($event_id);	
			$result['data'] = $event;
			$result['status'] = true;
		}	
		else
		{
			$result['status'] = false;
		}	
		return $result;
	}
	
	/*Event delete code start*/
	public function actionEventdelete()
	{
		$result = array();
		$user_id = $_POST['user_id'];
		$event_id = $_POST['event_id'];
		if($user_id)
        {
			$event_exist = PageEvents::find()->where(['_id' => $event_id,'is_deleted' => '1'])->one();
            if($event_exist)
            {
                $date = time();
                $datein = date('Y-m-d h:i:s');
                $event_exist->is_deleted = "0";
                $event_exist->updated_date = "$date";
                $event_exist->updated_in_date = "$datein";
                if($event_exist->update())
                {
                    Notification::deleteAll(['is_deleted' => '0','post_id' => $event_id,'notification_type' => 'eventgoing']);
                    Notification::deleteAll(['is_deleted' => '0','post_id' => $event_id,'notification_type' => 'eventinvite']);
                    EventVisitors::deleteAll(['is_deleted' => '1','event_id' => $event_id]);
                    $result['status'] = true;
                }
            }
            else
            {
                $result['status'] = false;;
            }
		}
		else
		{
			 $result['status'] = false;
		}
		return $result;	
	}
	
	 public function actionEventsendinvite()
    {
        $result = array();
		$user_id = (string)$_POST['user_id'];
		$connect_id = (string)$_POST['connect_id'];
        $event_id = (string)$_POST['event_id'];
        $event_details = PageEvents::getEventdetails($event_id);
		$date = time();
		if($event_details && $user_id)
        {
            
			$notification =  new Notification();
            $notification->from_connect_id = "$connect_id";
            $notification->user_id = "$user_id";
            $notification->post_id = "$event_id";
            $notification->notification_type = 'eventinvite';
            $notification->is_deleted = '0';
            $notification->status = '1';
            $notification->created_date = "$date";
            $notification->updated_date = "$date";
            if($notification->insert())
            {
                try
                {
                    $eventname = $event_details['event_name'];
                    $eventaddress = $event_details['address'];
                    $eventtagline = $event_details['tagline'];
                    $event_short_desc = $event_details['short_desc'];
                    $eventdate = $event_details['event_date'];
                    $eventtime = $event_details['event_time'];
                    $attend = EventVisitors::getEventCounts($event_id);
                    $ed = date("dS F Y", strtotime($eventdate)).' at '.$eventtime;
                    $frndname = $this->getuserdata($connect_id,'fullname');
                    $sendto = $this->getuserdata($connect_id,'email');
                    $fullname = $this->getuserdata($user_id,'fullname');
                    $ev_image = $this->geteventimage($event_id);
                    $ev_image = 'https://www.iaminjordan.com/'.$ev_image;
                    $eventlink = "https://www.iaminjordan.com/frontend/web/index.php?r=event/detail&e=$event_id";
                    $userlink = "https://www.iaminjordan.com/frontend/web/index.php?r=userwall/index&id=$user_id";
                    $addresslink = "https://maps.google.it/maps?q=$eventaddress";
                    $test = Yii::$app->mailer->compose()
                            ->setFrom(array('csupport@iaminjordan.com' => 'iaminjordan'))
                            ->setTo($sendto)
                            ->setSubject($fullname .' has invited you to an event '.$eventname.' in '.$eventaddress)
                            ->setHtmlBody('<html>
                                <head>
                                    <meta charset="utf-8" />
                                    <title>iaminjordan</title>
                                </head>
                                <body style="margin:0;padding:0;background:#dfdfdf;">
                                    <div style="color: #353535; float:left; font-size: 13px;width:100%; font-family:Arial, Helvetica, sans-serif;text-align:center;padding:40px 0 0;">
                                    <div style="width:600px;display:inline-block;">
                                        <table style="width:100%;">
                                            <tbody>
                                                <tr>
                                                  <td><img src="https://www.iaminjordan.com/frontend/web/assets/baf1a2d0/images/black-logo.png" style="margin:0 0 10px;width:130px;float:left;"></td>
                                                </tr>
                                                <tr style="background:#333;position:relative;width:100%;float:left;padding-top:10px;border-bottom:1px solid #e1e1e1;">
                                                  <td style="background:#fff;position:relative;width:100%;float:left;padding-top:10px;text-align:center;padding:0;">
                                                    <h4 style="color:#434343;font-size:14px;font-weight:normal;margin:0;padding:10px 15px;border-bottom:1px solid #e1e1e1;"><span style="font-weight:bold;">'.$fullname.'</span> has invited you to an event at <span style="font-weight:bold;">'.$eventaddress.'</span></h4>
                                                  </td>
                                                </tr>
                                                <tr style="background:#f5f5f5;position:relative;width:100%;float:left;padding:0;">
                                                  <td style="width:200px;padding:15px;padding-right:0;margin:0;float:left;"><img src="'.$ev_image.'" style="width:100%;"></td>
                                                  <td style="width:355px;padding:15px;vertical-align:top;float:right;padding-left:0;">
                                                    <div style="position:relative;width:100%;text-align:left;font-size:13px;">
                                                        <span style="color:#3399cc;margin: 0 0 5px;font-weight:bold;font-size:14px;display:inline-block;">'.$eventname.'</span>
                                                        <br />
                                                        <span style="margin: 0 0 15px;display:inline-block;">'.$eventtagline.'</span>
                                                        <br />
                                                        <span style="color:#999;margin: 0 0 5px;display:inline-block;">'.$ed.'</span>
                                                        <br />
                                                        <span style="color:#999;">'.$attend.' Attending</span>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr style="background:#fff;width:100%;float:left;padding:10px 0 20px;border-top:1px solid #e1e1e1;">
                                                  <td style="width:100%;float:left;padding:0;">
                                                    <div style="position:relative;width:100%;padding:0 20px;box-sizing: border-box;text-align:justify;color:#666;clear:both;float:left;font-size:13px;">
                                                        <p style="margin: 0 0 15px;">'.$event_short_desc.'</p>
                                                        <a href="'.$eventlink.'" style="background:#3399cc;font-size:13px;padding:8px 15px;border-radius:3px;float:right;clear:both;color:#fff;text-decoration:none;margin:10px 0 0;">View Event Details</a>
                                                    </div>	
                                                  </td>
                                                </tr>
                                                <tr style="width:100%;float:left;">
                                                    <td style="width:100%;float:left;padding-top:10px;text-align:center;padding:0;margin:10px 0 0;">
                                                        <div style="width:600px;display:inline-block;font-size:11px;">
                                                            <div style="color: #777;text-align: left;">&copy;  www.iaminjordan.com All rights reserved.</div>
                                                            <div style="text-align: left;width: 100%;margin:5px  0 0;color:#777;">For support, you can reach us directly at <a href="csupport@iaminjordan.com" style="color:#4083BF">csupport@iaminjordan.com</a></div>
                                                       </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </body>
                        </html>')
                    ->send();
                }
                catch (ErrorException $e)
                {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                }
                $result['status'] = true;
            }
            else
            {
                $result['status'] = false;
            }
        }
        else
        {
            $result['status'] =  false;
        }
		return $result;
    }
}	