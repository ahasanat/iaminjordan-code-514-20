<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Expression;
use yii\web\UploadedFile;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use frontend\models\Group;
use frontend\models\GroupMember;
use frontend\models\GroupPhoto;
use frontend\models\PostForm;
use frontend\models\LoginForm;
/**
 * Site controller
 */
class GroupController extends ActiveController {

    public $modelClass = 'api\modules\v1\models\UserForm';
   
    /* Authorization Status code */
    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
    
    /* Response status */
    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if($body != '')
        {
            // send the body
            echo $body;
        }
        // we need to create the body if none is passed
        else
        {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch($status)
            {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on 
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templated in a real-world solution
            $body = '
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "https://www.w3.org/TR/html4/strict.dtd">
                <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
                </head>
                <body>
                    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
                    <p>' . $message . '</p>
                    <hr />
                    <address>' . $signature . '</address>
                </body>
                </html>'; 

            echo $body;
        }
       return true;
    }
    
    /* Check Authorization by headers HTTP_X_USERNAME & HTTP_X_PASSWORD */
    private function _checkAuth()
    {//echo "<pre>";print_r();exit;
        $headers = apache_request_headers();
        $header_username = $headers['HTTP_X_USERNAME'];
        $header_pwd = $headers['HTTP_X_PASSWORD'];
        // Check if we have the USERNAME and PASSWORD HTTP headers set?
        if(!(isset($header_username) and isset($header_pwd))) {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
        $username = $header_username;
        $password = $header_pwd;
        // Find the user
        $user = UserForm::find('LOWER(email)=?',array(strtolower($username)));
        if($user===null) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Name is invalid');
        } else if(!LoginForm::find()->where(['email' => $username,'password' => $password])->orwhere(['phone'=> $username,'password' => $password])->one()) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Password is invalid');
        }
    }
	
	public function actionAddgroup() 
	{
		$userid = (string)$_POST['user_id'];
		if($userid)
		{
			$data 	= array();
			$date = time(); 
			$group = new Group();
			if(!empty($_FILES['images']))
			{
				$name = $_FILES["images"]["name"];
                $tmp_name = $_FILES["images"]["tmp_name"];
				$url = '../../frontend/web/uploads/group/';
                $image_extn = end(explode('.',$_FILES["images"]["name"]));
                $rand = rand(111,999);
				$image = $date.'.'.$image_extn;
				move_uploaded_file($_FILES["images"]["tmp_name"], $url.$date.'.'.$image_extn);
				$group->group_thumb =$image;
            }
			$group->user_id = $userid;
			$group->name = $_POST['title']; 
			$group->tagline = $_POST['tagline'];
			$group->location = $_POST['location'];
			$group->description = $_POST['description'];
			$group->privacy = $_POST['visible'];
			$group->review = $_POST['review'];
			$group->created_date = "$date";
			$group->modified_date = "$date";
			$group->is_deleted = "1";
			$group->entity_group = "1";
			$group->insert();
			$last_insert_id = $group->_id;
			if($last_insert_id)
			{
				$data['msg'] = 'success';
				$data['group_id'] = "$last_insert_id";
			}
			else
			{
				$data['msg'] = 'fail';
			}
		}
		else
		{
			$data['msg'] = 'fail';
		}
		return $data;
		
    }
	
	public function actionAllgroup()
	{
		$result = array();
		$user_id = (string)$_POST['user_id'];
		$type = $_POST['type'];
		if ($user_id)
		{
			if($type == 'yours')
			{
				$groups = Group::yoursgroup($user_id);
			}
			else if($type == 'join')
			{
				$groups = Group::joingroup($user_id);
			}
			else
			{
				$groups = Group::allgroup();
			}		
			if(!empty($groups))
			{
				$i=0;
				$grp = array();
				foreach($groups as $group)
				{
					$user_id = $group['user_id'];
					$group_id = $group['_id'];
					$grp[$i]['id'] =  $group_id;
					$grp[$i]['name'] = $group['name'];
					$grp[$i]['tagline'] = $group['tagline'];
					$grp[$i]['location'] = $group['location'];
					$grp[$i]['description'] = $group['description'];
					$grp[$i]['privacy'] = $group['privacy'];
					$grp[$i]['review'] = $group['review'];
					if($group['group_thumb'])
					{
						$grp[$i]['group_photo'] = 'uploads/group/'.$group['group_thumb'];
					} else {
						$grp[$i]['group_photo'] = 'images/additem-groups.png';
					}
					$grp[$i]['created_date'] = $group['created_date'];
					$grp[$i]['user_name'] = $this->getuserdata($user_id,'fullname');
					$grp[$i]['user_image'] = $this->getimageAPI($user_id,'thumb');
					$grp[$i]['group_member'] = count(GroupMember::getgroupmember($group_id))+1;
					$i++;
				}
				$result['data'] = $grp;
				$result['status'] = true;
			
			}else{
				$result['message'] = "No group found";
				$result['status'] = false;
			}
			
		}else{
			$result['message'] = "No group found";
			$result['status'] = false;
		}
		return $result;
	}
}	