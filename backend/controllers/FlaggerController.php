<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use frontend\models\DefaultImage;
use frontend\models\Gallery;
use frontend\models\PlaceDiscussion;
use frontend\models\PlaceReview;
use frontend\models\PlaceTip;
use frontend\models\PlaceAsk;
use frontend\models\Blog;
use frontend\models\Collections;
use frontend\models\Trip;
use frontend\models\Localdine;
use frontend\models\Homestay;
use frontend\models\Camping;
use frontend\models\LocalguidePost;
use frontend\models\LocaldriverPost;
use backend\models\Messagechat;

/**
 * Site controller
 */
class FlaggerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
         return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['photostream', 'discussion', 'reviews', 'flagperform','Messagechat','question','tips', 'blog', 'collections', 'homestay', 'localguide', 'localdriver', 'trip', 'camping', 'localdine'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],		
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionPhotostream()
    {
        $data = Gallery::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('photostream', array('data' => $data));
    }

    public function actionDiscussion()
    {
        $data = PlaceDiscussion::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('discussion', array('data' => $data));
    }

    public function actionReviews()
    {
        $data = PlaceReview::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('reviews', array('data' => $data));
    }

    public function actionQuestion()
    {
        $data = PlaceAsk::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('question', array('data' => $data));
    }

    public function actionTips()
    {
        $data = PlaceTip::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('tips', array('data' => $data));
    }

    public function actionBlog()
    {
        $data = Blog::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('blog', array('data' => $data));
    }

    public function actionCollections()
    {
        $data = Collections::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('collections', array('data' => $data));
    }

    public function actionTrip()
    {
        $data = Trip::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('trip', array('data' => $data));
    }

    public function actionLocaldine()
    {
        $data = Localdine::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('localdine', array('data' => $data));
    }

    public function actionHomestay()
    {
        $data = Homestay::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('homestay', array('data' => $data));
    }

    public function actionCamping()
    {
        $data = Camping::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('camping', array('data' => $data));
    }

    public function actionLocalguide()
    {
        $data = LocalguidePost::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('localguide', array('data' => $data));
    }

    public function actionLocaldriver()
    {
        $data = LocaldriverPost::find()->where(['flagger' => 'yes'])->asarray()->all();
        return $this->render('localdriver', array('data' => $data));
    }

    public function actionFlagperform()
    {
        $user_id = (string)$_SESSION['email'];
        $is_admin = Messagechat::IsAdmin($user_id);
        if($is_admin) {
            if(isset($_POST['id']) && $_POST['id'] != '') {
                if(isset($_POST['module']) && $_POST['module'] != '') {
                    if(isset($_POST['action']) && $_POST['action'] != '') {
                        $id = $_POST['id'];
                        $module = $_POST['module'];
                        $action = $_POST['action'];
                        $actionArray = array('unflag', 'delete');

                        if(in_array($action, $actionArray)) {
                            if($module == 'photostream') {
                                $post = Gallery::find()->where(['_id' => $id])->one();
                            } else  if($module == 'discussion') {
                                $post = PlaceDiscussion::find()->where(['_id' => $id])->one();
                            } else  if($module == 'reviews') {
                                $post = PlaceReview::find()->where(['_id' => $id])->one();
                            } else  if($module == 'question') {
                                $post = PlaceAsk::find()->where(['_id' => $id])->one();
                            } else  if($module == 'tips') {
                                $post = PlaceTip::find()->where(['_id' => $id])->one();
                            } else  if($module == 'blog') {
                                $post = Blog::find()->where(['_id' => $id])->one();
                            } else  if($module == 'collections') {
                                $post = Collections::find()->where(['_id' => $id])->one();
                            } else  if($module == 'trip') {
                                $post = Trip::find()->where(['_id' => $id])->one();
                            } else  if($module == 'localdine') {
                                $post = Localdine::find()->where(['_id' => $id])->one();
                            } else  if($module == 'homestay') {
                                $post = Homestay::find()->where(['_id' => $id])->one();
                            } else  if($module == 'camping') {
                                $post = Camping::find()->where(['_id' => $id])->one();
                            } else  if($module == 'localguide') {
                                $post = LocalguidePost::find()->where(['_id' => $id])->one();
                            } else  if($module == 'localdriver') {
                                $post = LocaldriverPost::find()->where(['_id' => $id])->one();
                            }
                            
                            if(!empty($post)) {
                                if($action == 'unflag') {
                                    $post->flagger = '';
                                    $post->flagger_date = '';
                                    $post->flagger_by = '';
                                    $post->save();
                                    $result = array('success' => 'yes', 'msg' => 'Unflag post.');
                                    return json_encode($result, true);
                                }

                                if($action == 'delete') {
                                    $post->delete();
                                    $result = array('success' => 'yes', 'msg' => 'Delete post.');
                                    return json_encode($result, true);
                                }
                            }
                        }
                    }
                }

            }
        }    
    }
}
