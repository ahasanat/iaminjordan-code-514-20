<?php
namespace backend\controllers;

use Yii; 
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use backend\models\Googlekey;

/**
 * Site controller
 */
class GoogleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
         return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'setkey'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],		
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSetkey()
    {
        if(isset($_POST['id']) && $_POST['id'] != '') {
            $id = $_POST['id'];
            return Googlekey::setkey($id);
        }
        return false;
    }


    public function actionSetkeyexpired()
    {
        return Googlekey::setkeyexpired();
    }

    public function actionCheckforunexpired()
    {
        return Googlekey::checkforunexpired();
    }

    public function actionGetkey()
    {
        return Googlekey::getkey();
    }
}
