<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use backend\models\LoginForm;
use backend\models\Messagechat;

/**
 * Site controller
 */
class MessagechatController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
         return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['addgiftimages', 'addgiftcategory'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],		
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

     public function actionLogin()
    {
        $this->layout = 'loginLayout';
        if (!\Yii::$app->user->isGuest)
        {
           return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login())
        {
            return $this->goBack();
            //return $this->redirect(array('site/index'));
        }
        else
        {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }


    public function actionAddgiftimages()
    {
        if(isset($_SESSION['email']) && !empty($_SESSION['email'])) {
            $user_id = (string)$_SESSION['email'];
            $is_admin = Messagechat::IsAdmin($user_id);
            if($is_admin) {
                return $this->render('addgiftimages');
            } else {
                \Yii::$app->user->logout();
                return $this->goHome(); 
            }
        } else {
            \Yii::$app->user->logout();
            return $this->goHome(); 
        }
        
    }

    public function actionAddgiftcategory()
    {
        if(isset($_SESSION['email']) && !empty($_SESSION['email'])) {
            $user_id = (string)$_SESSION['email'];
            $is_admin = Messagechat::IsAdmin($user_id);
            if($is_admin) {
                return $this->render('addgiftcategory');
            } else {
                \Yii::$app->user->logout();
                return $this->goHome(); 
            }
        } else {
            \Yii::$app->user->logout();
            return $this->goHome(); 
        }
        
    }
}
