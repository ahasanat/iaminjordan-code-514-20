<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\mongodb\ActiveRecord;
use yii\helpers\HtmlPurifier;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\mongodb\Expression;
use backend\components\ExSession;
use frontend\models\PostForm;
use frontend\models\LoginForm;
use frontend\models\PhotoForm;
use frontend\models\PlaceVisitor;

class PlacesController  extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout','allcollection'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

	public function getUser()
    {
        if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			return $this->hasOne(LoginForm::className(), ['_id' => 'post_user_id']);
		}	
    }

	public function actionAll()
    {
		$all = PlaceVisitor::getPlacesDetails();
		return $this->render('all',['all' =>$all]);
	}

    public function actionReviews()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$reviews = PostForm::getPlacePost('reviews');
			return $this->render('reviews',['reviews' =>$reviews]);
		}	
	}

	public function actionTips()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$tips = PostForm::getPlacePost('tip');
			return $this->render('tips',['tips' =>$tips]);
		}	
	}
	
	public function actionPhotos()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$photos = PhotoForm::getAllPhotos();
			return $this->render('photos',['photos' =>$photos]);
		}	
	}

	public function actionQuestions()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$asks = PostForm::getPlacePost('ask');
			return $this->render('asks',['asks' =>$asks]);
		}	
	}
	
	public function actionRemove()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$id = $_POST['id'];
				$exist = PhotoForm::find()->where(['_id' => $id ])->one();
				if($exist->delete())
				{
					return true;
				}
				else
				{
					return false;   
				}
			}
			else
			{
				return false;   
			}
		}	
	}
	
	public function actionDeleteplacephoto()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $image_name = isset($_POST['image_name']) ? $_POST['image_name'] : '';
        $post_id =  isset($_POST['post_id']) ? $_POST['post_id'] : '';
        $data = array();
        if($image_name != '' && $post_id != '' && $user_id)
        {
            $getpics = PhotoForm::deletePlacePhotos($post_id,$user_id,$image_name,'frontend');
            return $getpics;
        } else {
            $data['value'] = '0';
            return json_encode($data);
        }
    }
}