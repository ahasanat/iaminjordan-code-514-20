<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\mongodb\ActiveRecord;
use yii\helpers\HtmlPurifier;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\mongodb\Expression;
use backend\components\ExSession;
use frontend\models\Referal;

class ReferenceController  extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout','all'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

	public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
	
	public function actionAll()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$referals = Referal::getAllReferalsAdmin();
			return $this->render('all',['referals' =>$referals]);
		}	
	}
	
	public function actionRemoveref()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$id = $_POST['id'];
				$mt_exist = Referal::find()->where(['_id' => $id ])->one();
				$mt_exist->delete();
			}
			else
			{
				return false;   
			}
		}	
	}
	
	public function actionReferenceupdate()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$id = $_POST['id'];
				$status = $_POST['status'];
				
				if($status == 'Active'){
					$dl = "0"; 
				}else{
					$dl = "1";
				}
				$update = Referal::find()->where(['_id' => "$id"])->one();
				$update->is_deleted = $dl;
				$update->update();
				return true;
			}else{
				return false;
			}	
		}	
	}
}