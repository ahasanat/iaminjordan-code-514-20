<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use yii\mongodb\ActiveRecord;
use yii\web\UploadedFile;

/**
 * User
 */
class AddcreditsPlans extends ActiveRecord
{
   
     public static function collectionName()
    {
        return 'credits_plans';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id','amount','credits','percentage','plan_type'];

    }
	
    public function getCreditsPlans()
    {
        return AddcreditsPlans::find()->all();
    }
}
