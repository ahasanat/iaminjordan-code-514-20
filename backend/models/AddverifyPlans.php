<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use yii\mongodb\ActiveRecord;
use yii\web\UploadedFile;

class AddverifyPlans extends ActiveRecord
{
   
     public static function collectionName()
    {
        return 'verify_plans';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id','amount','months','percentage','plan_type'];

    }
	
    public function getVerifyPlans()
    {
        return AddverifyPlans::find()->all();
    }
}
