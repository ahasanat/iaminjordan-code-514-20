<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\helpers\Security;
use yii\mongodb\ActiveRecord;
use yii\web\IdentityInterface;

class DefaultImage extends ActiveRecord
{
    public static function collectionName()
    {
        return 'default_image';
    }

     public function attributes()
    {
		return ['_id', 'type', 'image','updated_date'];
	}
}
