<?php

namespace frontend\models;

use yii\base\Model;
use Yii;
use yii\mongodb\ActiveRecord;
use frontend\models\UserForm;
/**
 * This is the model class for table "session_frontend_user".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $ip 
 * @property integer $expire
 * @property resource $data
 */
class MessageBlock extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'messageblocks';
    }

    public function attributes() {
        return ['_id', 'from_id', 'to_id', 'con_id', 'created_at', '__v'];
    }
}