<?php

namespace backend\models;

use yii\base\Model;
use Yii;
use yii\mongodb\ActiveRecord;
use frontend\models\UserForm;
/**
 * This is the model class for table "session_frontend_user".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $ip 
 * @property integer $expire
 * @property resource $data
 */
class Messages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'messages';
    }

    public function attributes() {
        return ['_id', 'reply', 'type', 'from_id', 'to_id', 'con_id', 'category', 'created_at', 'to_id_del', 'from_id_del', 'to_id_read', 'from_id_read', 'to_id_flush', 'from_id_flush', '__v'];
    }

     public function getInfoForSelf($post) {
        foreach ($post as $type => $values) {
            foreach ($values as $key => $data) {
                $id= (string)$data['id'];
                $info = UserForm::find()
                ->select(['fname', 'lname', 'country'])
                ->where(['_id' => $id])
                //->andWhere(['status' => '1'])
                ->one();

                $thumb = $this->getimage($id,'thumb');
                //$thumb = (isset($info['gender']) && $info['gender'] == 'Male') ? 'Male32.png' : 'Female32.png';
                $country = (isset($info['country']) && $info['country'] != '') ? $info['country'] : '';
                $newInfo = [
                    'fname' => $info['fname'],
                    'lname' => $info['lname'],
                    //'gender' => $info['gender'],
                    'thumbnail' => $thumb,
                    'country' => $country
                ];

                if($newInfo) {
                    $post[$type][$key]['information'] = $newInfo;
                }
            }
        }

        return $post;
    }

    public function getInfoForAll($post) {
            foreach ($post as $key => $data) {
                $id= (string)$data;
                $info = UserForm::find()
                ->select(['fname', 'lname', 'country'])
                ->where(['_id' => $id])
                //->andWhere(['status' => '1'])
                ->one();

                $thumb = $this->getimage($id,'thumb');
                $country = (isset($info['country']) && $info['country'] != '') ? $info['country'] : '';
                $newInfo = [
                    'id' => $id,
                    'fname' => $info['fname'],
                    'lname' => $info['lname'],
                    //'gender' => $info['gender'],
                    'thumbnail' => $thumb,
                    'country' => $country
                ];

                if($newInfo) {
                    $post[$key] = $newInfo;
                }
            }
        return $post;
    }

    public function getRecentMessageUserInformation($post, $user_id) {
            foreach ($post as $key => $data) {
                $id= (string)$data['from_id'];

                if($id == $user_id) {
                    $id = (string)$data['to_id'];
                }
                
                $info = UserForm::find()
                ->select(['fname', 'lname', 'country'])
                ->where([(string)'_id' => $id])
                ->andWhere(['status' => '1'])
                ->one();


                $thumb = $this->getimage($id,'thumb');
                $country = (isset($info['country']) && $info['country'] != '') ? $info['country'] : '';
                $newInfo = [
                    'fname' => $info['fname'],
                    'lname' => $info['lname'],
                    'thumbnail' => $thumb,
                    'country' => $country
                ];

                if($newInfo) {
                    $post[$key]['information'] = $newInfo;
                }
            }
        return $post;
    }
     public function getLoadHistory($post) {
        $newCreatePost = [];
        
        if($post) {
                $ids = $post['ids'];   
                $category = $post['category'];   
                $newpost = [];
                $newpost['start'] = $post['start'];
                $newpost['limit'] = $post['limit'];
                foreach ($ids as $key => $value) {
                    $getInfo = UserForm::find()
                    ->select(['fname', 'lname'])
                    ->where([(string)'_id' => $value])
                    ->andWhere(['status' => '1'])
                    ->one();
                    
                    $thumb = $this->getimage($value,'thumb');
                    $current = [
                        'fname' => $getInfo['fname'],
                        'lname' => $getInfo['lname'],
                        'thumbnail' => $thumb
                    ];

                    $newpost['ids'][$value] = $current;
                }

                $socket = Messages::find()
                ->where([
                    'from_id' => $ids['from_id'],
                    'to_id' => $ids['to_id'],
                    'category' => $category
                ])
                ->orwhere([
                    'from_id' => $ids['to_id'],
                    'to_id' => $ids['from_id'],
                    'category' => $category
                ])
                ->orderBy('created_at DESC')
                ->limit($newpost['limit'])
                ->offset($newpost['start'])
                ->all();
               
                foreach ($socket as $key => $socval) {
                    $current = [];
                    $fname = $newpost['ids'][$socval['from_id']]['fname'];
                    $lname = $newpost['ids'][$socval['from_id']]['lname'];
                    $thumb = $newpost['ids'][$socval['from_id']]['thumbnail'];
                    $current['_id'] = (string)$socval['_id'];
                    $current['msg'] = $socval['reply'];
                    $current['fullname'] = $fname.' '.$lname;
                    $current['thumb'] = $thumb;
                    $current['type'] = $socval['type'];
                    $current['from_id'] = $socval['from_id'];
                    $current['to_id'] = $socval['to_id'];
                    $newCreatePost[] = $current;
                }
        }

        return $newCreatePost;
    }

    public function deletesocketconversation($from_id) {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        if($user_id) {
            /*$demo = Socket::find()->where(['from_id' => $user_id])->all();
            print_r($demo);
            die;*/
        } else {
            return false;
        }

    }

    /*public function getUserInfo($email) {
        return LoginForm::find()
            ->select(['_id', 'fname', 'lname', 'thumb', 'country'])
            ->where(['email' => $email])
            ->andWhere(['_id' => (string)$user_id])
            ->(['_id' => $id])
            ->one();
    }

    public function getAllUsers($post) {
        foreach ($post as $key => $types) {
            $count = count($types);
            $makeNewArray = [];
            for ($i=0; $i < $count; $i++) {

                $currentUser = $types[$i]; 
                $info = $this->getUserInfo($currentUser);
                $makeNewArray[] = $types[$info];
                
            }
        }
    }*/

}