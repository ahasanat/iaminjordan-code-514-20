<?php 
use yii\helpers\Html;
use backend\models\DefaultPosts;
$travelasset = backend\assets\TravelAsset::register($this);
$this->title = 'Credits Plans';
$travelbaseUrl = $travelasset->baseUrl;
$title = $description = '';

$DefaultPosts = DefaultPosts::find()->where(['module' => 'discussion'])->asarray()->one();
if(!empty($DefaultPosts)) {
	$title = isset($DefaultPosts['title']) ? $DefaultPosts['title'] : '';
	$description = isset($DefaultPosts['title']) ? $DefaultPosts['description'] : '';
}
?>

<div class="content-wrapper credits-admin">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Discussion Post</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
				  <form id="frm" class="topform">
					<div class="frow">
						<label>Post Title </label>&nbsp;
						<input type="text" id="title" value="<?=$title?>" />
					</div>
					<div class="frow">
						<label>Post Description </label>&nbsp;
						<textarea id="description" class="form-control" rows="4"><?=$description?></textarea>
					</div>
					<div class="frow">
						<input type="button" name="add" value="add" onclick="storeddefault()" class="btn btn-primary"/>  
						<input type="reset" name="clear" value="clear" class="btn btn-primary"/>  
					</div>
				  </form>
            </div>
	      </div>
        </div>
      </div>
    </section>
  </div>
 
<script type="text/javascript">
	function storeddefault() {
		$title = $('#title').val();
		$description = $('#description').val();

	    $.ajax({
            type: 'POST',
            url: "?r=defaultpost/storeddiscussion", 
            data: {
            	title: $title,
            	description: $description
            },
            success: function (data) {
                window.location.href = "";
            }
        });
	}
</script>