<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Abuse Complaint Listing';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
if(isset($information) && !empty($information)) {
	$information = json_decode($information, true);
} else {
	$information = array();
} 
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>Flag Message</h1>
    </section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
		              <h3 class="box-title">Message List</h3>
		            </div>
					<div class="box-body">
						<div class="contentExist">
							<table id="messageabuse" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Name</th>
										<th>Offer Id</th>
										<th>Reason</th>
										<th>Created At</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
								<?php foreach ($information as $key => $value) {
									$id = (string)$value['_id']{'$id'};
									$userId = $value['user_id'];
									$name = $value['userBasicInfo']['fname'].' '.$value['userBasicInfo']['lname'];
									$offer = $value['offer_id'];
									$reasonStatement = $value['reasonStatement'];
									$created_at = $value['created_at'];
									echo '<tr class="record'.$userId.$offer.'">
										<td><a target="_blank" href="'.$front_url.'?r=userwall/index&id='.$userId.'">'.$name.'</a></td>
										<td>'.$offer.'</td>
										<td>'.$reasonStatement.'</td>
										<td>'.date("Y-m-d H:i:s", $created_at).'</td>
										<td><a  id ='.$id.' href="javascript:void(0)" onclick="goDeleteabuse(\''.$id.'\')">Delete</a></td>
									</tr>';
								} ?>
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
 
<script>
	
	function goDeleteabuse($id) {
		if($id != undefined && $id != null) {
			BootstrapDialog.show({
				size: BootstrapDialog.SIZE_SMALL,
				title: 'Delete Message Flag',
				cssClass: 'custom-sdialog',
				message: 'Are you sure to Delete this Message flag ?',
				buttons: [{
						label: 'Keep',
						action: function (dialogItself) {
							dialogItself.close();
						}
					}, {
						label: 'Delete',
						action: function (dialogItself) {
							$.ajax({
								url: '?r=message/godeleteabuse', 
								type: 'POST',
								data: {$id},
								success: function (data) {
									dialogItself.close();
									$result = JSON.parse(data);
									if($result.status != undefined && $result.status == true) {
										var $cls = $result.class;
										var table = $('#messageabuse').DataTable();
										table.rows('.'+$cls).remove().draw( false );
									}
								}
							});
						}
					}]
			});
		}
	}
</script>
