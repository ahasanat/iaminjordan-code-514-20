<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\Like;
$this->title = 'Pages';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Pages</h1>
    </section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Page List</h3>
					</div>
					<div class="box-body">
						<table id="allpageslist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Name</th>
								  <th>Tagline</th>
								  <th>Created By</th>
								  <th>Created Date</th>
								  <th>Page Likes</th>
								  <th>Take Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($pages as $page)
								{
									$pageid = $page['page_id'];
									$page_owner = $page['created_by'];
									$owner_name = $this->context->getuserdata($page_owner,'fullname');
									$like_count = Like::getLikeCount($pageid);
									if($page['is_deleted']=='1'){$status = 'Active';}else{$status= 'Inactive';}
								?>
									<tr id="page_<?=$pageid?>">
										<td><?= $page['page_name'];?></td>
										<td><?= $page['short_desc'];?></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $page_owner;?>"><?= $owner_name;?></a></td>
										<td><?= date('d-M-Y',$page['created_date']);?></td>
										<td><?= $like_count;?></td>
										<td>
											<a target="_blank" href="<?= $front_url;?>?r=page/index&id=<?= $pageid;?>">View</a> / <a id = "remove_<?= $pageid;?>" href="javascript:void(0)" onclick="remove('<?= $pageid;?>')">Delete</a> / <a id = "update_<?= $pageid;?>" href="javascript:void(0)" onclick="update('<?= $pageid;?>','<?= $status;?>')" ><?= $status;?></a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this page?");
	if(r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=page/remove', 
			type: 'POST',
			data: 'id='+id,
			success: function (data){
				$('#page_'+id).html('');
				$('#page_'+id).remove();
				var row = $("#"+id).parents('tr');
				$('#allpageslist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
function update(id,status)
{	
	if(status == 'Active')
	{
		var statuss = 'Inactive';
	}
	else
	{
		var statuss = 'Active';
	}
	var r = confirm("Are you sure to "+statuss+" this page?");
	if(r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=page/update', 
			type: 'POST',
			data: 'id='+id+'&status='+status,
			success: function (data)
			{
				$('#update_'+id).html(statuss);
			}
		});
	}
}
</script>