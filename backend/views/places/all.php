<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
$this->title = 'Place All';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Place All</h1>
    </section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Place All List</h3>
					</div>
					<div class="box-body">
						<table id="placeslist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Place</th>
								  <th>Visited By</th>
								  <th>Visited Date</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach($all as $user)
								{
									$userid = $user['user_id'];
								?>
									<tr>
										<td><?= $user['place'];?></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $userid;?>"><?= $user['user']['fullname'];?></a></td>
										<td><?= date('d-M-Y h:i:s',$user['visited_date']);?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>