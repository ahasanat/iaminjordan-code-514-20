<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
$this->title = 'Place Photos';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper placespage">
	<section class="content-header">
		<h1>Place Photos</h1>
    </section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Place Photos List</h3>
					</div>
					<div class="box-body">
						<table id="placesphotoslist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Place</th>
								  <th>Photos</th>
								  <th>Photo By</th>
								  <th>Photo Date</th>
								  <th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach($photos as $post)
								{
									$postid = $post['_id'];
								?>
									<tr>
										<td><?= $post['place'];?></td>
										<td>
										<?php
											$eximgs = explode(',',$post['image'],-1);
											foreach ($eximgs as $eximg) {
											$added_by = $post['user_id'];
											$file = $front_url.'/uploads/placephotos/'.$added_by.'/'.$eximg;
											if(file_exists($file)){
												$inameclass = explode('.',$eximg);
												$inameclass = $inameclass[0];
											?>
											<div class="imgpreview-holder" id="<?=$added_by.'_'.$inameclass?>">
												<a target="_blank" href="<?=$file?>" class="listalbum-box imgpin">
													<img alt="" src="<?=$file?>" height="75px" width="75px"/>
												</a>
												<a href="javascript:void(0);" class="delbtn" onclick="deletePlaceImage('<?=$eximg?>','<?=$added_by?>','<?=$postid?>','<?=$inameclass?>')"><i class="zmdi zmdi-delete"></i></a>
											</div>
										<?php } } ?>
										</td>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $post['user_id'];?>"><?= $post['user']['fullname'];?></a></td>
										<td><?= date('d-M-Y',$post['created_date']);?></td>
										<td><a href="javascript:void(0)" id="<?=$postid;?>" onclick="remove('<?= $postid;?>')">Delete</a></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this post?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=places/remove', 
			type: 'POST',
			data: 'id='+id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#placesphotoslist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
function deletePlaceImage(image_name, user_id, post_id, iname)
{
	var r = confirm("Are you sure to delete this image?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			type: 'POST',
			url: '?r=places/deleteplacephoto',
			data: "image_name="+image_name+"&user_id="+user_id+"&post_id="+post_id,
			success: function (data)
			{
				var result = $.parseJSON(data);
				if(result['value'] === '1')
				{
					$('#'+user_id+'_'+iname).hide();
				}
			}
		});
	}
}
</script>