<?php 
use yii\helpers\Html;
use frontend\models\LoginForm;
$this->title = 'User Posts';

$user = LoginForm::find()->where(['_id'=> $_GET['user_id']])->one();
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Post Listing</h1>
      <!-- <ol class="breadcrumb">
        <li><a href="javascript:void(0)"><i class="mdi mdi-gauge"></i> Home</a></li>
        <li><a href="javascript:void(0)">Users</a></li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?=$user['fullname']?>'s Post List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="userpostlist" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th>View</th>
                  <th>Privacy</th>
				  <th>Post Text</th>   
				  <th>Post Type</th>   
				  <th>Delete</th>   
                </tr>
                </thead>
                <tbody>
    <?php foreach($userposts as $userpost){
	//$user = LoginForm::find()->where(['_id' => $userpost['reporter_id']])->one();
	?>
	
            <tr>
				<td><a href="../frontend/web/index.php?r=site/travpost&postid=<?= $userpost['_id'];?>" target='_blank'>View</a></td>
				<td><?= $userpost['post_privacy'];?></td>
                <td><?= $userpost['post_text'];?></td>
                <td><?= $userpost['post_type'];?></td>
				<!--<td><a id="<?= $userpost['_id'];?>" onclick="publish_post('<?= $userpost['_id'];?>')">Publish</a></td>-->
				<td><a id="<?= $userpost['_id'];?>" onclick="delete_post('<?= $userpost['_id'];?>')" style="cursor: pointer;">Delete</a></td>
			</tr>

            <?php }?>
                
                </tbody>
              </table>
            </div>
			<script>
			function delete_post(id)
			{
				var r = confirm("Are you sure to delete this post?");
				if (r == false) {
					return false;
				}
				else 
				{
					$.ajax({
							url: '?r=post/deletepost', 
							type: 'POST',
							data: 'id=' + id,
							success: function (data) 
							{	
								var row = $("#"+id).parents('tr');
								$('#userpostlist').dataTable().fnDeleteRow(row); 
							}
						});
				}
			}
				
			function publish_post(id)
			{
				var r = confirm("Are you sure to Publish this post?");
				if (r == false) 
				{
					return false;
				}
				else 
				{
					$.ajax({
							url: '?r=post/publishpost', 
							type: 'POST',
							data: 'id=' + id,
							success: function (data) 
							{
								$("#"+id).parents('tr').remove();	
							}
						});
				}
			}
				
			</script>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
