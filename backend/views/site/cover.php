<?php 
use yii\helpers\Html;
$travelasset = backend\assets\TravelAsset::register($this);
$this->title = 'Cover';

$travelbaseUrl = $travelasset->baseUrl;
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Cover</h1>
     <?php  $session =
                    Yii::$app->session;
           echo  $email =
                    $session->get('username'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="col-lg-12 col-xs-12">
		  <div class="settings-page box">
			<div class="box-header">
              <h3 class="box-title">Add Cover Picture</h3>
            </div>
			<div class="box-body">
				<div class="settings-content-holder">
					<div id="menu-coverpic" class="coverpic-content settings-content" style="display:block;">					
						<div class="cover-settings">						
							<div class="row">

								<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12 cropping-section">
									<div class="uploadCoverpic-cropping">
										
										<!-- crop section -->
										<div class="crop-holder">
											<div id="coverpicCrop"></div>
										</div>									
									
									</div>
								</div>
							
								<div class="col-lg-4 col-md-5 col-sm-5 col-xs-12">

									<div class="uploadProfile-stuff">
										<p class="grayp">Upload home slider images here</p>
										
										<div class="fakeFileButton">
											<img src="<?=$travelbaseUrl?>/images/upload-green.png" /> &nbsp; Upload a photo from your computer
											<div class="form-group">
												<input type="file" id="coverpic-upload" accept="image/*">
											</div>									
										</div>
										
										<div class="btn-holder">						
											<input type="button" value="Save" class="btn btn-primary btn-sm clkbackcover">
											<input type="button" onclick="hidewebcam();" value="Cancel" class="btn btn-primary btn-sm">						
										</div>
									</div>
								</div>

							</div>
							<div class="coverpic-list gallery images-container">
								<div class="row">	
								
								<table id="CoverPhotos" class="table table-bordered table-striped">
								<thead>
								<tr>
								  <th>Number</th>
								  <th>Image</th>
								  <th>Action</th>
							   </tr>
								</thead>
								<tbody>
								<?php if(isset($cover) && !empty($cover)) 
								{ 
									$i = 1;
									foreach($cover as $coversingle) 
									{
									$name = $coversingle['cover_image'];
								?>
								<tr> 
									<td style="width: 10%"><?=$i?></td>
									<td style="width: 80%">
										<figure class="coverpicbox-holder">
										<a href="../frontend/web/uploads/cover/<?=$name?>" data-size="1600x1600" data-med="../frontend/web/uploads/cover/<?=$name?>" data-med-size="1024x1024" class="coverpic-box"><img src="../frontend/web/uploads/cover/<?=$name?>"/></a>
										</figure>
									</td>
									<td style="width: 10%"><a id ='<?=$coversingle['_id']?>' onclick="remove_cover('<?=$coversingle['_id']?>')" style="cursor: pointer;">Delete</a></td>
								</tr>

								<?php $i++; } } ?>
													 
								</tbody>
               
							</table>
								</div>
							</div>
						</div>
					
					</div>
				</div>
				
			</div>
		  </div>
        </div>
        
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script>
	
		document.getElementById('coverpic-upload').onchange = function (evt) 
		{
			var tgt = evt.target || window.event.srcElement,
			files = tgt.files;
			// FileReader support
			var fr = new FileReader();
			fr.onload = function (e) 
			{					
				coverpicCrop(e.target.result);
			}
			fr.readAsDataURL(files[0]);			
		}
		
		function coverpicCrop($imgPath) 
		{
			if($imgPath != '') 
			{
				var croppicContainerPreloadOptions = 
				{
					cropUrl:'?r=site/profileimagecrop',
					loadPicture:$imgPath,
					enableMousescroll:true,
					 onAfterImgCrop:function(data)
					 { 
						if(data != 'undefined' || data != undefined) 
						{
							 var $url = data.url;
							 if($url != 'undefined' || $url != undefined) 
							 {
								 $('#coverpicCrop').html('');
								 $selector = $('.coverpic-list').find('.row');
								 if($selector.length) 
								 {
									$id = data.id;
									$divs = '<figure class="coverpicbox-holder"><a href="'+$url+'" data-size="1600x1600" data-med="'+$url+'" data-med-size="1024x1024" class="coverpic-box"><img src="'+$url+'"/></a></figure>';
									
									var t = $('#CoverPhotos').DataTable();
									var page = t.page.info();
									var counter = page.recordsTotal+1;
									t.row.add( [counter, $divs, '<a id="'+$id+'" onclick="remove_cover(\''+$id+'\')" style="cursor: pointer;">Delete</a>'] ).draw( false );
								 }
							 }
						}
					}
				}
				var cropContainerPreload = new Croppic('coverpicCrop', croppicContainerPreloadOptions);
				
				cropContainerPreload.destroy();
				cropContainerPreload.reset();
			}
		}
		
		function remove_cover($id) 
		{
			if($id != undefined && $id != null) 
			{
			var r = confirm("Are you sure to delete this cover photo?");
			if (r == false) {
				return false;
			}
			else 
			{		

				$.ajax({
					url: '?r=site/removecover',
					type: 'POST',
					data: {$id},
					success: function (data) {			
						if(data == true) {
							var row = $("#"+$id).parents('tr');
							$('#CoverPhotos').dataTable().fnDeleteRow(row); 									
						}
					}
				});
			}	
			
			}
		}
		
  </script>