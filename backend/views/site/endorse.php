<?php 
use yii\helpers\Html;
$travelasset = backend\assets\TravelAsset::register($this);

$this->title = 'Endorsement';

$travelbaseUrl = $travelasset->baseUrl;
?>

<div class="content-wrapper addbuscat-admin">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Endorsement</h1>
     <?php  $session =
                    Yii::$app->session;
           echo  $email =
                    $session->get('username'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Add Endorsement</h3>
            </div>
			<div class="box-body">
				<form id="frm" class="topform">
					<div class="frow">
						<label>Please add endorsement name </label>&nbsp;
						<input type="text" name="name" id="name" required/><span class="name_notice" style="display: none"></span><br/>
					</div>
					<div class="frow">
						<input type="button" name="add" value="add" onclick="addbuscat()" class="btn btn-primary"/>  
						<input type="reset" name="clear" value="clear"  class="btn btn-primary"/>  
					</div>
				</form>
            </div>
            <script>
                function addbuscat(){
                    var name = $('#name').val();
                    if(name == '')
                    {
                        $('.name_notice').html('Please enter endorsement name');
                        $('.name_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
                        $("#name").focus();
                        return false;
                    }
                    else
                    {
                        $.ajax({
                            url: '?r=site/endorsement', 
                            type: 'POST',
                            data: 'name=' + name,
                            success: function (data) 
                            {
                                if(data == 'insert')
                                {
                                    $("#frm")[0].reset();
                                    $("#example1").load(window.location + " #example1");
                                }
                                else
                                {
                                    $('.name_notice').html('This endorsement exist');
                                    $('.name_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
                                }
                            }
                        });
                    }
                }
            </script>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
            <?php foreach($bus_cats as $bus_cat){ ?>
            <tr>
                <td><?= $bus_cat['name'];?></td>
				<td><a id="<?= $bus_cat['_id'];?>" style="cursor: pointer;" onclick="removesubcat('<?= $bus_cat['_id'];?>')">Delete</a></td>
            </tr>
            <?php }?>
                
                </tbody>
               
              </table>
            </div>
			<script>
			function removesubcat(id){
					var r = confirm("Are you sure to delete this endorsement?");
					if (r == false) {
						return false;
					}
					else 
					{
						$.ajax({
								url: '?r=site/removeendorsement', 
								type: 'POST',
								data: 'id=' + id,
								success: function (data) {
									$("#"+id).parents('tr').remove();	
								}
							});
					}
				}
			</script>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 
