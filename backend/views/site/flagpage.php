<?php 
use yii\helpers\Html;
use frontend\models\LoginForm;
use frontend\models\Notification;
$this->title = 'Flag Page';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Flag Page</h1>
      <!-- <ol class="breadcrumb">
        <li><a href="javascript:void(0)"><i class="mdi mdi-gauge"></i> Home</a></li>
        <li><a href="javascript:void(0)">Users</a></li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Page List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="flag_page" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th>View</th>
				  <th>Name</th>   
                  <th>created by</th>
                  <th>Reason</th>
				  <th>Publish</th>   
				  <th>Delete</th>   
                </tr>
                </thead>
                <tbody>
    <?php foreach($pagelists as $page){
	$reason = Notification::find()->where(['post_id' => (string)$page['page_id']])->one();
	$user = LoginForm::find()->where(['_id' => $page['created_by']])->one();
	?>
	
            <tr>
				<td><a href="../frontend/web/index.php?r=collection/detail&col_id=<?= $page['page_id'];?>" target='_blank'>View</a></td>
                <td><?= $page['page_name'];?></td>
				<td><?= $user['fullname'];?></td>
                <td><?= $reason['flag_reason'];?></td>
				<td><a id="<?= $page['page_id'];?>" onclick="publish_page('<?= $page['page_id'];?>')">Publish</a></td>
				<td><a id="<?= $page['page_id'];?>" onclick="delete_page('<?= $page['page_id'];?>')">Delete</a></td>
			</tr>

            <?php }?>
                
                </tbody>
              </table>
            </div>
			<script>
			function delete_page(page_id)
			{
				var r = confirm("Are you sure to delete this page?");
				if (r == false) {
					return false;
				}
				else 
				{
					$.ajax({
						type: 'POST',
						url: '?r=site/deletepage',
						data: 'page_id='+page_id,
						success: function(data)
						{
							$("#"+page_id).parents('tr').remove();
						}
					});
				}
			}
			
			function publish_page(page_id)
			{
				var r = confirm("Are you sure to publish this page?");
				if (r == false) {
					return false;
				}
				else 
				{
					$.ajax({
						type: 'POST',
						url: '?r=site/publishpage',
						data: 'page_id='+page_id,
						success: function(data)
						{
							$("#"+page_id).parents('tr').remove();
						}
					});
				}	
			}
			
			</script>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
