<?php 
use yii\helpers\Html;
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl;
$this->title = 'Dashboard';
?>

<div class="content-wrapper">
    <section class="content-header">
		<h1>Dashboard</h1>
	</section>
    <section class="content">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3><?= $usercounts;?></h3>
						<p>Users</p>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-account"></i>
					</div>
					<a href="?r=userdata/index" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="small-box bg-green">
					<div class="inner">
						<h3><?= $postcount;?></h3>
						<p>Posts</p>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-file"></i>
					</div>
					<a href="javascript:void(0)" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="small-box bg-red">
					<div class="inner">
						<h3><?= $messagecount;?></h3>
						<p>Messages</p>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-email"></i>
					</div>
					<a href="javascript:void(0)" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="small-box bg-blue">
					<div class="inner">
						<h3><?= $photocount;?></h3>
						<p>Photos</p>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-image"></i>
					</div>
					<a href="javascript:void(0)" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="small-box bg-red">
					<div class="inner">
						<h3><?= $tripexpcount;?></h3>
						<p>Trip exprience</p>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-airplane"></i>
					</div>
					<a href="?r=tripexp/all" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
				</div>
			</div>
			
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="small-box bg-purple">
					<div class="inner">
						<h3><?= $pagecount;?></h3>
						<p>Pages</p>
					</div>
					<div class="icon">
						<i class="ionsize mdi mdi-file-powerpoint"></i>
					</div>
					<a href="?r=page/all" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>20</h3>
						<p>Collection</p>
					</div>
					<div class="icon">
						<i class="ionsize mdi mdi-file-outline"></i>
					</div>
					<a href="?r=collection/all" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="small-box bg-blue">
					<div class="inner">
						<h3><?= $adscount;?></h3>
						<p>Ads</p>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-font"></i>
					</div>
					<a href="?r=ads/all" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
				</div>
			</div>
			
			
			
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="small-box bg-purple">
					<div class="inner">
						<h3><?= $hotelcount;?></h3>
						<p>Hotels</p>
					</div>
					<div class="icon">
						<i class="ionsize mdi mdi-hotel"></i>
					</div>
					<a href="javascript:void(0)" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3><?= $tourcount;?></h3>
						<p>Tours</p>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-car"></i>
					</div>
					<a href="?r=tours/all" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="small-box bg-green">
					<div class="inner">
						<h3><?= $placecount;?></h3>
						<p>Places</p>
					</div>
					<div class="icon">
						<i class="ionsize mdi mdi-sign-direction"></i>
					</div>
					<a href="?r=places/all" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
              <!-- USERS LIST -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Latest Members</h3>

                  <div class="box-tools right">
                    <span class="label label-danger">8 New Members</span>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="zmdi zmdi-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="zmdi zmdi-close"></i>
                    </button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
					<ul class="users-list clearfix">
						<?php foreach($userdashs as $userdash)
						{
							$id = (string)$userdash['_id'];
							$gender = isset($userdash['gender']) ? $userdash['gender'] : 'Male';
							$default_usr = $gender.'.jpg'; 
							$dp = $this->context->getimageAPI($id,'photo');
							if(substr($dp,0,7) == 'profile') {
								$dp = '../'.$dp;
							}

							$dp = str_replace("../../vendor","../vendor",$dp);
							if(!file_exists($dp)) {
								$dp = $baseUrl.'/images/'.$default_usr;
							}
						?>	
						<li>
						  <img src="<?= $dp;?>" >
						  <?= $userdash['fname'];?>
						  <span class="users-list-date"><?=  date("d M",$userdash['created_date']);?></span>
						</li>
						<?php }?>
					</ul>
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="?r=userdata/index" class="uppercase">View All Users</a>
                </div>
                <!-- /.box-footer -->
              </div>
              <!--/.box -->
            </div>
			<div class="col-md-6">
		    </div>
          <!-- /.box -->
		</div>
	</section>
</div>

