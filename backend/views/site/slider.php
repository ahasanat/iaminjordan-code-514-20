<?php 
use yii\helpers\Html;
$travelasset = backend\assets\TravelAsset::register($this);

$this->title = 'Slider';

$travelbaseUrl = $travelasset->baseUrl;
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Slider</h1>
     <?php  $session =
                    Yii::$app->session;
           echo  $email =
                    $session->get('username'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="col-lg-12 col-xs-12">
          <!-- small box -->
		  <!--
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $usercounts;?></h3>

              <p>Slider for home page</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="?r=userdata/index" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
          </div>-->
		  <div class="settings-page box">
			<div class="box-header">
              <h3 class="box-title">Add Slider</h3>
            </div>
			<div class="box-body">
				<div class="settings-content-holder">
					<div id="menu-slider" class="slider-content settings-content" style="display:block;">					
						<div class="slider-settings">						
							<div class="row">

								<div class="col-lg-8 col-md-7 col-sm-7 col-xs-12 cropping-section">
									<div class="uploadSlider-cropping">
										
										<!-- crop section -->
										<div class="crop-holder">
											<div id="sliderCrop"></div>
										</div>									
									
									</div>
								</div>
							
								<div class="col-lg-4 col-md-5 col-sm-5 col-xs-12">

									<div class="uploadProfile-stuff">
										<p class="grayp">Upload home slider images here</p>
										
										<div class="fakeFileButton">
											<img src="<?=$travelbaseUrl?>/images/upload-green.png" /> &nbsp; Upload a photo from your computer
											<div class="form-group">
												<input type="file" id="slider-upload" accept="image/*">
												<input type="file" name="slider_image[]" id="slider_image" accept="image/*">
											</div>									
										</div>
										
										<div class="btn-holder">						
											<input type="submit" value="Save" class="btn btn-primary btn-sm" id="uploadsave">
											<input type="button" onclick="hidewebcam();" value="Cancel" class="btn btn-primary btn-sm">						
										</div>

									</div>
								</div>

							</div>
							<div class="slider-list gallery images-container">
								<div class="row">	
									<figure class="sliderbox-holder">
										<a href="<?=$travelbaseUrl?>/images/home-bg-1.jpg" data-size="1600x1600" data-med="<?=$travelbaseUrl?>/images/home-bg-1.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="slider-box"><img src="<?=$travelbaseUrl?>/images/home-bg-1.jpg"/></a>
									</figure>
									<figure class="sliderbox-holder">
										<a href="<?=$travelbaseUrl?>/images/home-bg-2.jpg" data-size="1600x1600" data-med="<?=$travelbaseUrl?>/images/home-bg-2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="slider-box"><img src="<?=$travelbaseUrl?>/images/home-bg-2.jpg"/></a>
									</figure>
									<figure class="sliderbox-holder">
										<a href="<?=$travelbaseUrl?>/images/home-bg-3.jpg" data-size="1600x1600" data-med="<?=$travelbaseUrl?>/images/home-bg-3.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="slider-box"><img src="<?=$travelbaseUrl?>/images/home-bg-3.jpg"/></a>
									</figure>
									<figure class="sliderbox-holder">
										<a href="<?=$travelbaseUrl?>/images/home-bg-4.jpg" data-size="1600x1600" data-med="<?=$travelbaseUrl?>/images/home-bg-4.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="slider-box"><img src="<?=$travelbaseUrl?>/images/home-bg-4.jpg"/></a>
									</figure>
									<figure class="sliderbox-holder">
										<a href="<?=$travelbaseUrl?>/images/home-bg-5.jpg" data-size="1600x1600" data-med="<?=$travelbaseUrl?>/images/home-bg-5.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="slider-box"><img src="<?=$travelbaseUrl?>/images/home-bg-5.jpg"/></a>
									</figure>
								</div>
							</div>
						</div>
						<div style="clear: both;"></div>
							<div class="form-group consider-div slider_image">
							</div>
			
					</div>

						
		  
		  <!-- START Repace Slider Image Popup -->
		<!--<div id="take-photo" class="white-popup-block mfp-hide takephoto" data-myval="">
			<div class="modal-title graytitle">       
				<a class="popup-modal-dismiss popup-modal-close" href="javascript:void(0)"><i class="zmdi zmdi-close"></i></a>
			</div>
			<div class="modal-detail"> 
				
				<div class="row profpic-settings profilepic-sec">
					<div id="main-picture">
						<div id="profilepreview">
							<div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 form-ppoptions">					
								<div class="opt-div">
									<div class="fakeFileButton">
										<i class="zmdi zmdi-upload"></i>Upload a photo for Slider
										
										
										<div class="form-group">
											<input type="file" name="slider_image[]" id="slider_image" accept="image/*">
										</div>
										
										<input type="hidden" name="load" value"edit">
										<input type="hidden" id="getreplaceboxidinput">
										<input type="hidden" id="parendiv">
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group consider-div">						
					<div class="right fb-btnholder">																		
						<input type="button" class="btn btn-primary btn-sm" value="Save" id="replaceslider">
						<input type="button" class="btn btn-primary btn-sm" value="Cancel" onClick="resetALL();">						
					</div>										
				</div>
				
			</div>
		</div>-->
	<!-- END Repace Slider Image Popup -->
		  

				</div>			 
			</div>
		 </div>

		</div>
		<script>
		
			document.getElementById('slider_image').onchange = function (evt) {
			var tgt = evt.target || window.event.srcElement,
			files = tgt.files;
			// FileReader support
			var fr = new FileReader();
			fr.onload = function (e) {				
				profileCrop(e.target.result);
			}
			fr.readAsDataURL(files[0]);
			
		}
		
		function profileCrop($imgPath) {
		 if($imgPath == '') {
			$imgPath = 'images/night.jpg';
		}   

		var profileCropOptions = {
		//cropUrl:'?r=site/profile-image-crop',
		loadPicture:$imgPath,
		enableMousescroll:true
		
	   
		}


		var profileCrop = new Croppic('sliderCrop', profileCropOptions);
	}
		
		</script>
        <!-- ./col -->
        <!-- <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="javascript:void(0)" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
          </div>
        </div> -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 
