<?php 
use yii\helpers\Html;
use frontend\models\LoginForm;
$this->title = 'Verify Member Listing';
//echo "<pre>"; print_r($country); die();
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Verify</h1>
      <!-- <ol class="breadcrumb">
        <li><a href="javascript:void(0)"><i class="mdi mdi-gauge"></i> Home</a></li>
        <li><a href="javascript:void(0)">Users</a></li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
		<!--<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="small-box bg-yellow">
					<div class="inner">
						<p>Total</p>
						<h3><?= array_sum($country)?></h3>
						<p>Verify Users</p>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-account"></i>
					</div>
				</div>
			</div>
		</div>-->
		<div class="row">	
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Verify Member List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="verify_statastics" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Country Name</th>
                  <th>No of Verify Members</th>
                </tr>
                </thead>
                <tbody>
    <?php foreach($country as $key => $singlecountry){ 
		//	$country = LoginForm::find()->where(['_id' => $userdata['user_id']])->one();
			?>
            <tr>
                <td><?= $key;?></td>
                <td><?= $singlecountry;?></td>
                
                </tr>

            <?php }?>
                
                </tbody>
              </table>
			  
			  <table id="verify_gender" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Gender</th>
                  <th>No of Vip Members</th>
                </tr>
                </thead>
                <tbody>
    <?php foreach($gender as $key => $singlegender){ 
		//	$country = LoginForm::find()->where(['_id' => $userdata['user_id']])->one();
			?>
            <tr>
                <td><?= $key;?></td>
                <td><?= $singlegender;?></td>
                
                </tr>

            <?php }?>
                
                </tbody>
              </table>
			  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
