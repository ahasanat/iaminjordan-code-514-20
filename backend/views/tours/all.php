<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\Like;
use frontend\models\TravAdsVisitors;
$this->title = 'Tours';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Tours</h1>
    </section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Tour List</h3>
					</div>
					<div class="box-body">
						<table id="alltourslist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Image</th>
								  <th>Name</th>
								  <th>City</th>
								  <th>Country</th>
								  <th>Take Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($tours as $tour)
								{
									$tourid = (string)$tour['_id'];
								?>
									<tr id="tour_<?=$tourid?>">
										<td><img src="<?=$tour['ProductImage'];?>" alt="" height="25px" width="25px"/></td>
										<td><?=$tour['ProductName'];?></td>
										<td><?=$tour['City'];?></td>
										<td><?=$tour['Country'];?></td>
										<td>
											<a target="_blank" href="<?=$tour['ProductURL'];?>">View</a> / <a id = "remove_<?= $tourid;?>" href="javascript:void(0)" onclick="remove('<?= $tourid;?>')">Delete</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this tour details?");
	if(r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=tours/remove', 
			type: 'POST',
			data: 'id='+id,
			success: function (data){
				$('#tour_'+id).html('');
				$('#tour_'+id).remove();
				var row = $("#"+id).parents('tr');
				$('#alltourslist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
</script>