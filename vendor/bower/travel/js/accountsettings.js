var $view_photos_custom = [];
var $my_post_view_status_custom = [];
var $add_public_wall_custom = [];
var $add_post_on_your_wall_view_custom = [];

var $restricted_list_label = [];
var $blocked_list_label = [];
var $blocked_event_invites_label = [];
var $message_filter_label = [];
var $request_filter_label = [];

var $isview_photos_custom = 'no';
var $ismy_post_view_status_custom = 'no';
var $isadd_public_wall_custom = 'no';
var $isadd_post_on_your_wall_view_custom = 'no';

var $isrestricted_list_label = 'no';
var $isblocked_list_label = 'no';
var $isblocked_event_invites_label = 'no';
var $ismessage_filter_label = 'no';
var $isrequest_filter_label = 'no';

$(document).on('click', '#customdone', function() {
	$bulkomg = customArrayTemp.slice();
	if($isview_photos_custom == 'yes') { 
		$view_photos_custom = $bulkomg;
	}

	if($ismy_post_view_status_custom == 'yes') {
		$my_post_view_status_custom = $bulkomg;
	}

	if($isadd_public_wall_custom == 'yes') {
		$add_public_wall_custom = $bulkomg;
	}

	if($isadd_post_on_your_wall_view_custom == 'yes') {
		$add_post_on_your_wall_view_custom = $bulkomg;
	}

	if($isrestricted_list_label == 'yes') {
		$restricted_list_label = $bulkomg;
	}

	if($isblocked_list_label == 'yes') {
		$blocked_list_label = $bulkomg;
	}

	if($isblocked_event_invites_label == 'yes') {
		$blocked_event_invites_label = $bulkomg;
	}

	if($ismessage_filter_label == 'yes') {
		$message_filter_label = $bulkomg;
	}

	if($isrequest_filter_label == 'yes') {
		$request_filter_label = $bulkomg;
	}
})

$(document).on('click', '.restricted_list_label, .blocked_list_label, .blocked_event_invites_label, .message_filter_label, .request_filter_label', function() {
	if($(this).hasClass('restricted_list_label')) {
		addUserForAccountSettingsArrayTemp = $restricted_list_label;
		addUserForAccountSettingsArray = $restricted_list_label;

		$isrestricted_list_label = 'yes';
		$isblocked_list_label = 'no';
		$isblocked_event_invites_label = 'no';
		$ismessage_filter_label = 'no';
		$isrequest_filter_label = 'no';
		
		whoisopeninblock = 'restricted_list_label';
	}

	if($(this).hasClass('blocked_list_label')) {
		addUserForAccountSettingsArrayTemp = $blocked_list_label;
		addUserForAccountSettingsArray = $blocked_list_label;

		$isrestricted_list_label = 'no';
		$isblocked_list_label = 'yes';
		$isblocked_event_invites_label = 'no';
		$ismessage_filter_label = 'no';
		$isrequest_filter_label = 'no';
		whoisopeninblock = 'blocked_list_label';
	}

	if($(this).hasClass('blocked_event_invites_label')) {
		addUserForAccountSettingsArrayTemp = $blocked_event_invites_label;
		addUserForAccountSettingsArray = $blocked_event_invites_label;

		$isrestricted_list_label = 'no';
		$isblocked_list_label = 'no';
		$isblocked_event_invites_label = 'yes';
		$ismessage_filter_label = 'no';
		$isrequest_filter_label = 'no';
		whoisopeninblock = 'blocked_event_invites_label';
	}

	if($(this).hasClass('message_filter_label')) {
		addUserForAccountSettingsArrayTemp = $message_filter_label;
		addUserForAccountSettingsArray = $message_filter_label;

		$isrestricted_list_label = 'no';
		$isblocked_list_label = 'no';
		$isblocked_event_invites_label = 'no';
		$ismessage_filter_label = 'yes';
		$isrequest_filter_label = 'no';
		whoisopeninblock = 'message_filter_label';
	}

	if($(this).hasClass('request_filter_label')) {
		addUserForAccountSettingsArrayTemp = $request_filter_label;
		addUserForAccountSettingsArray = $request_filter_label;

		$isrestricted_list_label = 'no';
		$isblocked_list_label = 'no';
		$isblocked_event_invites_label = 'no';
		$ismessage_filter_label = 'no';
		$isrequest_filter_label = 'yes';
		whoisopeninblock = 'request_filter_label';
	}
});

$(document).on('click', '.view_photos, .my_post_view_status, .add_public_wall, .add_post_on_your_wall_view', function() {
	if($(this).hasClass('view_photos')) {
		customArrayTemp = $view_photos_custom;
		customArray = $view_photos_custom;
		$isview_photos_custom = 'yes';
		$ismy_post_view_status_custom = 'no';
		$isadd_public_wall_custom = 'no';
		$isadd_post_on_your_wall_view_custom = 'no';
	}

	if($(this).hasClass('my_post_view_status')) {
		customArrayTemp = $my_post_view_status_custom;
		customArray = $my_post_view_status_custom;
		$isview_photos_custom = 'no';
		$ismy_post_view_status_custom = 'yes';
		$isadd_public_wall_custom = 'no';
		$isadd_post_on_your_wall_view_custom = 'no';
	}

	if($(this).hasClass('add_public_wall')) {
		customArrayTemp = $add_public_wall_custom;
		customArray = $add_public_wall_custom;
		$isview_photos_custom = 'no';
		$ismy_post_view_status_custom = 'no';
		$isadd_public_wall_custom = 'yes';
		$isadd_post_on_your_wall_view_custom = 'no';
	}

	if($(this).hasClass('add_post_on_your_wall_view')) {
		customArrayTemp = $add_post_on_your_wall_view_custom;
		customArray = $add_post_on_your_wall_view_custom;
		$isview_photos_custom = 'no';
		$ismy_post_view_status_custom = 'no';
		$isadd_public_wall_custom = 'no';
		$isadd_post_on_your_wall_view_custom = 'yes';
	}


});

$(window).resize(function() {
	if($('#menu-security').hasClass('active')) {
		close_all_edit();
	}	
})

/* document ready */
	$(document).ready(function() {
		/************ SETTINGS FUNCTIONS ************/
 
			fetchlanguagedropdown();
			fetchinterestsdropdown();
			fetcheducationdropdown();
			fetchoccupationdropdown();

			initDropdown();

			$('.chips').material_chip();
			$('.chips-initial').material_chip({
			data: [{
			  tag: 'Apple',
			}, {
			  tag: 'Microsoft',
			}, {
			  tag: 'Google',
			}],
			});
			$('.chips-placeholder').material_chip({
			placeholder: 'Enter a tag',
			secondaryPlaceholder: '+Tag',
			});
			$('.chips-autocomplete').material_chip({
			autocompleteOptions: {
			  data: {
			    'Apple': null,
			    'Microsoft': null,
			    'Google': null
			  },
			  limit: Infinity,
			  minLength: 1
			}
			});


			var cropper = $('.cropper');
			if (cropper.length) {
			$.each(cropper, function(i, val) {
			var uploadCrop;

			var readFile = function(input) {
			  if (input.files && input.files[i]) {
			    var reader = new FileReader();

			    reader.onload = function(e) {
			      uploadCrop.croppie('bind', {
			        url: e.target.result
			      });
			    };

			    reader.readAsDataURL(input.files[i]);
			  } else {
			    alert("Sorry - you're browser doesn't support the FileReader API");
			  }
			};

			uploadCrop = $('.js-cropping').croppie({ // TODO: fix so its selects right element
			  viewport: {
			    width: 200,
			    height: 200
			  },
			  boundary: {
			    width: 350,
			    height: 355
			  },
			  enableOrientation: true
			});

			$('.js-cropper-upload').on('change', function() {
			  $('.crop').show(); // TODO: fix so its selects right element
			  readFile(this);
			});

			$('.js-cropper-rotate--btn').on('click', function(ev) {
			  uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
			});

			$('.js-cropper-result--btn').on('click', function(ev) {
			  uploadCrop.croppie('result', 'canvas').then(function(resp) {
			    popupResult({
			      src: resp
			    });
			  });
			});

			var popupResult = function(result) {
				if(result.src != undefined && result.src != '') {
					$.ajax({
					    type: 'POST',
					    url: "?r=site/profile-image-crop",
					    data: {
					    	file: result.src
					   	},
					    success: function (data) {
					    	var $data = $.parseJSON(data);
					    	if($data.status != undefined && $data.status == 'success') {
								if($data.url != undefined && $data.url != '') {
									Materialize.toast('Profile picture changed.', 2000, 'green'); 
									$('.setting-pic').find('img').attr('src', $data.url);
									$('.profile-top').find('img').attr('src', $data.url);
									var $html = '<img src="' + $data.url + '" />';
									$('.js-cropper-result').show();	
									$('.js-cropper-result').html($html);
									$('.crop').hide();
									$('.image-upload').show();
								}
							}
					    }
					});
				}
			};
			});
			}
			
			$(document).on('click', '#menu-security .dropdown-content li', function(){
	            var thisvalue = $(this).find('a').text().trim();
	            $(this).closest('div').find('.dropdown-button').find('span').first().html(thisvalue);
		    });


			/* Blocking */
			$('.sidemenu-setting ul li').click(function(){ 
				$('.sidemenu-setting ul li').removeClass("active");
				$(this).addClass("active");
			});
			$('radio-holder').click(function(){
				$(".others-click").css("display", "none");		
			});
 
			$(".multiple-select-dropdown li").click(function(){		
				$(".dropdown-content.select-dropdown.multiple-select-dropdown").css('display','none');
			});

			$('#menu-security .selectore').on('click', function() {
				$selectore = $(this).find('a').html().trim();
				if($(this).parents('li').find('.getvalue').length) {
					$(this).parents('li').find('.getvalue').html($selectore);
				}
			});

			/* Settings Side Menu */
			if($(".settings-page").length){
				$("body").on("click", "ul.submenu li a", function(){setPageMainTab("settings",this);});
				$("body").on("click", "ul#settings-menu li.has_sub a.mainlink", function(){setSettingsMenu(this);});
			}
			/* End Settings Side Menu */
			
			/*Show password on click*/
			$('.showPass').mousedown(function(){
			  
			  var getParent = $(this).parent();
			  
			  var obj = getParent.find("input").first();      
			  obj.attr("type","text");

			  
			  }).mouseup(function() {
			   
			   var getParent = $(this).parent();
			   
			   var obj = getParent.find("input").first();    
			   obj.attr("type","password");
			   
			  });
			/*End showpass*/
			
			/* theme box : togle body theme	*/
			$( ".theme-drawer a" ).bind( "click", function() {
		
				var clickedClass = $(this).attr('body-color'); // or var clickedBtnID = this.id
				
				var bodyclass=$("body").attr("class");
				var arr = bodyclass.split(' ');
				for(var i=0;i<arr.length;i++){
					if(arr[i].toLowerCase().indexOf("theme-") >= 0){
						arr=$.grep(arr, function(value) {
						  return value != arr[i];
						});
						break;
					}					
				}
				var newBclass= arr.toString();
				newBclass=newBclass.replace(/,/g,' ');
				
				$("body").attr("class",newBclass);
				$("body").addClass(clickedClass);
				setCookie('bodyClass',clickedClass);		
			});			
			/* end theme box : togle body theme	*/
		
		/************ END SETTINGS FUNCTIONS ************/
	});
/* end document ready */

function securitysettingssomeparams(){
	$.ajax({
		url: '?r=userwall/securitysettingssomeparams', 
		success: function (data) {
			var result = $.parseJSON(data);
			$view_photos_custom = result.view_photos_custom;
			$my_post_view_status_custom = result.my_post_view_status_custom;
			$add_public_wall_custom = result.add_public_wall_custom;
			$add_post_on_your_wall_view_custom = result.add_post_on_your_wall_view_custom;
		}
	});
}

function blockingsomeparams(){
	$.ajax({
		url: '?r=userwall/blockingsomeparams', 
		success: function (data) {
			var result = $.parseJSON(data);
			$restricted_list_label = result.restricted_list_label;
			$blocked_list_label = result.blocked_list_label;
			$blocked_event_invites_label = result.blocked_event_invites_label;
			$message_filter_label = result.message_filter_label;
			$request_filter_label = result.request_filter_label;
		}
	});
}

function open_edit_act_blocking() {
	$('#menu-block').find('.editicon1').hide();
	$('#menu-block').find('.editicon1').addClass('dis-none');
	$('#menu-block').find('.editicon1').css('display', 'none');

	$('#menu-block').find('.editicon2').hide();
	$('#menu-block').find('.editicon2').addClass('dis-none');
	$('#menu-block').find('.editicon2').css('display', 'none');
 
	$.ajax({
        url: "?r=account/getblockingedit",
        beforeSend: function() {
        },
        success: function (data) {
        	$('#menu-block').find('.normal-part').addClass('dis-none');
			$('#menu-block').find('.normal-part').hide()
			$('#menu-block').find('.edit-part').removeClass('dis-none');
			$('#menu-block').find('.edit-part').show();
			$('#menu-block').find('.edit-part').html(data);
			$('#menu-block').find('.edit-part').find('.edit-mode').show();
			$('select').material_select();
			$('.dropdown-button').dropdown();

			$('html, body').animate({
			    scrollTop: 0
			}, 400);

			$(".main-content.with-lmenu").scrollTop(0);

			blockingsomeparams();
        }
    });
}

function open_edit_act_blocking_cl(dsik) {
	var w = $(window).width();
	if(w<=768) {
		$('#menu-block').find('.editicon2').show();
		$('#menu-block').find('.editicon2').removeClass('dis-none');

		$('#menu-block').find('.editicon1').hide();
		$('#menu-block').find('.editicon1').addClass('dis-none');
	} else {
		$('#menu-block').find('.editicon1').show();
		$('#menu-block').find('.editicon1').removeClass('dis-none');

		$('#menu-block').find('.editicon2').hide();
		$('#menu-block').find('.editicon2').addClass('dis-none');
	}

	if(dsik) {
		$.ajax({
			type: 'POST', 
			url: "?r=site/blockingsave",
			data: {
				restricted_list_label : $restricted_list_label,
				blocked_list_label : $blocked_list_label,
				blocked_event_invites_label : $blocked_event_invites_label,
				message_filter_label : $message_filter_label,
				request_filter_label : $request_filter_label
			},
	        beforeSend: function() {
	        },
	        success: function (data) {
	        	var result = $.parseJSON(data);
	        	if(result.success != undefined && (result.success == 'false' || result.success == false)) {
	        		if(result.msg != undefined && (result.msg != '')) {
	        			$msg = result.msg;
	        			Materialize.toast($msg, 2000, 'red');
	        		}
	        	} else {
	        		Materialize.toast('Saved.', 2000, 'green');

	        		$.ajax({
				        url: "?r=account/getblockingnormal",
				        beforeSend: function() {
				        },
				        success: function (data) {
							$('#menu-block').find('.edit-part').addClass('dis-none');
							$('#menu-block').find('.edit-part').hide();
							$('#menu-block').find('.edit-part').find('.edit-mode').hide();
							$('#menu-block').find('.normal-part').removeClass('dis-none');
							$('#menu-block').find('.normal-part').show().html(data);

							$('html, body').animate({
							    scrollTop: 0
							}, 400);

							$(".main-content.with-lmenu").scrollTop(0);
				        }
				    });


				}
	        }
	    });
	} else {
		$.ajax({
	        url: "?r=account/getblockingnormal",
	        beforeSend: function() {
	        },
	        success: function (data) {
				$('#menu-block').find('.edit-part').addClass('dis-none');
				$('#menu-block').find('.edit-part').hide();
				$('#menu-block').find('.edit-part').find('.edit-mode').hide();
				$('#menu-block').find('.normal-part').removeClass('dis-none');
				$('#menu-block').find('.normal-part').show().html(data);

				$('html, body').animate({
				    scrollTop: 0
				}, 400);

				$(".main-content.with-lmenu").scrollTop(0);
	        }
	    });
	}
}

function open_edit_act_ss() {
	$('#menu-security').find('.editicon1').hide();
	$('#menu-security').find('.editicon1').addClass('dis-none');
	$('#menu-security').find('.editicon1').css('display', 'none');

	$('#menu-security').find('.editicon2').hide();
	$('#menu-security').find('.editicon2').addClass('dis-none');
	$('#menu-security').find('.editicon2').css('display', 'none');

	$.ajax({
        url: "?r=account/getsecuritysettingsedit",
        beforeSend: function() {
        	//$('#menu-close').html('<div class="generalbox-list"> <div class="row"><center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center></div> </div>')
        },
        success: function (data) {
    		$('#menu-security').find('.normal-part').addClass('dis-none');
			$('#menu-security').find('.normal-part').hide()
			$('#menu-security').find('.edit-part').removeClass('dis-none');
			$('#menu-security').find('.edit-part').show();
			$('#menu-security').find('.edit-part').html(data);
			$('#menu-security').find('.edit-part').find('.edit-mode').show();
			$('select').material_select();
			$('.dropdown-button').dropdown();

			$('html, body').animate({
			    scrollTop: 0
			}, 400);

			$(".main-content.with-lmenu").scrollTop(0);

			securitysettingssomeparams();
        }
    });
}

function open_edit_act_ss_cl(dsik) {
	var w = $(window).width();
	if(w<=768) {
		$('#menu-security').find('.editicon2').show();
		$('#menu-security').find('.editicon2').removeClass('dis-none');

		$('#menu-security').find('.editicon1').hide();
		$('#menu-security').find('.editicon1').addClass('dis-none');
	} else {
		$('#menu-security').find('.editicon1').show();
		$('#menu-security').find('.editicon1').removeClass('dis-none');

		$('#menu-security').find('.editicon2').hide();
		$('#menu-security').find('.editicon2').addClass('dis-none');
	}

	if(dsik) {
		var securityquestion = $("#securityquestion").val();
		var securityanswer = $("#securityanswer").val();
		var my_view_status = $("#my_view_status").find('span').first().text();
		var connect_request = $("#connect_request").find('span').first().text();
		var connect_list = $("#connect_list").find('span').first().text();
		var view_photos = $("#view_photos").find('span').first().text();
		var my_post_view_status = $("#my_post_view_status").find('span').first().text();
		var add_public_wall = $("#add_public_wall").find('span').first().text();
		var review_posts = $("#review_posts").find('span').first().text();
		var review_tags = $("#review_tags").find('span').first().text();
		var add_post_on_your_wall_view = $("#add_post_on_your_wall_view").find('span').first().text();

		$.ajax({
			type: 'POST', 
			url: "?r=site/accountsettingsecuritysettings",
			data: {
				securityquestion: securityquestion,
				securityanswer: securityanswer,
				my_view_status: my_view_status,
				connect_request: connect_request,
				connect_list: connect_list,
				view_photos: view_photos,
				my_post_view_status: my_post_view_status,
				add_public_wall: add_public_wall,
				review_posts: review_posts,
				review_tags: review_tags,
				add_post_on_your_wall_view: add_post_on_your_wall_view,
				view_photos_custom: $view_photos_custom,
				my_post_view_status_custom: $my_post_view_status_custom,
				add_public_wall_custom: $add_public_wall_custom,
				add_post_on_your_wall_view_custom: $add_post_on_your_wall_view_custom
			},
	        beforeSend: function() {
	        	//$('#menu-close').html('<div class="generalbox-list"> <div class="row"><center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center></div> </div>')
	        },
	        success: function (data) {
	        	var result = $.parseJSON(data);
	        	if(result.success != undefined && (result.success == 'false' || result.success == false)) {
	        		if(result.msg != undefined && (result.msg != '')) {
	        			$msg = result.msg;
	        			Materialize.toast($msg, 2000, 'red');
	        		}
	        	} else {
	        		Materialize.toast('Saved.', 2000, 'green');

	        		$.ajax({
				        url: "?r=account/getsecuritysettingsnormal",
				        beforeSend: function() {
				        	//$('#menu-close').html('<div class="generalbox-list"> <div class="row"><center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center></div> </div>')
				        },
				        success: function (data) {
							$('#menu-security').find('.edit-part').addClass('dis-none');
							$('#menu-security').find('.edit-part').hide();
							$('#menu-security').find('.edit-part').find('.edit-mode').hide();
							$('#menu-security').find('.normal-part').removeClass('dis-none');
							$('#menu-security').find('.normal-part').show().html(data);

							$('html, body').animate({
							    scrollTop: 0
							}, 400);

							$(".main-content.with-lmenu").scrollTop(0);
				        }
				    });


				}
	        }
	    });
	} else {
		$.ajax({
	        url: "?r=account/getsecuritysettingsnormal",
	        beforeSend: function() {
	        	//$('#menu-close').html('<div class="generalbox-list"> <div class="row"><center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center></div> </div>')
	        },
	        success: function (data) {
				$('#menu-security').find('.edit-part').addClass('dis-none');
				$('#menu-security').find('.edit-part').hide();
				$('#menu-security').find('.edit-part').find('.edit-mode').hide();
				$('#menu-security').find('.normal-part').removeClass('dis-none');
				$('#menu-security').find('.normal-part').show().html(data);
				
				$('html, body').animate({
				    scrollTop: 0
				}, 400);

				$(".main-content.with-lmenu").scrollTop(0);
	        }
	    });
	}
}

function open_edit_act_bf() {
	$('#menu-basicinfo').find('.editicon1').hide();
	$('#menu-basicinfo').find('.editicon1').addClass('dis-none');
	$('#menu-basicinfo').find('.editicon1').css('display', 'none');

	$('#menu-basicinfo').find('.editicon2').hide();
	$('#menu-basicinfo').find('.editicon2').addClass('dis-none');
	$('#menu-basicinfo').find('.editicon2').css('display', 'none');

	$.ajax({
        url: "?r=account/getbasicinformationedit",
        beforeSend: function() {
        	//$('#menu-close').html('<div class="generalbox-list"> <div class="row"><center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center></div> </div>')
        },
        success: function (data) {
			$('#menu-basicinfo').find('.normal-part').addClass('dis-none');
			$('#menu-basicinfo').find('.normal-part').hide()
			$('#menu-basicinfo').find('.edit-part').removeClass('dis-none');
			$('#menu-basicinfo').find('.edit-part').show();
			$('#menu-basicinfo').find('.edit-part').html(data);
			$('#menu-basicinfo').find('.edit-part').find('.edit-mode').show();
			$('select').material_select();

			$('html, body').animate({
			    scrollTop: 0
			}, 400);

			$(".main-content.with-lmenu").scrollTop(0);
        }
    });
}
 
function open_edit_act_bf_cl(dsik) {
	var w = $(window).width();
	if(w<=768) {
		$('#menu-basicinfo').find('.editicon2').show();
		$('#menu-basicinfo').find('.editicon2').removeClass('dis-none');

		$('#menu-basicinfo').find('.editicon1').hide();
		$('#menu-basicinfo').find('.editicon1').addClass('dis-none');
	} else {
		$('#menu-basicinfo').find('.editicon1').show();
		$('#menu-basicinfo').find('.editicon1').removeClass('dis-none');

		$('#menu-basicinfo').find('.editicon2').hide();
		$('#menu-basicinfo').find('.editicon2').addClass('dis-none');
	}

	if(dsik) {
		var fname = $("#fname").val();
		var lname = $("#lname").val();
		var email = $("#bemail").val();
		var alternate_email = $("#alternate_email").val();
		var city = $("#autocomplete1").val();
		var country = $("#country").val();
		var isd_code = $("#isd_code").val();
		var phone = $("#phone").val();
		var about = $("#personalinfo-about").val();
		var language = $('#language1').val();
		var interests = $('#interests1').val();
		var education = $('#education1').val();
		var occupations = $('#occupations1').val();

		$.ajax({
			type: 'POST',  
			url: "?r=site/accountsettingbasicinformation",
			data: {
				fname: fname,
				lname: lname,
				email: email,
				alternate_email: alternate_email,
				city: city,
				country: country,
				isd_code: isd_code,
				phone: phone,
				about: about,
				language: language,
				interests: interests,
				education: education,
				occupations: occupations
			},
	        beforeSend: function() {
	        	//$('#menu-close').html('<div class="generalbox-list"> <div class="row"><center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center></div> </div>')
	        },
	        success: function (data) {
	        	var result = $.parseJSON(data);
	        	if(result.success != undefined && (result.success == 'false' || result.success == false)) {
	        		if(result.msg != undefined && (result.msg != '')) {
	        			$msg = result.msg;
	        			Materialize.toast($msg, 2000, 'red');
	        		}
	        	} else {
	        		Materialize.toast('Saved.', 2000, 'green');

	        		$.ajax({
				        url: "?r=account/getbasicinformationnormal",
				        beforeSend: function() {
				        	//$('#menu-close').html('<div class="generalbox-list"> <div class="row"><center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center></div> </div>')
				        },
				        success: function (data) {
							$('#menu-basicinfo').find('.edit-part').addClass('dis-none');
							$('#menu-basicinfo').find('.edit-part').hide();
							$('#menu-basicinfo').find('.edit-part').find('.edit-mode').hide();
							$('#menu-basicinfo').find('.normal-part').removeClass('dis-none');
							$('#menu-basicinfo').find('.normal-part').show().html(data);

							$('html, body').animate({
							    scrollTop: 0
							}, 400);

							$(".main-content.with-lmenu").scrollTop(0);
				        }
				    });


				}
	        }
	    });
	} else {
		$.ajax({
	        url: "?r=account/getbasicinformationnormal",
	        beforeSend: function() {
	        	//$('#menu-close').html('<div class="generalbox-list"> <div class="row"><center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center></div> </div>')
	        },
	        success: function (data) {
				$('#menu-basicinfo').find('.edit-part').addClass('dis-none');
				$('#menu-basicinfo').find('.edit-part').hide();
				$('#menu-basicinfo').find('.edit-part').find('.edit-mode').hide();
				$('#menu-basicinfo').find('.normal-part').removeClass('dis-none');
				$('#menu-basicinfo').find('.normal-part').show().html(data);

				$('html, body').animate({
				    scrollTop: 0
				}, 400);

				$(".main-content.with-lmenu").scrollTop(0);
	        }
	    });
	}
}

/************ START Design Code ************/
	function setCookie(cname, cvalue, exdays) {
	    var d = new Date();
	    d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    var expires = "expires="+d.toUTCString();
	    document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	function showpoinfo()
	{
		$(".green-top").addClass("showon");
		$(".over-hidden").addClass("shocolor");
		$(".click-hide").hide();
	}
	
	function showpoinfo1()
	{
		$(".green-top").addClass("showon");
		$(".over-hidden").addClass("shocolor");
		$(".click-hide").hide();
	}

	function hidepop(){
		$(".green-top").removeClass("showon");
		$(".over-hidden").removeClass("shocolor");
	}


    function checkuseradminstuff() {
        $.ajax({
            url: "?r=site/checkuseradminstuff",
            beforeSend: function() {
            	$('#menu-close').html('<div class="generalbox-list"> <div class="row"><center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center></div> </div>')
            },
            success: function (data) {
                $('#menu-close').html(data);
            }
        });
    }

/* FUN set settings menu */
	
	function setPageMainTab(fromWhere, obj){
		if(fromWhere == "settings"){
			
			var contentClass=$(obj).attr("class");
			
			$(".settings-content").each(function(){			
				if($(this).attr("id")!=contentClass){
					$(this).hide();
					if($(this).hasClass("active")){
						$(this).removeClass("active");
					}
				}
				else{
					if(!$(this).hasClass("active")){
						$(this).addClass("active");
						$(this).show();					
					}
				}	
			});
			$(".settings-menu .has_sub").each(function(){			
				$(this).find(".submenu > li").each(function(){
					$(this).removeClass("active");
				});
				if(!$(this).hasClass("opened") && $(this).hasClass("active")){
					$(this).removeClass("active");
				}
			});
			var getA = $(".settings-menu .has_sub .submenu > li a[class='"+contentClass+"']")
			getA.parents("li").addClass("active");
			resetInnerPage("settings","hide"); // hide settings menu in mobile
		}
		
		setTimeout(function(){
			//setGeneralThings();
			var winw = $(window).width();
			if(winw < 768 ){
			}else{
				$('html, body').animate({
					scrollTop: 0},
				'slow');
			}
		},1000);
	}

	function setSettingsMenu(obj){
		
		var thisParent=$(obj).parent("li");
		if(thisParent.hasClass("opened"))
		{
			thisParent.children(".submenu").slideUp(300);
			thisParent.removeClass("opened");
		}
		else{					
			closeAllExpandedMenu(thisParent);
			thisParent.children(".submenu").slideDown(300);
			thisParent.addClass("opened");
		}
	}
	function closeAllExpandedMenu(e){
			
		var thisParent=e;		
		$("ul#settings-menu li").each(function(){

			var regLi=$(this).parent("li");
		
			if($(this).hasClass("has_sub")){
				
				if($(this).hasClass("opened")){
					
					$(this).children(".submenu").slideUp(300);
					$(this).removeClass("opened");
																					
				}							
			}
		});			
	}
/* FUN end set settings menu */

/* FUN edit/normal mode settings page */
	function close_all_edit(){
	   var count=0;
	   $(".settings-ul .settings-group").each(function(){
			
		   var editmode=$(this).children(".edit-mode").css("display");		
		
		   if(editmode!="none"){
				
				$(this).children(".normal-mode").slideDown(300);
				$(this).children(".edit-mode").slideUp(300);
			}
	   });
	}
	function open_edit(obj, type=null){
		customArray = [];
		customArrayTemp = [];
		addUserForAccountSettingsArray = [];
		addUserForAccountSettingsArrayTemp = [];
		close_all_edit();

		var obj=$(obj).parents(".settings-group");			
		var editmode=obj.children(".edit-mode").css("display");

		if(editmode=="none"){
			obj.children(".normal-mode").slideUp(300);
			obj.children(".edit-mode").slideDown(300);
		} else { 
			obj.children(".normal-mode").slideDown(300);
			obj.children(".edit-mode").slideUp(300);
		}

		initDropdown();

		var typeArray = ['my_view_status', 'connect_request', 'connect_list', 'view_photos_custom', 'my_post_view_status_custom', 'add_public_wall_custom', 'review_posts', 'review_tags', 'add_post_on_your_wall_view_custom'];
		if($.inArray(type, typeArray) !== -1) {
			$selectore = $(obj).find('.edit-mode').find('ul');
			var winw = $(window).width();
			if(winw <= 480) {
				if($selectore.length) {
					$selectore.parents('.left').find('a').attr('onclick', 'privacymodal(this)');
					$selectore.parents('.left').find('a').attr('data-activates', '');
					$selectore.addClass('forcefullyhide');
				}
			} else {
				if($selectore.length) {
					$data_activities = $selectore.attr('id');
					$selectore.parents('.left').find('a').attr('data-activates', $data_activities);
					$selectore.parents('.left').find('a').attr('onclick', '');
					$selectore.removeClass('forcefullyhide');
					$selectore.hide();
					$('.dropdown-button').dropdown();
				}
			}

			$.ajax({  
				url: '?r=site/xhiso',  
				type: 'POST', 
				data: {type},
				success: function(data) {
					var $result = $.parseJSON(data);
					customArray = $result;
					customArrayTemp = $result;
				}
			});
		}
	}
	function close_edit(obj, type=null) {
		if($(obj).parents('li').find('.getvalue').length) {
		    var name = $(obj).data('nm');
		    if(name != undefined && name != '') {
				var lispan = $(obj).parents('li').find('.getvalue').html().trim();
				$('.'+name+'li .edit-mode #'+name).val(lispan);
				setting(type);  
				$('.'+name+'li .normal-mode .row .security-setting.'+name+'display').html(lispan);
		    }
		}
 
		var objParent=$(obj).parents(".settings-group");
		var emode=objParent.children(".edit-mode");
		var nmode=objParent.children(".normal-mode");
		var editmode=emode.css("display");

		if(editmode=="none") {
			nmode.slideUp(300);
			emode.slideDown(300);
		} else {
			nmode.slideDown(300);
			emode.slideUp(300);
		}
	}   
/* FUN end edit/normal mode settings page */

/* theme color settings */
	function theme_color(color){
		if(color != ''){
			var link = 'color='+color;
			$.ajax({
				url: '?r=site/theme-change',  
				type: 'GET',
				data: link,
				async:false,        
				processData: false,
				contentType: false,
				success: function(data) {
				}
			});
		}
	}
/* end theme color settings */


/************ END Design Code ************/
	function getSecurityAnswer(name, placeholder){
		var input = $('.securityquestionli .edit-mode .securityanswerinput');
		var info = '';
		$.ajax({
		   url: '?r=site/get-security-answer',
		   success: function(data){
				info = $.parseJSON(data);
				var data = info[name];
				if(data != '') {
					$(input).val(data);
				} else {
					$(input).val('');
				}
				$(input).attr("placeholder", placeholder);
				
		}
		});
	}

	$(".securityquestionli .edit-mode #securityquestion").on("change", function(){
		var a = $(this).val();
		var input = $('.securityquestionli .edit-mode .securityanswerinput');
		if(a == 'eml_ans'){
			getSecurityAnswer('email', 'Please enter your Email.');
		} else if(a == 'born_ans'){
			getSecurityAnswer('born', 'Please enter your Born place.');
		} else if(a == 'gf_ans'){
			getSecurityAnswer('girl', 'Please enter your Girlconnect name.');
		}
	});

	/* Start Funtion For Adding Security Settings In-To DataBase */
		function setting(type=null) { 
			$.ajax({ 
			   type: 'POST',
			   url: '?r=site/security-setting',
			   data: $("#frm-setting").serialize() + '&custom='+customArrayTemp+'&type='+type,
			   success: function(data){
			   		if(data){ 
				   		Materialize.toast('Saved', 2000, 'green');
				   } else {
				   		Materialize.toast('Oops something went wrong.', 2000, 'red');
				   }
			   }
			});
		}
	/* End Funtion For Adding Security Settings In-To DataBase */
	
	/* Start Funtion For Adding Security Settings In-To DataBase */
		function blocking() {
			$.ajax({
			    type: 'POST',
			    url: '?r=site/blocking',
			    data: {
			   		'label' : whoisopeninblock,
			   		'ids' : addUserForAccountSettingsArrayTemp
			   	},
			    success: function(data) {
			       if(data == '1'){
				   		Materialize.toast('Saved', 2000, 'green');
				   }
			   }
			});
		}
	/* End Funtion For Adding Security Settings In-To DataBase */


		$('.settings-ul li .cmn-toggle-round').click(function() {
			var id = $(this).attr('id');
			if ($(this).is(':checked')){
				$('#'+id+'_switch').val('Yes');
			}
			else{
				$('#'+id+'_switch').val('No');
			}
			notify();
		});

		$(document).on('change', '#communication_label, #is_received_message_tone_on, #is_new_message_display_preview_on, #show_away, #is_send_message_on_enter', function() {
			communication();
		});
		
	/* Start Funtion For Adding Comminication  Settings In-To DataBase */	
		function communication()
		{
			var is_received_message_tone_on = 'off';
			var is_new_message_display_preview_on = 'off';
			var communication_label = '';
			var show_away = 'off';
			var is_send_message_on_enter = 'off';

			if($("#is_received_message_tone_on").prop('checked') == true) {
				is_received_message_tone_on = 'on';
			}
			
			if($("#is_new_message_display_preview_on").prop('checked') == true) {
				is_new_message_display_preview_on = 'on';
			}

			communication_label = $('#communication_label').val();

			if($("#show_away").prop('checked') == true) {
				show_away = 'on';
			}
			
			if($("#is_send_message_on_enter").prop('checked') == true) {
				is_send_message_on_enter = 'on';
			}


			$.ajax({ 
				type: 'POST',
				url: '?r=site/communication-settings',
				data: {
					'is_received_message_tone_on' : is_received_message_tone_on,
					'is_new_message_display_preview_on' : is_new_message_display_preview_on,
					'communication_label' : communication_label,
					'show_away' : show_away,
					'is_send_message_on_enter' : is_send_message_on_enter
				},
				success: function(data){
					if(data == '1'){
						Materialize.toast('Saved', 2000, 'green');
					}
				}
			});			
		}
	/* End Funtion For Adding Comminication  Settings In-To DataBase */
	
		function notify(){
			var connect_activity = $('#connect_activity_switch').val();
			var email_on_account_issues = $('#email_on_account_issues_switch').val();
			var connect_activity_on_user_post = $('#connect_activity_on_user_post_switch').val();
			var non_connect_activity = $('#non_connect_activity_switch').val();
			var connect_request = $('#connect_request_switch').val();
			var e_card = $('#e_card_switch').val();
			var credit_activity = $('#credit_activity_switch').val();
			var sound_on_notification = $('#sound_on_notification_switch').val();
			var sound_on_message = $('#sound_on_message_switch').val();
			var like_post = $('#like_post_switch').val();
			var comment_post = $('#comment_post_switch').val();
			var share_post = $('#share_post_switch').val();
			var follow_collection = $('#follow_collection_switch').val();
			var share_collection = $('#share_collection_switch').val();
			var share_event = $('#share_event_switch').val();
			var add_post_event = $('#add_post_event_switch').val();
			var add_photo_event = $('#add_photo_event_switch').val();
			var attend_event = $('#attend_event_switch').val();
			var invited_for_event = $('#invited_for_event_switch').val();
			var share_group = $('#share_group_switch').val();
			var add_post_group = $('#add_post_group_switch').val();
			var add_photo_group = $('#add_photo_group_switch').val();
			var become_member_of_your_group = $('#become_member_of_your_group_switch').val();
			var invited_for_group = $('#invited_for_group_switch').val();
			var add_trip_by_connect = $('#add_trip_by_connect_switch').val();
			var invited_for_trip = $('#invited_for_trip_switch').val();
			var member_invited_for_your_event = $('#member_invited_for_your_event_switch').val();
			var member_invited_for_your_group = $('#member_invited_for_your_group_switch').val();

			$.ajax({
				   type: 'POST',
				   url: '?r=site/notification-setting',
				   data: "connect_activity="+connect_activity+"&email_on_account_issues="+email_on_account_issues+"&connect_activity_on_user_post="+connect_activity_on_user_post+
										"&non_connect_activity="+non_connect_activity+
										 "&connect_request="+connect_request+"&e_card="+e_card+"&credit_activity="+credit_activity+
										 "&sound_on_notification="+sound_on_notification+"&sound_on_message="+sound_on_message+"&like_post="+like_post+"&comment_post="+comment_post+"&share_post="+share_post+
										 "&follow_collection="+follow_collection+"&share_collection="+share_collection+"&share_event="+share_event+"&add_post_event="+add_post_event+
										 "&add_photo_event="+add_photo_event+"&attend_event="+attend_event+"&invited_for_event="+invited_for_event+"&share_group="+share_group+
										 "&add_post_group="+add_post_group+"&add_photo_group="+add_photo_group+"&become_member_of_your_group="+become_member_of_your_group+
										 "&invited_for_group="+invited_for_group+"&add_trip_by_connect="+add_trip_by_connect+"&invited_for_trip="+invited_for_trip+
										 "&member_invited_for_your_event="+member_invited_for_your_event+"&member_invited_for_your_group="+member_invited_for_your_group,
				   success: function(data){
					  if(data == '1'){
					  		Materialize.toast('Saved', 2000, 'green');
					   }
					}
				});
		}

		function listCookies() {
			var theCookies = document.cookie.split(';');
			var aString = '';
			for (var i = 1 ; i <= theCookies.length; i++) {
				aString += i + ' ' + theCookies[i-1] + "\n";
			}
		}

		function firstToUpperCase(string)
		{
			return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
		}

	/* Start Function For Saving User First and Last Name In-To DataBase */
	function basicinfo()
	{
		var bfname = $("#fname").val();
		var blname = $("#lname").val();
		var fname = firstToUpperCase( bfname );
		var lname = firstToUpperCase( blname );
		$("#fname").val(fname);
		$("#lname").val(lname);
		var reg_nm = /^[a-zA-Z\s]+$/;  

		if(fname == "")
		{
			Materialize.toast('Please enter first name.', 2000, 'red');
			return false;
		}
		else if(fname.length < 2)
		{
			Materialize.toast('Minimum 2 characters allowed.', 2000, 'red');
			return false;
		}
		else if(!reg_nm.test(fname))
		{
			Materialize.toast('Characters only allowed.', 2000, 'red');
			return false;
		}
		else if(lname == "")
		{
			Materialize.toast('Characters only allowed', 2000, 'red');
			return false;
		}
		else if(lname.length < 2)
		{
			Materialize.toast('Minimum 2 characters allowed.', 2000, 'red');
			return false;
		}

		else if(!reg_nm.test(lname))
		{
			Materialize.toast('Characters only allowed', 2000, 'red');
			return false;
		}
		else
		{
			$.ajax({
				   type: 'POST',
				   url: '?r=site/accountsettings',
				   data: $("#frm-name").serialize(),
				   success: function(data){
						var result = $.parseJSON(data);

						var fname = result.substr(0,result.indexOf(' '));
						$("#name").html(result);
						$("#u_id").html(fname);
						Materialize.toast('Saved', 2000, 'green');
				   }
				   
			   });
			}
	}
	/* End Function For Saving User First and Last Name In-To DataBase */
	
		/* Start Function For Saving User Email Id In-To DataBase */
		function email_address()
		{
			var email = $("#bemail").val();
			var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
			if(email =="")
			{
				Materialize.toast('Please Enter Email.', 2000, 'red');
				return false;
			}

			else if(!pattern.test(email))
			{ 
				Materialize.toast('Please Enter valid Email.', 2000, 'red');
				return false;
			}
			else
			{
				var r = confirm("you have to use this email for next time login");
				if (r == false) 
				{
					return false;
				}
					$.ajax({
						   type: 'POST',
						   url: '?r=site/accountsettings',
						   data: $("#frm-email").serialize(),
						   success: function(data){
							  var result = $.parseJSON(data);
							  var email = result[0];
							  if(email == '0'){
							  	 Materialize.toast('oops this email id is already in use.', 2000, 'red');
							  }
							  else if(email == '1'){
							  		Materialize.toast('Saved', 2000, 'green');
								   $.ajax({
										type: 'POST',
										url: '?r=site/user-logout',
										success: function(data){
											if(data == "1") {
												window.location.href='?r=site/index';
											}
										}
									});
							  }
							  else{
								  $("#email").html(result[0]);
								  Materialize.toast('Saved', 2000, 'green');
										$.ajax({
											type: 'POST',
											url: '?r=site/user-logout',
											success: function(data){
												if(data == "1") {
													window.location.href='?r=site/index';
												}
											}
										});
							  }
						   }   
					   });
			}
		}
	/* End Function For Saving User Email Id In-To DataBase */	
	
	/* Start Function For Saving User Alternate Email Id In-To DataBase */
		function email2(){
			var email = $("#alternate_email").val();
			var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
			if(email =="")
			{
				Materialize.toast('Please enter alternate Email.', 2000, 'red');
				return false;
			}

			else if(!pattern.test(email))
			{
				Materialize.toast('Please enter valid alternate Email.', 2000, 'red');
				return false;
			}
			else
			{
			$.ajax({
				   type: 'POST',
				   url: '?r=site/accountsettings',
				   data: $("#frm-alt-email").serialize(),
				   success: function(data){
					  var result = $.parseJSON(data);
					 $("#alt-email").html(result[0]);
					 Materialize.toast('Saved', 2000, 'green');
				   }
				});
			}
		}
	/* End Function For Saving User Alternate Email Id In-To DataBase */

	/* Start Function For Saving User Password In-To DataBase */
	function pwd()
	{
		var old_password = $("#old_password").val();
		var password = $("#password").val();
		var con_password = $("#con_password").val();
  
		if(old_password == "")
		{
			Materialize.toast('Oops please enter old password.', 2000, 'red');
			return false;
		}
		else if(old_password.length < 6)
		{
			Materialize.toast('Oops please enter old password of atleast 6 characters.', 2000, 'red');
			return false;
		}
		else if(password == "")
		{
			Materialize.toast('Oops please enter password.', 2000, 'red');
			return false;
		}
		else if(password.length < 6)
		{
			Materialize.toast('Oops please Enter password of atleast 6 characters.', 2000, 'red');
			return false;
		}
		else if(con_password == "")
		{
			Materialize.toast('Oops please enter confirm password.', 2000, 'red');
			return false;
		}
		else if(con_password.length < 6)
		{
			Materialize.toast('Oops please enter confirm password of atleast 6 characters.', 2000, 'red');
			return false;
		}
		else
		{
		  $.ajax({
			   type: 'POST',
			   url: '?r=site/password',
			   success: function(data){
				   var result = $.parseJSON(data);
					 if(result == old_password){
						 
						if(password != con_password)
						{
							Materialize.toast('Oops please mismatched.', 2000, 'red');
							return false;
						}
						else{
							$.ajax({
								type: 'POST',
								url: '?r=site/accountsettings',
								data: $("#frm-pwd").serialize(),
								success: function(data){
									var result = $.parseJSON(data);
									$("#pwd-change").html(result[0]);
									Materialize.toast('Saved', 2000, 'green');

									if ($("#test1").is(':checked')){
									DoPost();
									}
									else{
									}
								}
							});
						}
					}
			}
		});
	}
	}	
	/* End Function For Saving User Password In-To DataBase */

	/* Start Function For Saving User Phone In-To DataBase */
		function phone()
		{
			var phone = $("#phone").val();
			var isd_code = $("#isd_code").val();
			var ptn = /([0-9\s\-]{1,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;

			if(phone =="")
			{
				Materialize.toast('Please enter mobile number.', 2000, 'red');
				$("#phn-success").hide();
				$("#phn-fail").show();
				$("#phone").focus();
				return false;
				
			}
			else if(!ptn.test(phone))
			{
				Materialize.toast('Please enter valid number.', 2000, 'red');
				$("#phn-success").hide();
				$("#phn-fail").show();
				$("#phone").focus();
				return false;
			}
			else
			{
				$.ajax({
				url: "?r=site/phone",
				data:'phone='+$("#phone").val(),
				type: "POST",
				success:function(data){
					if(data == '1')
					{
						$("#phn-fail").hide();
						$("#phn-success").show();

						$.ajax({
							type: 'POST',
							url: '?r=site/accountsettings',
							data: $("#frm-phone").serialize(),
							success: function(data){
							var result = $.parseJSON(data);
							Materialize.toast('Saved', 2000, 'green');
							$("#phone1").html(isd_code+' '+result[0]);
							}
						});
						return true;
						}
					else
					{
						$("#phn-success").hide();
						$("#phn-fail").show();
						$("#checckavail").attr("disabled", true);
						$("#checckavail").css('pointer-events', 'none');
						Materialize.toast('Already registered', 2000, 'red');
						$("#phn-success").hide();
						$("#phn-fail").show();				
						return false;
					}
					},
				});
			}
		}
	/* End Function For Saving User Phone In-To DataBase */
	
	/* Start Function For Saving User City In-To DataBase */
		function city(){
			var cntry = $("#country").val();
			var contry_code = $("#country_code").val();
			var isd = $("#isd_code").val();
			var phone = $("#phone").val();
			$.ajax({
				type: 'POST',
				url: '?r=site/accountsettings',
				data: $("#frm-city").serialize()+'&isd_code='+isd+'&country='+cntry+'&contry_code='+contry_code,
				success: function(data){
					var result = $.parseJSON(data);                                      
					$("#city").html(result[0]);
					$("#country1").html(result[1]);
					$("#isd_code").html(result[2]);
					$("#phone1").html(result[2]+' '+phone);
					Materialize.toast('Saved', 2000, 'green');
				}	   
			});
		}
	/* End Function For Saving User City In-To DataBase */	
		
	/* Start Function For Saving User Country In-To DataBase */
		function country(){
			$.ajax({
				type: 'POST',
				url: '?r=site/accountsettings',
				data: $("#frm-country").serialize(),
				success: function(data){
				var result = $.parseJSON(data);
				$("#country1").html(result[0]);
				Materialize.toast('Saved', 2000, 'green');
				}
			});
		}
	/* End Function For Saving User Country In-To DataBase */	
	
	/* Start Function For Saving User Gender In-To DataBase */
		function gender(){
			$.ajax({
				type: 'POST',
				url: '?r=site/accountsettings',
				data: $("#frm-gender").serialize(),
				success: function(data){
				var result = $.parseJSON(data);
				$("#gender").html(result[0]);
				Materialize.toast('Saved', 2000, 'green');
				}
			});
		}
	/* End Function For Saving User Gender In-To DataBase */	
	
	/* Start Function For Saving User Birth Date In-To DataBase */
		function birth_date(){
			$.ajax({
				type: 'POST',
				url: '?r=site/accountsettings',
				data: $("#frm-birth-date").serialize(),
				success: function(data){
				var result = $.parseJSON(data);
				$("#birth_date").html(result[0]);
				Materialize.toast('Saved', 2000, 'green');
				}
			});
		}
	/* End Function For Saving User Birth Date In-To DataBase */	
		
		function hometown(){
			$.ajax({
			   type: 'POST',
			   url: '?r=site/accountsettings',
			   data: $("#frm-hometown").serialize(),
			   success: function(data){
				   var result = $.parseJSON(data);
					 $("#hometown").html(result[0]);
					 Materialize.toast('Saved', 2000, 'green');
			   }
			});
		}
	
	/* Start Function For Saving User Language In-To DataBase */
		function language() {
			$language = $('#language1').val();
			$language_access = $('#language_access').val();
			$.ajax({
			    type: 'POST',
			    url: '?r=site/accountsettings',
			    data: {
			    	'language' : $language,
			    	'language_access' : $language_access,
			    	'dummy' : true
			    },
			    success: function(data) {
					var result = $.parseJSON(data);
					if(result[0] != undefined && result[0] == '') {
						$("#language").html('No language set');
					} else {
						$("#language").html(result[0]);
					}
					Materialize.toast('Saved', 2000, 'green');
				}
			});
		}
	/* End Function For Saving User Language In-To DataBase */
	
	/* Start Function For  Saving User About Informaion In-To DataBase */
		function about(){
			$.ajax({
			   type: 'POST',
			   url: '?r=site/accountsettings',
			   data: $("#frm-about").serialize(),
			   success: function(data){  
				var result = $.parseJSON(data);
				$("#about").html(result[0]);
				Materialize.toast('Saved', 2000, 'green');
			   }
			});
		}
	/* End Function For  Saving User About Informaion In-To DataBase */
	
	/* Start Function For  Saving User Education In-To DataBase */
		function education(){
			$education = $('#education1').val();
			$.ajax({
			   type: 'POST',
			   url: '?r=site/accountsettings',
			   data: {
			    	'education' : $education,
			    	'dummy' : true
			    },
			   success: function(data){
					var result = $.parseJSON(data);
			   		if(result[0] != undefined && result[0] == '') {
						$("#education").html('No education set');
					} else {
						$("#education").html(result[0]);
					}
					Materialize.toast('Saved', 2000, 'green');
			   }
			});
		}
	/* End Function For  Saving User Education In-To DataBase */
	
	/* Start Function For  Saving User Interests In-To DataBase */
		function interests(){
			$interests = $('#interests1').val();
			$.ajax({
			   type: 'POST',
			   url: '?r=site/accountsettings',
			   data: {
			    	'interests' : $interests,
			    	'dummy' : true
			    },
			   success: function(data){  
					var result = $.parseJSON(data);
			   		if(result[0] != undefined && result[0] == '') {
						$("#interests").html('No interests set');
					} else {
						$("#interests").html(result[0]);
					}
					Materialize.toast('Saved', 2000, 'green');
			   }
			});
		}
	/* End Function For  Saving User Interests In-To DataBase */
	
	/* Start Function For  Saving User Occupation In-To DataBase */
		function occupation(){
			$occupation = $('#occupations1').val();
			$.ajax({
			   type: 'POST',
			   url: '?r=site/accountsettings',
			   data: {
			   	 'occupation' : $occupation,
			   	 'dummy' : true
			   },
			   success: function(data){
			   	    var result = $.parseJSON(data);
			   		if(result[0] != undefined && result[0] == '') {
						$("#occupation").html('No occupation set');
					} else {
						$("#occupation").html(result[0]);
					}
					Materialize.toast('Saved', 2000, 'green');
			   }
			});
		}
	/* End Function For  Saving User Occupation In-To DataBase */
		
	/* Start Function Of PlaceHolder  For Password Fields */
		function pwd_placeholder()
		{
			document.getElementById("old_password").value = "";
			document.getElementById("password").value = "";
			document.getElementById("con_password").value = "";
			$("#old_password").attr("placeholder", "Type current password");
			$("#password").attr("placeholder", "Type new password");
			$("#con_password").attr("placeholder", "Retype new password");
		}
	/* End Function Of PlaceHolder  For Password Fields */

	/* Start Function  for Redirecting to Account Setting Page */
		function redirect_accountsettings()
		{
			window.location.href = "?r=site/accountsettings";
		}
	/* End Function  for Redirecting to Account Setting Page */
	
	/* Start Funtion For Adding Security Settings In-To DataBase */
		function close_account(isPermission=false) {
			var reason_of_leaving = $('#menu-close').find("input[name='group1']:checked").attr('id');
			var why_close_account = $('#menu-close').find("#reason_input").val().trim();
			var isValidate = true;

			if(reason_of_leaving == undefined || reason_of_leaving == '') {
				isValidate = false;
	        	Materialize.toast('Select reason of leaving.', 2000, 'red');
			} else if(why_close_account == undefined || why_close_account == '') {
				isValidate = false;
	        	$('#menu-close').find("#reason_input").focus();
	        	Materialize.toast('Why account close?.', 2000, 'red');
			}

			if(isValidate) {
				if(isPermission) {
					$.ajax({
					   type: 'POST',
					   url: '?r=site/close-account',
					   data: {reason_of_leaving, why_close_account},
					   success: function(data) {
							Materialize.toast('Account closed.', 2000, 'green');	
							window.location.href='';									   	
					   }
					   
					});
				} else {
					$('.dropdown-button').dropdown("close");
					var disText = $(".discard_md_modal .discard_modal_msg");
				    var btnKeep = $(".discard_md_modal .modal_keep");
				    var btnDiscard = $(".discard_md_modal .modal_discard");
			    	disText.html("Close account.");
		            btnKeep.html("Keep");
		            btnDiscard.html("Close");
		            btnDiscard.attr('onclick', 'close_account(true)');
		            $(".discard_md_modal").modal("open");
				}
			}
		}

function fetchlanguagedropdown() {
	if(!$('#languagedropdown').hasClass('langloaded')) {
		$.ajax({
			type: 'GET', 
			url: '?r=site/fetchlanguagesforaccountsetting',
			success: function(data) {
				var result = $.parseJSON(data);
				var $language = result['$language'];
				var $languagearray = result['$languagearray'];
				
				$.each($languagearray, function(i, v) {
					$('#language1').append($("<option></option>") .attr("value", v) .text(v)); 
				});

				setTimeout(function() { 
					$('#languagedropdown').addClass('langloaded');
					$('select').material_select();  
					$("#language1").val($language).material_select();
					$('.disabled').find('input').remove();
				}, 400);  
			}
		});
	}
}

function fetchinterestsdropdown() {
	if(!$('#interestsdropdown').hasClass('langloaded')) {
		$.ajax({
			type: 'GET',
			url: '?r=site/fetchinterestsforaccountsetting',
			success: function(data) {
				var result = $.parseJSON(data);
				var $interests = result['$interests'];
				var $interestsarray = result['$interestsarray'];
				
				$.each($interestsarray, function(i, v) {
					$('#interests1').append($("<option></option>") .attr("value", v) .text(v)); 
				});

				setTimeout(function() { 
					$('#interestsdropdown').addClass('langloaded');
					$('select').material_select();  
					$("#interests1").val($interests).material_select();
					$('.disabled').find('input').remove();
				}, 400);  
			}
		});
	}
}

function fetcheducationdropdown() {
	if(!$('#educationdropdown').hasClass('langloaded')) {
		$.ajax({
			type: 'GET',
			url: '?r=site/fetcheducationforaccountsetting',
			success: function(data) {
				var result = $.parseJSON(data);
				var $education = result['$education'];
				var $educationarray = result['$educationarray'];
				
				$.each($educationarray, function(i, v) {
					$('#education1').append($("<option></option>") .attr("value", v) .text(v)); 
				});

				setTimeout(function() { 
					$('#educationdropdown').addClass('langloaded');
					$('select').material_select();  
					$("#education1").val($education).material_select();
					$('.disabled').find('input').remove();
				}, 400);  
			}
		});
	}
}

function fetchoccupationdropdown() {
	if(!$('#occupationdropdown').hasClass('langloaded')) {
		$.ajax({
			type: 'GET',
			url: '?r=site/fetchoccupationforaccountsetting',
			success: function(data) {
				var result = $.parseJSON(data);
				var $occupation = result['$occupation'];
				var $occupationarray = result['$occupationarray'];
				
				$.each($occupationarray, function(i, v) {
					$('#occupations1').append($("<option></option>") .attr("value", v) .text(v)); 
				});

				setTimeout(function() { 
					$('#occupationdropdown').addClass('langloaded');
					$('select').material_select();  
					$("#occupations1").val($occupation).material_select();
					$('.disabled').find('input').remove();
				}, 400);  
			}
		});
	}
}
/* End Funtion For Adding Security Settings In-To DataBase */
