	/* document ready */
		$(document).ready(function() {
			/************ ADVERTISE FUNCTIONS ************/
				/* budget-text input text entered */
					$("body").on('keyup', '.budget-text',budgetStringEntered);
					$("body").on('keydown', '.budget-text',budgetStringKeyPressed);
				/* end budget-text input text entered */

			/************ END ADVERTISE FUNCTIONS ************/
		});
	/* end document ready */
	 
	/************ START Design Code **********/

		function adPhotoInputChange(obj, evt, imgSize, adimage){
			
			$getIdInstance = $(obj).attr('id');

			// open pop up before upload image processing...........
			
			var tgt = evt.srcElement || evt.target,
			files = tgt.files;
			
			// FileReader support
			var fr = new FileReader();
			fr.onload = function (e) {	
				//adImgCrop('');

				$('.'+$getIdInstance).attr('src', e.target.result);
				adImgCrop(e.target.result,imgSize,adimage);
				
				if(imgSize=="adImageOne")
					changeAdPhoto('adImageOne'); // 575 x 215
				else if(imgSize=="adImageTwo")
					changeAdPhoto('adImageTwo'); // 575 x 243
				else if(imgSize=="adImageThree")
					changeAdPhoto('adImageThree'); // 575 x 260
				else if(imgSize=="adImageFour")
					changeAdPhoto('adImageFour'); // 575 x 290
				else
					changeAdPhoto('adImageLogo'); // 150 x 150
			}
			fr.readAsDataURL(files[0]);
		}
		
	/* resetting functions */
		function resetAdDetailsDone(){
			var win_w=$(window).width();
			
			$(".detail-box .travadvert-section").each(function(){
				$(this).removeClass("done");
			});
		
		}
		function resetAdDetails(){		
			var win_w=$(window).width();
			
			$(".detail-box .travadvert-section").each(function(){
				$(this).removeClass("active");				
			});
			$('html, body').animate({ scrollTop: 0 }, 'slow');		
		}
		function resetAdLinks(){
			$(".travad-navs > li").each(function(){
				if($(this).hasClass("active"))
					$(this).addClass("done");
				$(this).removeClass("active");
			});
		}
		function resetAdObjective(obj){
			$(".travad-accordion .mainhead").each(function(){			
				$(this).removeClass("active");
			});
			
			$(obj).parents(".mainhead").addClass("active");
		}
		function resetObjectDetails(adObjective,objName){
			
			$(".main-accContent .travad-detailbox").hide();
			$(".travad-accordion.travad-details .sub-title").html(objName);
			$(".main-accContent .travad-detailbox."+adObjective).show();
			
		}	
	/* end resetting functions */

	/* navigation function */
		function navigateAdDetails(obj, fromWhere, adtype){
			if($(obj).hasClass('directcheckuserauthclass')) {
				return false;
			} 
			else if(fromWhere=="topnav")
			{
				var fparent=$(obj).parent("li");
				var isDisabled=fparent.attr("class");
				
				if(isDisabled!="disabled" && isDisabled!="active")
				{
					var dataClass=fparent.attr("data-class");
					var dataDetailid=fparent.attr("data-detailid");
					var stepCount=dataClass.replace("step","");				
					
					var prevStep=getPrevStep();
					//var isValidated=true;
					var isValidated=getAdValidate(prevStep, dataClass, adtype);
					if(isValidated)
					{
						resetAdLinks();
						fparent.addClass("active");

						resetAdDetails();
						$(".detail-box .travadvert-section#"+dataDetailid).addClass("active");
					
					}
				
					if(dataClass=="step2")
					{
						$('.gauge2').gauge({
							values: {
								0 : 'Specific',
								20: 'Good',
								100: 'Broad'
							},
							colors: {
								0 : '#ED1C24',
								20 : '#39B54A',
								80: '#FDD914'
							},
							angles: [
								180,
								360
							],
							lineWidth: 20,
							arrowWidth: 10,
							arrowColor: '#ccc',
							inset:true,

							value: 100
						});
					}
					if(adtype=="createad")
					{
						setVars();
					}
				}
			}
			else
			{
				var fparent=$(obj).parent(".dirinfo");
				var nextDataClass=fparent.attr("data-ndataclass");
				var currStep=fparent.attr("data-class");
				
				if(nextDataClass=="step2")
				{
					var adObjective="pagelikes";
					var objName="Page Engagement";					
					
					adObjective=$(obj).attr("dataobj");
					objName=$(obj).attr("objname");
					
					$("#adobj").val(adObjective);

					resetAdObjective(obj);
					resetObjectDetails(adObjective,objName);					
				}
				
				if(adtype=="createad")
				{
					setVars();
				}
				
				var isValidated=getAdValidate(currStep, nextDataClass, adtype);
				//var isValidated=true;
				
				var linkObjParent = $(".travad-navs > li[data-class='"+nextDataClass+"']");

				if(isValidated)
				{
					linkObjParent.attr("class","done"); // remove Disabled class from next step
					
					var linkObj = linkObjParent.find("a");
					navigateAdDetails(linkObj, "topnav");
				}
			}

			change_audience_guage();
			initDropdown();
			$('.audience-form').find('li.disabled').find('label').remove();
		}
	/* end navigation function */

	/* manage function */
		function getAdValidate(currentStep, nextStep, adtype) {
			var status = $("#user_status").val();
			if(status == 0) { return false; }

			var adname = $("#advert_name").val();

			if(adname == '') {
				Materialize.toast('Enter advert name.', 2000, 'red');
				$("#advert_name").focus();
				return false;
			} else {
				return true;
			}
		}

		function getPrevStep(){
			var step="";
			$(".travad-navs > li").each(function(){
				if($(this).hasClass("active"))
					step=$(this).attr("data-calss");			
			});
			return step;
		}
		function textCounter(obj,uplimit,counter,type) {
			
			var tt = $(obj).val().length;
			if(tt<=uplimit){
				var charLeft=uplimit-tt;
				$(counter).html(charLeft);
				var adtype = $("#"+type).val();
				adtype = adtype.charAt(0).toUpperCase() + adtype.slice(1);
				$("."+type).html(adtype);
				$("#"+type).val(adtype);
			}
		}
	/* end manage function */

	/* upload image / crop popup function */
		function resetAdPhotoSection(){
			$(".popup-area#change-adphoto .pc-section").hide();
			
		}
		function changeAdPhoto(which){
			resetAdPhotoSection();
			$(".popup-area#change-adphoto .pc-section."+which).show();			
			$(".popup-area#change-adphoto").removeClass("adlogo-popup");
			if(which=="adImageLogo"){
				$(".popup-area#change-adphoto").addClass("adlogo-popup");
			}
		}
		function adImgCrop($imgPath,adImgSize,adimage){
			if($imgPath == '') {
				if(adImgSize=="adImageOne")			
					$imgPath = $assetsPath+'/images/ad-sizeone.jpg';
				else if(adImgSize=="adImageTwo")
					$imgPath = $assetsPath+'/images/ad-sizetwo.jpg';
				else if(adImgSize=="adImageThree")
					$imgPath = $assetsPath+'/images/ad-sizethree.jpg';
				else if(adImgSize=="adImageFour")
					$imgPath = $assetsPath+'/images/ad-sizefour.jpg';
				else
					$imgPath = $assetsPath+'/images/ad-sizelogo.jpg';
		    } 
			
		    /*var adPhotoCropOptions = {
				cropUrl:'?r=ads/adsimagecrop',
				loadPicture:$imgPath,
				enableMousescroll:true,
				
				onAfterImgCrop:function(response){
						$('.'+adimage).attr('src',response.url);
				}
			}

			if(adImgSize=="adImageOne")
				var adPhotoCrop = new Croppic('adImageOneCrop', adPhotoCropOptions);
			else if(adImgSize=="adImageTwo")
				var adPhotoCrop = new Croppic('adImageTwoCrop', adPhotoCropOptions);
			else if(adImgSize=="adImageThree")
				var adPhotoCrop = new Croppic('adImageThreeCrop', adPhotoCropOptions);
			else if(adImgSize=="adImageFour")
				var adPhotoCrop = new Croppic('adImageFourCrop', adPhotoCropOptions);
			else
				var adPhotoCrop = new Croppic('adImageLogoCrop', adPhotoCropOptions);*/
	   }
	/* end upload image / crop popup function */

	/* manage ad details */
		function backToAds(obj){
			var sparent = $(obj).parents(".manageadvert-details");
			sparent.find(".sub-content").hide();		
		}
		function openManageAdDetails(obj){
			var sparent = $(obj).parents(".manageadvert-details");
			
			// @dev : replace the content in sub-content section 
			var adid = $(obj).attr("data-adid");
			viewaddetails(adid);
			
			sparent.find(".sub-content").show();
			
			$('html,body').animate({
				scrollTop: $(".sub-content").offset().top - 78},
	        'slow');
		}
		function openEditAd(adid){ 
			$(".manageadvert-page").find(".edit-travad").show();
			editAd(adid); 
		}

		function closeEditAd(){
			$(".manageadvert-page").find(".edit-travad").hide();
		}
	/* end manage ad details */

	/* FUN budget-text string entered */
		function budgetStringEntered(e){		
			var thisValue=$(this).val().trim();
			if(thisValue!= ""){
				if(e.keyCode == 13){
					var newVal=parseFloat(thisValue).toFixed(2);			
					$(this).val(newVal);
				}
			}		
		}
		function budgetStringKeyPressed(e){
			if(!(e.keyCode==8 || e.keyCode==13 || (e.keyCode >= 48 && e.keyCode <= 57))) {
				e.preventDefault();
			}		
		}
	/* FUN end budget-text string entered
	 */ 
	/* insert advertise */
		function setVars(){
			var adObjective = $("#adobj").val();
			var adname = $("#advert_name").val();
			var adaction = $("#adaction").val();
			var advrt_id = $("#advrt_id").val();
			var urlvalidate = /^(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
			
			var formdata;
			formdata = new FormData();
			formdata.append("adobj", adObjective);
			formdata.append("adname", adname);
			formdata.append("adaction", adaction);
			formdata.append("advrt_id", advrt_id);
			var websitepattern = /^(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
			if(adObjective=='pagelikes')
			{
				var pagelikenames = $("#pagelikenames").val();
				formdata.append("pagelikenames", pagelikenames);
				var pagelikescatch = $("#pagelikescatch").val();
				if(pagelikescatch == '')
				{
					Materialize.toast('Enter catch phrase.', 2000, 'red');
					$("#pagelikescatch").focus();
					return false;
				}
				else
				{
					formdata.append("pagelikescatch", pagelikescatch);
				}
				var pagelikesheader = $("#pagelikesheader").val();
				if(pagelikesheader == '')
				{
					Materialize.toast('Enter headline.', 2000, 'red');
					$("#pagelikesheader").focus();
					return false;
				}
				var pagelikestext = $("#pagelikestext").val();
				if(pagelikestext == '')
				{
					Materialize.toast('Enter text.', 2000, 'red');
					$("#pagelikestext").focus();
					return false;
				}
				else
				{
					formdata.append("pagelikesheader", pagelikesheader);
				}
				
				formdata.append("pagelikestext", pagelikestext);
				var pagelikes = $("#pagelikesimagedp").attr('src');
				formdata.append("pagelikes", pagelikes);
			}
			else if(adObjective=='brandawareness')
			{
				var brandawarenesscatch = $("#brandawarenesscatch").val();
				if(brandawarenesscatch == '')
				{
					Materialize.toast('Enter branding phrase.', 2000, 'red');
					$("#brandawarenesscatch").focus();
					return false;
				}
				else
				{
					formdata.append("brandawarenesscatch", brandawarenesscatch);
				}
				var brandawarenesstext = $("#brandawarenesstext").val();
				if(brandawarenesstext == '')
				{
					Materialize.toast('Enter text.', 2000, 'red');
					$("#brandawarenesstext").focus();
					return false;
				}
				formdata.append("brandawarenesstext", brandawarenesstext);
				var brandawarenesssite = $("#brandawarenesssite").val();
				if(brandawarenesssite == '' || !websitepattern.test(brandawarenesssite))
				{
					Materialize.toast('Enter valid website.', 2000, 'red');
					$("#brandawarenesssite").focus();
					return false;
				}
				else
				{
					formdata.append("brandawarenesssite", brandawarenesssite);
				}
				var brandawareness = $("#brandawarenessimagedp").attr('src');
				formdata.append("brandawareness", brandawareness);
			}
			else if(adObjective=='websiteleads')
			{
				var websiteleadstitle = $("#websiteleadstitle").val();
				if(websiteleadstitle == '')
				{
					Materialize.toast('Enter ad title.', 2000, 'red');
					$("#websiteleadstitle").focus();
					return false;
				}
				else
				{
					formdata.append("websiteleadstitle", websiteleadstitle);
				}
				var websiteleadslogo = $(".websiteleadslogo").attr('src');
				formdata.append("websiteleadslogo", websiteleadslogo);
				var websiteleadscatch = $("#websiteleadscatch").val();
				if(websiteleadscatch == '')
				{
					Materialize.toast('Enter catch phrase.', 2000, 'red');
					$("#websiteleadscatch").focus();
					return false;
				}
				else
				{
					formdata.append("websiteleadscatch", websiteleadscatch);
				}
				var websiteleadsheader = $("#websiteleadsheader").val();
				if(websiteleadsheader == '')
				{
					Materialize.toast('Enter headline.', 2000, 'red');
					$("#websiteleadsheader").focus();
					return false;
				}
				else
				{
					formdata.append("websiteleadsheader", websiteleadsheader);
				}
				var websiteleadstext = $("#websiteleadstext").val();
				if(websiteleadstext == '')
				{
					Materialize.toast('Enter text.', 2000, 'red');
					$("#websiteleadstext").focus();
					return false;
				}
				formdata.append("websiteleadstext", websiteleadstext);
				var websiteleadssite = $("#websiteleadssite").val();
				if(websiteleadssite == '' || !websitepattern.test(websiteleadssite))
				{
					Materialize.toast('Enter valid website.', 2000, 'red');
					$("#email").focus();
					return false;
				}
				else
				{
					formdata.append("websiteleadssite", websiteleadssite);
				}
				var websiteleads = $("#websiteleadsimagedp").attr('src');
				formdata.append("websiteleads", websiteleads);
			}
			else if(adObjective=='websiteconversion')
			{
				var websiteconversiontitle = $("#websiteconversiontitle").val();
				if(websiteconversiontitle == '')
				{
					Materialize.toast('Enter ad title.', 2000, 'red');
					$("#websiteconversiontitle").focus();
					return false;
				}
				else
				{
					formdata.append("websiteconversiontitle", websiteconversiontitle);
				}
				var websiteconversionlogo = $(".websiteconversionlogo").attr('src');
				formdata.append("websiteconversionlogo", websiteconversionlogo);
				var websiteconversioncatch = $("#websiteconversioncatch").val();
				if(websiteconversioncatch == '')
				{
					Materialize.toast('Enter catch phrase.', 2000, 'red');
					$("#websiteconversioncatch").focus();
					return false;
				}
				else
				{
					formdata.append("websiteconversioncatch", websiteconversioncatch);
				}
				var websiteconversionheader = $("#websiteconversionheader").val();
				if(websiteconversionheader == '')
				{
					Materialize.toast('Enter headline.', 2000, 'red');
					$("#websiteconversionheader").focus();
					return false;
				}
				else
				{
					formdata.append("websiteconversionheader", websiteconversionheader);
				}
				var websiteconversiontext = $("#websiteconversiontext").val();
				if(websiteconversiontext == '')
				{
					Materialize.toast('Enter text.', 2000, 'red');
					$("#websiteconversiontext").focus();
					return false;
				}
				formdata.append("websiteconversiontext", websiteconversiontext);
				var websiteconversiontype = $("#websiteconversiontype").val();
				formdata.append("websiteconversiontype", websiteconversiontype);
				var websiteconversionsite = $("#websiteconversionsite").val();
				if(websiteconversionsite == '' || !websitepattern.test(websiteconversionsite))
				{
					Materialize.toast('Enter valid website.', 2000, 'red');
					$("#websiteconversionsite").focus();
					return false;
				}
				else
				{
					formdata.append("websiteconversionsite", websiteconversionsite);
				}
				var websiteconversion = $("#websiteconversionimagedp").attr('src');
				formdata.append("websiteconversion", websiteconversion);
			}
			else if(adObjective=='inboxhighlight')
			{
				var inboxhighlighttitle = $("#inboxhighlighttitle").val();
				if(inboxhighlighttitle == '')
				{
					Materialize.toast('Enter ad title.', 2000, 'red');
					$("#inboxhighlighttitle").focus();
					return false;
				}
				else
				{
					formdata.append("inboxhighlighttitle", inboxhighlighttitle);
				}
				var inboxhighlightlogo = $(".inboxhighlightlogo").attr('src');
				formdata.append("inboxhighlightlogo", inboxhighlightlogo);
				var inboxhighlightcatch = $("#inboxhighlightcatch").val();
				if(inboxhighlightcatch == '')
				{
					Materialize.toast('Enter catch phrase.', 2000, 'red');
					$("#inboxhighlightcatch").focus();
					return false;
				}
				else
				{
					formdata.append("inboxhighlightcatch", inboxhighlightcatch);
				}
				var inboxhighlightsubcatch = $("#inboxhighlightsubcatch").val();
				if(inboxhighlightsubcatch == '')
				{
					Materialize.toast('Enter sub catch phrase.', 2000, 'red');
					$("#inboxhighlightsubcatch").focus();
					return false;
				}
				else
				{
					formdata.append("inboxhighlightsubcatch", inboxhighlightsubcatch);
				}
				var inboxhighlightheader = $("#inboxhighlightheader").val();
				if(inboxhighlightheader == '')
				{
					Materialize.toast('Enter headline.', 2000, 'red');
					$("#inboxhighlightheader").focus();
					return false;
				}
				else
				{
					formdata.append("inboxhighlightheader", inboxhighlightheader);
				}
				var inboxhighlighttext = $("#inboxhighlighttext").val();
				if(inboxhighlighttext == '')
				{
					Materialize.toast('Enter text.', 2000, 'red');
					$("#inboxhighlighttext").focus();
					return false;
				}
				formdata.append("inboxhighlighttext", inboxhighlighttext);
				var inboxhighlighttype = $("#inboxhighlighttype").val();
				formdata.append("inboxhighlighttype", inboxhighlighttype);
				var inboxhighlightsite = $("#inboxhighlightsite").val();
				if(inboxhighlightsite == '' || !websitepattern.test(inboxhighlightsite))
				{
					Materialize.toast('Enter valid website.', 2000, 'red');
					$("#inboxhighlightsite").focus();
					return false;
				}
				else
				{
					formdata.append("inboxhighlightsite", inboxhighlightsite);
				}
				var inboxhighlightimage = $("#inboxhighlightimageimagedp").attr('src');
				formdata.append("inboxhighlightimage", inboxhighlightimage);
			}
			else if(adObjective=='pageendorse')
			{
				var pagelikenames = $("#pagelikenames").val();
				formdata.append("pagelikenames", pagelikenames);
				var pageendorsecatch = $("#pageendorsecatch").val();
				if(pageendorsecatch == '')
				{
					Materialize.toast('Enter catch phrase.', 2000, 'red');
					$("#pageendorsecatch").focus();
					return false;
				}
				else
				{
					formdata.append("pagelikescatch", pageendorsecatch);
				}
				var pageendorseheader = $("#pageendorseheader").val();
				if(pageendorseheader == '')
				{
					Materialize.toast('Enter headline.', 2000, 'red');
					$("#pageendorseheader").focus();
					return false;
				}
				else
				{
					formdata.append("pagelikesheader", pageendorseheader);
				}
				var pagelikestext = $("#pagelikestext").val();
				formdata.append("pagelikestext", pagelikestext);
				var pageendorseimage = $("#pageendorseimagedp").attr('src');
				formdata.append("pageendorseimage", pageendorseimage);
			}
			else if(adObjective=='travstorefocus')
			{
				var travstorefocusitem = $("#travstorefocusitem").val();
				formdata.append("travstorefocusitem", travstorefocusitem);
				var travstorefocustype = $("#travstorefocustype").val();
				formdata.append("travstorefocustype", travstorefocustype);
				var travstorefocussite = $("#travstorefocussite").val();
				formdata.append("travstorefocussite", travstorefocussite);
			}
			else if(adObjective=='eventpromo')
			{
				var adevents = $("#adevents").val();
				formdata.append("adevents", adevents);
				var eventpromocatch = $("#eventpromocatch").val();
				if(eventpromocatch == '')
				{
					Materialize.toast('Enter catch phrase.', 2000, 'red');
					$("#eventpromocatch").focus();
					return false;
				}
				else
				{
					formdata.append("eventpromocatch", eventpromocatch);
				}
				var eventpromoheader = $("#eventpromoheader").val();
				if(eventpromoheader == '')
				{
					Materialize.toast('Enter headline.', 2000, 'red');
					$("#eventpromoheader").focus();
					return false;
				}
				else
				{
					formdata.append("eventpromoheader", eventpromoheader);
				}
				var eventpromotext = $("#eventpromotext").val();
				if(eventpromotext == '')
				{
					Materialize.toast('Enter text.', 2000, 'red');
					$("#eventpromotext").focus();
					return false;
				}
				formdata.append("eventpromotext", eventpromotext);
				var eventpromo = $("#eventpromoimagedp").attr('src');
				formdata.append("eventpromo", eventpromo);
			}

			var ads_loc = $("#ads_loc").val();
			formdata.append("ads_loc", ads_loc);
			var minage = $('#test-slider').find('.noUi-handle-lower').find('.noUi-tooltip').find('span').text();
			//var minage = $("#minage").html();
			if(minage<0 || minage > 100)
	        {
	            minage  = 0;
	        }
			formdata.append("minage", minage);
			
			var maxage = $('#test-slider').find('.noUi-handle-upper').find('.noUi-tooltip').find('span').text();
			//var maxage = $("#maxage").html();
			if(maxage<0 || maxage >100)
	        {
	            maxage  = 100;
	        }
			formdata.append("maxage", maxage);
			var ads_lang = $("#ads_lang").val();
			formdata.append("ads_lang", ads_lang);
			if(document.getElementById('Male').checked)
	        {
	            formdata.append("male", 'male');
	        }
	        if(document.getElementById('Female').checked)
	        {
	            formdata.append("female", 'female');
	        }
			var ads_pro = $("#ads_pro").val();
			formdata.append("ads_pro", ads_pro);
			var ads_int = $("#ads_int").val();
			formdata.append("ads_int", ads_int);
			var min_budget = $("#min_budget").val();
			if(min_budget < 1)
			{
				Materialize.toast('Enter at least 1 USD dollar.', 2000, 'red');
				$("#min_budget").focus();
				return false;
			}
			else
			{
				formdata.append("min_budget", min_budget);
			}
			if($('#daily').is(':checked'))
			{
				formdata.append("runat", 'daily');
				var startdate = $("#today_date").val();
				var enddate = $("#daily_date").val();
				if(enddate == '')
				{
					Materialize.toast('Select ad end date.', 2000, 'red');
					$("#daily_date").focus();
					return false;
				}
				formdata.append("startdate", startdate);
				formdata.append("enddate", enddate);
			}
			else
			{
				var startdate = $("#startdate").val();
				var enddate = $("#enddate").val();
				if(startdate == '')
				{
					Materialize.toast('Select ad start date.', 2000, 'red');
					$("#startdate").focus();
					return false;
				}
				if(enddate == '')
				{
					Materialize.toast('Select ad end date.', 2000, 'red');
					$("#enddate").focus();
					return false;
				}
				formdata.append("runat", 'manual');
				formdata.append("startdate", startdate);
				formdata.append("enddate", enddate);
			}
 
			$.ajax({
				url: '?r=ads/newad', 
				type: 'POST',
				data: formdata,
				processData: false,
				contentType: false,
				success: function (data)
				{
					var result = $.parseJSON(data);
					if(result['msg'] == 'success')
					{
						if(pagename == 'edit')
						{
							closeEditAd();
						}
						else 
						{
							$.ajax({
					            type: 'POST',
					            url: "?r=site/payment", 
					            data: {'code': 'ADSUBI003322'},
					            success: function (data) {
					            	$('#payment_popup').html(data);
					            	setTimeout(function(){ 
					            		$('#payment_popup').modal('open');
					            		ad_summary();
					                    initDropdown();
										$('.tabs').tabs(); 
										setModelMaxHeight();
					            	},400);
					        
					                var user_money = '';
									$.ajax({ 
										url: '?r=ads/total-money', 
										success: function (data) {
											user_money = data;	
											var amount = result['total_amount'];
											var days = result['days'];
											var end_date = result['end_date'];
											var old_total_budget = result['old_total_budget'];
											var benifit_amount = result['benifit_amount'];
											var ad_cost = result['ad_cost'];
											var calFig = amount - user_money;

											if(user_money > amount) {
												var final_amount = 0;
												var getDiffAmount = user_money - amount;
											} else {
												var final_amount = amount - user_money;
												var getDiffAmount = 0;
											}

											$('#pay1').css('display', 'none');
											$('#pay2').css('display', 'none');
											$('#cardSubmitBtn').css('display', 'none');
											$('#manualpublish').css('display', 'none');
											$('#total_credits_id').hide();
											$('#left_credits_id').hide();
											$('#old_budget').hide();
										  
											$("#ads_amount").val(final_amount);
											$("#ads_amount2").val(final_amount);
											$("#total_budget").html('$'+final_amount);
											$("#dif_amount").val(getDiffAmount);
											$("#total_credits").html('$'+user_money);
											$("#left_credits").html('$'+getDiffAmount);
											$("#old_total_budget").html('$'+old_total_budget);
											$("#benifit_amount").val(benifit_amount);
											$("#ad_cost").html('$'+ad_cost);
											if(old_total_budget > 0) {
												$('#old_budget').show();
											}	
											if(user_money > 0) {
												$('#total_credits_id').show();
												
											}
											if(ad_cost < old_total_budget) {
											  $('#total_credits_id').hide();
											}
											$("#ad_duration").html('<p>'+days+' Day</p><span class="adend">&nbsp;Campaigns will end at midnight ( UTC Timezone ) on <span class="b">'+end_date+'</span></span>');	
											if(final_amount > 0) {
												$('#pay1').css('display', 'inline-block');
												$('#cardSubmitBtn').css('display', 'inline-block');
											} else {
												$('#pay2').css('display', 'inline-block');
												$('#manualpublish').css('display', 'inline-block');
											}
																	
										}
									}); 
					            }
					        });
							return true;
						}
					}
					else
					{
						return false;
					}
				}
			});
		}
	/* end insert advertise */

	/* get page info */
		function pageSelect(type,count,pageimage,pagedesc,atype){
			var pageid = $("#"+type).val();
			$.ajax({
				type: 'POST',
				url: '?r=page/getpageinfo', 
				data: 'pageid='+pageid+'&pageobj='+atype,
				success: function (data)
				{
					var result = $.parseJSON(data);
					if(result['msg'] == 'success')
					{
						$("."+type).html(result['name']);
						$("."+count).html(result['count']);
						$("#"+pagedesc).html(result['desc']);
						$("."+pagedesc).html(result['desc']);
						$('.'+pageimage).attr('src',result['img']);
					}
				}
			});
		}
	/* end get page info */

	/* get travel info */
		function travitemSelect(itemid,price,travimage,address,desc){
			var id = $("#"+itemid).val();
			$.ajax({
				type: 'POST',
				url: '?r=ads/gettravitem', 
				data: 'id='+id,
				success: function (data)
				{
					var result = $.parseJSON(data);
					if(result['msg'] == 'success')
					{
						$("."+itemid).html(result['name']);
						$("."+price).html(result['price']);
						$('.'+travimage).attr('src',result['img']);
						$("."+address).html(result['address']);
						$("."+desc).html(result['desc']);
					}
				}
			});
		}
	/* end get travel info */

	/* get event info */
		function eventSelect(itemid,month,date,desc,evimage,eva) {
			var id = $("#"+itemid).val();
			$.ajax({
				type: 'POST',
				url: '?r=ads/geteventdetails', 
				data: 'id='+id,
				success: function (data)
				{
					var result = $.parseJSON(data);
					if(result['msg'] == 'success')
					{
						$("."+itemid).html(result['name']);
						$('.'+evimage).attr('src',result['img']);
						$("."+month).html(result['month']);
						$("."+date).html(result['date']);
						$("."+desc).html(result['desc']);
						$("#"+desc).html(result['desc']);
						$("."+eva).html(result['count']);
					}
				}
			});
		}
	/* end get event info */

	/* ad header info */
		function adHeader(header){
			var adheader = $("#"+header).val();
			adheader = adheader.charAt(0).toUpperCase() + adheader.slice(1);
			$("."+header).html(adheader);
			$("#"+header).val(adheader);
	    }
	/* end ad header info */
	/************ END Design Code **********/