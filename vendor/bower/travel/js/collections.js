$(document).ready(function() {
    justifiedGalleryinitialize();
    $('.collection-card-body').find('img').css('opacity', '1');
    $('.collectiondetail-page').find('#placebox').removeClass('dis-none');
    lightGalleryinitialize();
});

$(document).ready(function(){ 
    $w = $(window).width();
    if ( $w > 739) {      
        $(".places-tabs .sub-tabs li a").click(function(){
           $("body").removeClass("remove_scroller");
        }); 
        $(".tabs.icon-menu.tabsnew li a").click(function(){
           $("body").removeClass("remove_scroller");
        }); 
        $(".mbl-tabnav").click(function(){
           $("body").removeClass("remove_scroller");
        }); 
        $(".clicable.viewall-link").click(function(){
           $("body").removeClass("remove_scroller");
        }); 
    } else {
        $(".places-tabs .sub-tabs li a").click(function(){
           $("body").addClass("remove_scroller");
        }); 
        $(".clicable.viewall-link").click(function(){
           $("body").addClass("remove_scroller");
        });         
        $(".tabs.icon-menu.tabsnew li a").click(function(){
           $("body").addClass("remove_scroller");
        }); 
        $(".mbl-tabnav").click(function(){
           $("body").removeClass("remove_scroller");
        });
    }

    $(".header-icon-tabs .tabsnew .tab a").click(function(){
        $(".bottom_tabs").hide();
    });

    $(".places-tabs .tab a").click(function(){
        $(".top_tabs").hide();
    });

    // footer work for places home page only
    $('.footer-section').css('left', '0');
    $w = $(window).width();
    if($w <= 768) {
        $('.main-footer').css({
           'width': '100%',
           'left': '0'
        });
    } else {
        var $_I = $('.places-content.places-all').width();
        var $__I = $('.places-content.places-all').find('.container').width();

        var $half = parseInt($_I) - parseInt($__I);
        $half = parseInt($half) / 2;

        $('.main-footer').css({
           'width': $_I+'px',
           'left': '-'+$half+'px'
        });
    }
});

$(window).resize(function() {
    if($('#places-all').hasClass('active')) {
        $('.footer-section').css('left', '0');
        $w = $(window).width();
        if($w <= 768) {
           $('.main-footer').css({
              'width': '100%',
              'left': '0'
           });
        } else {
           var $_I = $('.places-content.places-all').width();
           var $__I = $('.places-content.places-all').find('.container').width();

           var $half = parseInt($_I) - parseInt($__I);
           $half = parseInt($half) / 2;

           $('.main-footer').css({
              'width': $_I+'px',
              'left': '-'+$half+'px'
           });
        }
    }
});

$(document).on('click', '.tablist .tab a', function(e) {
    $href = $(this).attr('href');
    $href = $href.replace('#', '');
    $('.places-content').removeClass().addClass('places-content '+$href);
    $this = $(this);
});

$(document).on('click', '.upload-gallery_cols', function (obj) {
    if($(this).hasClass('checkuserauthclassnv')) {
        checkuserauthclassnv();
    } else if($(this).hasClass('checkuserauthclassg')) {
        checkuserauthclassg();
    } else {
        $.ajax({ 
            url: '?r=collections/fetchlayereduploadphotohtml',  
            success: function(data) {
                photoUpload = [];
                customArray = [];
                customArrayTemp = [];
                $('#upload-gallery-popup').html(data);    
                setTimeout(function() { 
                    $('#upload-gallery-popup').modal('open');
                    $('.chips').material_chip({
                        placeholder: 'Enter photo categories.'
                    });  
                }, 400);
            }
        });
    }
});


$(document).on('click', '.edit-gallery_cols', function (e) {
    var $editid = $(this).attr('data-editid');
    $.ajax({
        type: 'POST', 
        data:{$editid}, 
        url: '?r=collections/fetcheditlayereduploadphotohtml',  
        success: function(data) {
            photoUpload = [];
            customArray = [];
            customArrayTemp = [];
            
            $('#edit-collection-popup').html(data);    
            setTimeout(function() { 
                //lightGallerydestroy();

                $('#edit-collection-popup').modal('open');
                $('.chips').material_chip({
                    placeholder: 'Enter photo categories.'
                });  
                
                $('#edit-collection-popup').css('z-index', 2000);
                $('#compose_discard_modal').css('z-index', 2050);

                fetchgallerycategoriestaggeduser($editid);
            }, 400);
        }
    }); 
});

$(document).on('click', '.edit-gallery_cols_s', function (e) {
    if($(this).hasClass('checkuserauthclassnv')) {
        checkuserauthclassnv();
    } else if($(this).hasClass('checkuserauthclassg')) {
        checkuserauthclassg();
    } else {
        var $editid = $(this).attr('data-editid');
        $.ajax({
            type: 'POST', 
            data:{$editid}, 
            url: '?r=collections/fetcheditlayereduploadphotohtml_s',  
            success: function(data) {
                photoUpload = [];
                customArray = [];
                customArrayTemp = [];
                
                $('#edit-collection-popup').html(data);    
                setTimeout(function() { 
                    //lightGallerydestroy();

                    $('#edit-collection-popup').modal('open');
                    $('.chips').material_chip({
                        placeholder: 'Enter photo categories.'
                    });  
                    
                    $('#edit-collection-popup').css('z-index', 2000);
                    $('#compose_discard_modal').css('z-index', 2050);

                    fetchgallerycategoriestaggeduser($editid);
                }, 400);
            }
        }); 
    }
});

function temp_ps() {
    applypostloader('SHOW');
    $uploadpopupJIDSphototitle = $('.upload-popupJIDS-phototitle').val();
    $uploadpopupJIDSdescription = $('.upload-popupJIDS-description').val();
    $uploadpopupJIDSlocation = $('.upload-popupJIDS-location').val();
    $uploadpopupJIDSvisibleto = $('.upload-popupJIDS-visibleto').html().trim(); 
    $uploadpopupJIDStaggedconnections = userwall_tagged_users;

    if($uploadpopupJIDSphototitle == '') {
        Materialize.toast('Add title', 2000, 'red');
        $('.upload-popupJIDS-phototitle').focus();
        applypostloader('HIDE');
        return false;
    }

    if (storedFiles.length < 3) {
        Materialize.toast('Please upload three cover photos.', 2000, 'red');
        applypostloader('HIDE');
        return false;
    }

    var formdata;
    formdata = new FormData();
    for(var i=0, len=storedFiles.length; i<len; i++) {
        formdata.append('images[]', storedFiles[i]);
    }
    formdata.append('title', $uploadpopupJIDSphototitle);
    formdata.append('description', $uploadpopupJIDSdescription);
    formdata.append('location', $uploadpopupJIDSlocation);
    formdata.append('visibleto', $uploadpopupJIDSvisibleto);
    formdata.append('tagged', $uploadpopupJIDStaggedconnections);
    formdata.append('custom', customArray);

    $.ajax({ 
        type: 'POST', 
        url: "?r=collections/addcollections", 
        data: formdata,
        async: false,
        processData: false,
        contentType: false,
        success: function (data) {
            //applypostloader('HIDE');
            var result = $.parseJSON(data);
            if(result.success != undefined && result.success == true) {
                storedFiles = [];
                storedFilesExsting = [];
                userwall_tagged_users = [];
                userwall_tagged_usersTemp = [];
                customArrayTemp = [];
                customArray = [];
                photoUpload = [];
                $('#upload-gallery-popup').modal('close');
                Materialize.toast('Saved', 2000, 'green');
                window.location.href="";
            }
        }
    });
}

function tempedit_ps(obj) {
    $editid = $(obj).attr('data-editid');
    if($editid) {
        applypostloader('SHOW');
        $uploadpopupJIDSphototitle = $('.upload-popupJIDS-phototitleedit').val();
        $uploadpopupJIDSdescription = $('.upload-popupJIDS-descriptionedit').val();
        $uploadpopupJIDSlocation = $('.upload-popupJIDS-locationedit').val();
        $uploadpopupJIDSvisibleto = $('.upload-popupJIDS-visibletoedit').html().trim(); 
        $uploadpopupJIDStaggedconnections = userwall_tagged_users;

        // check validation
        if($uploadpopupJIDSphototitle == '') {
            Materialize.toast('Add title', 2000, 'red');
            $('.upload-popupJIDS-phototitleedit').focus();
            applypostloader('HIDE');
            return false;
        }

         if ($('#edit-collection-popup').find('.custom-file').length) {
            validate = false;
            Materialize.toast('Please upload three cover photos.', 2000, 'red');
            applypostloader('HIDE');
            return false;
        } else if ($('#edit-collection-popup').find('.post-photos').find('.img-row').find('.img-box').length != 3) {
            validate = false;
            Materialize.toast('Please upload three cover photos.', 2000, 'red');
            applypostloader('HIDE');
            return false;
        }

        var formdata;
        formdata = new FormData();
        for(var i=0, len=storedFiles.length; i<len; i++) {
            formdata.append('images[]', storedFiles[i]);
        }
        formdata.append('title', $uploadpopupJIDSphototitle);
        formdata.append('description', $uploadpopupJIDSdescription);
        formdata.append('location', $uploadpopupJIDSlocation);
        formdata.append('visibleto', $uploadpopupJIDSvisibleto);
        formdata.append('tagged', $uploadpopupJIDStaggedconnections);
        formdata.append('custom', customArray);
        formdata.append('id', $editid);

        $.ajax({
            type: 'POST', 
            url: "?r=collections/editcollections", 
            data: formdata, 
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                //applypostloader('HIDE');
                var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    storedFiles = [];
                    storedFilesExsting = [];
                    userwall_tagged_users = [];
                    userwall_tagged_usersTemp = [];
                    customArrayTemp = [];
                    customArray = [];
                    photoUpload = [];
     
                    $('#edit-collection-popup').modal('close');
                    Materialize.toast('Saved', 2000, 'green');
                    window.location.href="";
                } else {
                    if(result.message != undefined && result.message != '') {
                        Materialize.toast(result.message, 2000, 'red');
                    }
                }
            }
        });     
    }
}

function uploadtempedit_ps(obj) {
    $editid = $(obj).attr('data-editid');
    if($editid) {
        applypostloader('SHOW');
        $uploadpopupJIDSphototitle = $('.upload-popupJIDS-phototitleedit').val();
        $uploadpopupJIDSdescription = $('.upload-popupJIDS-descriptionedit').val();
        $uploadpopupJIDSlocation = $('.upload-popupJIDS-locationedit').val();
        $uploadpopupJIDSvisibleto = $('.upload-popupJIDS-visibletoedit').html().trim(); 
        $uploadpopupJIDStaggedconnections = userwall_tagged_users;

        // check validation
        if($uploadpopupJIDSphototitle == '') {
            Materialize.toast('Add title', 2000, 'red');
            $('.upload-popupJIDS-phototitleedit').focus();
            applypostloader('HIDE');
            return false;
        }
        
        var formdata;
        formdata = new FormData();
        for(var i=0, len=storedFiles.length; i<len; i++) {
            formdata.append('images[]', storedFiles[i]);
        }
        formdata.append('title', $uploadpopupJIDSphototitle);
        formdata.append('description', $uploadpopupJIDSdescription);
        formdata.append('location', $uploadpopupJIDSlocation);
        formdata.append('visibleto', $uploadpopupJIDSvisibleto);
        formdata.append('tagged', $uploadpopupJIDStaggedconnections);
        formdata.append('custom', customArray);
        formdata.append('id', $editid);

        $.ajax({
            type: 'POST', 
            url: "?r=collections/editcollections", 
            data: formdata, 
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                //applypostloader('HIDE');
                var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    storedFiles = [];
                    storedFilesExsting = [];
                    userwall_tagged_users = [];
                    userwall_tagged_usersTemp = [];
                    customArrayTemp = [];
                    customArray = [];
                    photoUpload = [];
     
                    $('#edit-collection-popup').modal('close');
                    Materialize.toast('Saved', 2000, 'green');
                    window.location.href="";
                } else {
                    if(result.message != undefined && result.message != '') {
                        Materialize.toast(result.message, 2000, 'red');
                    }
                }
            }
        });     
    }
}

function removepiccollections(obj)
{   
    $this = $(obj);
    var $id = $this.attr('data-postid');
    var $src = $this.parents('.allow-gallery').find('.pic').attr('src');
    if($id) {   
        $.ajax({
            type: 'POST', 
            url: '?r=collections/removepic',  
            data: {$id, $src},
            success: function(data) {
                var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    Materialize.toast('Deleted', 2000, 'green');
                    $this.parents('.allow-gallery').remove();

                    justifiedGalleryinitialize();
                    lightGalleryinitializeforgallery();
                    
                    if($('#placebox').find('.allow-gallery').length <=0) {
                        $('#placebox').html('<div class="combined-column"> <div class="cbox-desc"> <div class="right upload-gallery" style="cursor: pointer;">Upload Photo</div> </div> </div> <div id="lgt-gallery-photoGallery" class="lgt-gallery-photoGallery lgt-gallery-justified dis-none"><div class="content-box bshadow"> <div class="post-holder bshadow"> <div class="joined-tb"> <i class="mdi mdi-file-outline"></i> <p>No pinned photos</p> </div> </div> </div></div>');
                    }
                }
            }
        });
    }
}


function removepiccollections_re(obj)
{   
    $this = $(obj);
    var $id = $this.attr('data-postid');
    var $src = $this.parents('.allow-gallery').find('.pic').attr('src');
    if($id) {   
        $.ajax({
            type: 'POST', 
            url: '?r=collections/removepic',  
            data: {$id, $src},
            success: function(data) {
                var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    Materialize.toast('Deleted', 2000, 'green');
                    window.location.href='';
                }
            }
        });
    }
}

function deletecollection(obj)
{   
    $this = $(obj);
    var $id = $this.attr('data-deleteid');
    if($id) {   
        $.ajax({
            type: 'POST', 
            url: '?r=collections/deletecollection',  
            data: {$id},
            success: function(data) {
                var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    Materialize.toast('Deleted', 2000, 'green');
                    $this.parents('.col').remove();
                    justifiedGalleryinitialize();
                    lightGalleryinitializeforgallery();
                    
                    if($('#placebox').find('.allow-gallery').length <=0) {
                        $('#placebox').html('<div class="combined-column"> <div class="cbox-desc"> <div class="right upload-gallery" style="cursor: pointer;">Upload Photo</div> </div> </div> <div id="lgt-gallery-photoGallery" class="lgt-gallery-photoGallery lgt-gallery-justified dis-none"><div class="content-box bshadow"> <div class="post-holder bshadow"> <div class="joined-tb"> <i class="mdi mdi-file-outline"></i> <p>No pinned photos</p> </div> </div> </div></div>');
                    }
                }
            }
        });
    }
}

function removepiccollections_modal($id, obj)
{   
    $this = $(obj);
    var $src = $this.parents('.img-box').find('img').attr('src');
    if($id) {   
        $.ajax({
            type: 'POST', 
            url: '?r=collections/removepic',  
            data: {$id, $src},
            success: function(data) {
                var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    Materialize.toast('Deleted', 2000, 'green');
                    $this.parents('.img-box').remove();

                    // Remove upload image input if 3 images is exists or uploaded....
                    if($('.modal.open').find('.upldimg').length >= 3) {
                        $('.modal.open').find('.custom-file').parents('.img-box').remove();
                    } else {
                        if(!$('.modal.open').find('.custom-file').length) {
                            $uploadBox = '<div class="img-box"> <div class="custom-file addimg-box add-photo ablum-add"> <span class="icont">+</span> <br><span class="">Update photo</span> <input class="upload custom-upload remove-custom-upload edit-collections-file-upload" id="edit-collections-file-upload" title="Choose a file to upload" data-class=".post-photos .img-row" type="file"> </div> </div>';
                            if($('.modal.open').find('.img-row').append($uploadBox));
                        }   
                    }
                }
            }
        });
    }
}