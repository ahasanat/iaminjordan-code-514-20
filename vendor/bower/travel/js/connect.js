/* suggest connect */
	function suggestConnect(connectid, suggestto){
		if (connectid != '' && suggestto != '' )
		{
			$.ajax({ 
				type: 'POST',
				url: '?r=site/suggestconnect',
				data: "connectid=" + connectid + "&suggestto=" + suggestto,
				success: function (data)
				{
					var result = $.parseJSON(data);
					if (result['status'] != '0')
					{
						$("#send_"+connectid).hide();
						$("#done_"+connectid).html(result['msg']);
						$("#done_"+connectid).show();
					}
				}
			});
		}
	}
/* end suggest connect */
	
/* connect listing */
	function connectList(fid){
		if (fid != ''){ 
			$.ajax({
				type: 'POST',
				url: '?r=site/suggestconnectlist',
				data: "fid="+fid,
				success: function (data)
				{
					$("#suggest-connections").html(data);
					$("#suggest-connections").css('width', '900px');
					$('#suggest-connections').modal('open');
				}
			});
		}
	}
/* end connect listing */

/* Start Function Connect menu Generalize */
	function fetchconnectmenu(guser_id, isnew='', obj='') {
		$.ajax({
			url: '?r=connect/fetchconnectmenu',  
			type: 'POST',
			data: {guser_id},
			success: function(data) {
				if(isnew == 'yes') {
					$(obj).parents('.dropdown.dropdown-custom').find('ul').html(data);
				} else {
					$('.add-icon_'+guser_id).parent('.overlay').find('.more-span').find('ul').html(data);
				}
			}
		});
	}
/* End  Function Connect menu Generalize */

function removeConnectFromListing(fid){
	$('#remove_' + fid).hide();
}

/* alert for not accepting connect request */ 
	function privateMessage(){
		Materialize.toast('This member is not accepting connect request.', 2000, 'red');
		return false;
	}
/* end alert for not accepting connect request */ 

/* Start Add/Remove Connect Function */
function addConnect(id){
	var status = $("#user_status").val();
	if(status == 0){
		return false;
	}	
	
	var login_user_id = $('#login_id').val();
	$.ajax({
		url: '?r=connect/add-connect', 
		type: 'POST',
		data: 'to_id=' + id + '&from_id=' + login_user_id,
		success: function (data) {
			if(data)
			{	 
				var result = $.parseJSON(data);

				if(result.auth == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} 
				else if(result.auth == 'checkuserauthclassg') {
					checkuserauthclassg();
				} 
				else { 
					$('.people_'+id).hide();
					$('.sendmsgdisplay_'+id).html(result.msg);
					$('.sendmsgdisplay_'+id).show();
					$('.sendmsg_'+id).show();
					$('.acceptmsg_'+id).html(result.msg);
					$('.acceptmsg_'+id).show();
					$('.wallconnectaction').attr('title',result.msg);
					$('.title_'+id).attr('title',result.msg);
					$('#frnd_rmv_img').remove();
					$('#frnd_img').html('<a class="gray-text-555"><i class="mdi mdi-account-minus"></i></a>');
					$('.add-icon_'+id).css('display','block');
					$('.add-icon_'+id).html('<a href="javascript:void(0);" class="gray-text-555" onclick="removeConnect(\''+id+'\',\''+login_user_id+'\',\''+id+'\', \'cancle_connect_request\')"><i class="mdi mdi-account-minus"></i></a>');

					if($('.wallpage-content').find('.wall-header').find('.header-strip').find('.fa-user-plus').length) {
						$('.people_'+id).show();
						$('.wallpage-content').find('.wall-header').find('.header-strip').find('.fa-user-plus').removeClass('fa-user-plus').addClass('fa-user-times');

						$('.wallpage-content').find('.wall-header').find('.header-strip').find('.fa-user-times').attr("onclick","removeConnect('"+id+"','"+login_user_id+"','"+id+"', 'cancle_connect_request')");
					}


					$(".addconnect").html('<a href="javascript:void(0);" onclick="removeConnect(\''+id+'\',\''+login_user_id+'\',\''+id+'\', \'cancle_connect_request\')">Cancel connect request</a>');
					$(".cancleconnectrequest").html('<a href="javascript:void(0);" onclick="addConnect(\''+id+'\')">Add connect</a>');

					$('.travconnections_'+id).html('<a href="javascript:void(0)" title="Add connect" class="gray-text-555"><i class="mdi mdi-account-plus  dis-none" onclick="addConnect(\''+id+'\')"></i><i class="mdi mdi-account-minus" onclick="removeConnect(\''+id+'\',\''+login_user_id+'\',\''+id+'\',\'cancle_connect_request\')"></i></a><a href="javascript:void(0)" class="tb-pyk-remove"></a>');

					$('.travconnections_'+id).find('.people_').addClass('people_'+id);
					$('.travconnections_'+id).find('.sendmsg_').addClass('sendmsg_'+id);
					Materialize.toast('Connect request sent.', 2000, 'green');
				}
			}   
		}
	});
}
/* End Add/Remove Connect Function */	

/* follow connect */
function followConnect(fid){
	if (fid != ''){
		$.ajax({
			type: 'POST',
			url: '?r=site/unfollowconnect',
			data: "fid=" + fid,
			success: function (data) {
				if (data){
					if(data === '1'){
						$(".follow_" + fid).html('Mute connect posts');
					}
					else if(data === '2'){
						$(".follow_" + fid).html('Unmute connect posts');
					}else{}
					$.ajax({
						type: 'POST',
						url: '?r=site/muteconnect',
						data: "fid=" + fid,
						success: function (data) {
							if (data){
								if(data === '1'){
									$(".mute_connect_" + fid).html('Mute notifications');
									Materialize.toast('Mute notifications', 2000, 'green');
								}
								else if(data === '2'){
									$(".mute_connect_" + fid).html('Unmute notifications');
									Materialize.toast('Unmute notifications', 2000, 'green');
								}
							}
						}
					});
				}
			}
		});
	}
}
/* end follow connect */

/* mute connect */
	function muteConnect(fid,  isPermission=false) {
		if($id) {
			if(isPermission) {	
				$.ajax({
					type: 'POST',
					url: '?r=site/muteconnect',
					data: "fid=" + fid,
					success: function (data) { 
						if (data)
						{
							if(data === '1'){
								$(".mute_connect_" + fid).html('Mute notifications');
								Materialize.toast('Mute notifications', 2000, 'green');
							}
							else if(data === '2'){
								$(".mute_connect_" + fid).html('Unmute notifications');
								Materialize.toast('Unmute notifications', 2000, 'green');
							}

							if(data === '1')
							{
								$(".mute_connect_" + fid).html('Mute notifications');
							}
							else if(data === '2')
							{
								$(".mute_connect_" + fid).html('Unmute notifications');
							}
							$(".discard_md_modal").modal("close");
						}
					}
				});
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
		    	disText.html("Mute connect?");
	            btnKeep.html("Keep");
	            btnDiscard.html("Mute");
	            btnDiscard.attr('onclick', 'muteConnect(\''+fid+'\', true)');
	            $(".discard_md_modal").modal("open");
			}
		}
	}
/* end mute connect */