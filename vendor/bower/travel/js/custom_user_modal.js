$('#custom_user_modal').modal({
    dismissible: false,
    opacity: .5,
    inDuration: 500,
    outDuration: 700,
    startingTop: '50%',
    endingTop: '50%',    
    ready: function (modal, trigger) {
        custom_user_modal();
        $('#custom_user_modal').find('.search_box').val('');
        customArrayTemp = customArray.slice();     
        $('#custom_user_modal').css('z-index', '1100');       
    },
    complete: function () { 
        isNormalpost = 'no';
        isAlbumphotobox = 'no';
    }
});

$(document).on("keyup", "#custom_user_modal .search_box", function () {
    var $key = $("#custom_user_modal").find(".search_box").val().trim();
    $.ajax({
        url: "?r=site/custom-user-search", 
        type: 'POST',
        data: {$key, customArrayTemp}, 
        success: function (data) {
            var result = $.parseJSON(data);
            if(result.status != undefined && result.status == true) {
                var content = result.html;
                var userCount = customArrayTemp.length;
                if(userCount>1) {
                    var label = userCount + ' users selected';
                } else {
                    var label = userCount + ' user selected';
                }

                $('#custom_user_modal').find('.selected_photo_text').html(label);
                if(content != '') {
                    $('#custom_user_modal').find('.person_box').html(content);
                } else {
                    getnolistfound('norecordfoundcustomuser');
                }
            } else {
                getnolistfound('norecordfoundcustomuser');
            }
        }
    });
}); 

$(document).on('click', '#privacymodal .head, #privacymodal .uniquehead', function (e) {
    if(e.toElement != undefined) {
        if(e.toElement.className == 'label') {
        } else {
            $(this).find('.label').trigger('click');
        }
    }
});

$(document).on('click', '#custom_user_modal .person_detail_container', function () {
    $(this).find(".chk_person").click();
    var $thisvalue = $(this).find(".chk_person").val();    

    if($(this).find(".chk_person").prop('checked') == true){
        if($.inArray($thisvalue, customArrayTemp) !== -1) {
        } else {
            customArrayTemp.push($thisvalue);
        }
    } else {
        customArrayTemp = $.grep(customArrayTemp, function(value) {
          return value != $thisvalue;
        });
    }

    if(customArrayTemp.length >0) {
        $label = customArrayTemp.length + ' users selected.';
        $('#custom_user_modal').find('#customdone').removeClass('focoutTRV03');
    } else {
        $label = '0 user selected.';            
        $('#custom_user_modal').find('#customdone').addClass('focoutTRV03');
    }
    $('#custom_user_modal').find('.selected_photo_text').html($label);
});   

$(document).on('click', '.customli_modal', function () {
    isnewwithmodal = 'no';
    if($('.modal.open').length) {
        $selectore = $('#privacymodal').find('#customdoneprivacymodal').attr('data-privacy');
        if($selectore != '' && $selectore !=  undefined) {
            if($('.modal.open').find('.'+$selectore).length) {
                isnewwithmodal = 'yes';
            } else {
                $amukcase = ['photosecurityprivacylabel', 'postprivacylabel', 'postonwallprivacylabel', 'activitypermissionprivacylabel', 'birth_date_privacy', 'gender_privacy'];
                if($.inArray($selectore, $amukcase) !== -1) {
                    isnewwithmodal = 'yes';
                }   
            }
        } 
    } 

    $('#custom_user_modal').find('.person_box').html('');
    $('#custom_user_modal').modal('open');
    setTimeout(function() {
        $('#custom_user_modal').css('z-index', '2500');
    }, 1000);
});

$(document).on('click', '.customli_modal_post', function () {
    customArray = [];
    customArrayTemp = [];
    if($(this).hasClass('itissharepost')) {
        $pid = selectedpostid;  
    } else if($(this).hasClass('itiseditpost')) {
        $pid = selectedpostid; 
    } else if($(this).parents('.col.s12').length){
        $pid = $(this).parents('.col.s12').attr('data-postid');
    } else if($(this).parents('.bborder.post-holder').length){
        $pid = $(this).parents('.bborder.post-holder').attr('data-postid');
    }

    if($pid) {
        $.ajax({ 
            url: "?r=site/getsinglepostcustom", 
            type: 'POST',  
            data: {id: $pid},
            success: function (data) {
                var result = $.parseJSON(data);
                if(result.ids != undefined) {
                    customArray = result.ids;
                    customArrayTemp = result.ids; 
                }

                isNormalpost = 'yes';
                selectedpostid = $pid;

                $('#custom_user_modal').find('.person_box').html('');
                $('#custom_user_modal').modal('open');

                setTimeout(function() {
                    $('#custom_user_modal').css('z-index', 2500);
                }, 1000);
            }
        });
    }
});

$(document).on('click', '#customdone', function () {
    customArray = customArrayTemp.slice();
    if(isNormalpost == 'yes') { 
        if(selectedpostid != '') {
            $iswmodal = true;
            if($('.modal.open').length) {
                $selectore = $('#privacymodal').find('#customdoneprivacymodal').attr('data-privacy');
                if($selectore != '' && $selectore !=  undefined) {
                    if($('.modal.open').find('.'+$selectore).length) {
                        $iswmodal = false;
                        $('.'+$selectore).each(function() {
                            $(this).find('span').first().html('Custom');
                        });

                        if($('#privacymodal').length) {
                            $('#privacymodal').modal('close');
                        }
                        $('#custom_user_modal').modal('close'); 
                    }
                }
            }

            if($iswmodal) {
                $.ajax({
                    type: 'POST',   
                    url: '?r=site/directprivacy',
                    data: {
                        '$privacy' : 'custom', 
                        '$id' : selectedpostid,
                        'customids' : customArray
                    },
                    success: function(data){
                        var result = $.parseJSON(data);
                        if(result.status != undefined && result.status == 'yes') {
                            Materialize.toast('Saved', 2000, 'green');
                            var $content = result.content;
                            $('#hide_'+selectedpostid).find('.timestamp').find('a.dropdown-button').html($content);
                        }
                        if($('#privacymodal').length) {
                            $('#privacymodal').modal('close');
                        }
                        $('#custom_user_modal').modal('close');    
                    }
                });
            }
        }
    } else if(isAlbumphotobox == 'yes') { 
        if(selectedpostid != '') {
            $iswmodal = true;
            if($('.modal.open').length) {
                $selectore = $('#privacymodal').find('#customdoneprivacymodal').attr('data-privacy');
                if($selectore != '' && $selectore !=  undefined) {
                    if($('.modal.open').find('.'+$selectore).length) {
                        $iswmodal = false;
                        $('.'+$selectore).each(function() {
                            $(this).find('span').first().html('Custom');
                        });

                        if($('#privacymodal').length) {
                            $('#privacymodal').modal('close');
                        }
                        $('#custom_user_modal').modal('close'); 
                    }
                }
            }

            if($iswmodal) {
                $.ajax({
                    type: 'POST',  
                    url: '?r=site/directprivacyalbumphoto',
                    data: {
                        '$privacy' : 'custom', 
                        '$id' : selectedpostid,
                        'customids' : customArray
                    },
                    success: function(data){
                        var result = $.parseJSON(data);
                        if(result.status != undefined && result.status == 'yes') {
                            Materialize.toast('Saved', 2000, 'green');
                            var $content = result.content;
                            $('.albumphotobox'+selectedpostid).find('.ksjdsikosadsa').html($content);
                        }
                        if($('#privacymodal').length) {
                            $('#privacymodal').modal('close');
                        }
                        $('#custom_user_modal').modal('close');    
                    }
                });
            }
        }
    } else { 
        if(isnewwithmodal == 'yes') {
            if($('.modal.open').length) {
                $selectore = $('#privacymodal').find('#customdoneprivacymodal').attr('data-privacy');
                if($selectore != '' && $selectore !=  undefined) {
                    if($('.modal.open').find('.'+$selectore).length) {
                        $('.'+$selectore).each(function() {
                            $(this).find('span').first().html('Custom');
                        });
                    } else if($('.'+$selectore).length) {
                        $('.'+$selectore).each(function() {
                            $(this).find('span').first().html('Custom');
                        });
                    }
                }
            }
        }
        
        if($('#privacymodal').length) {
            $('#privacymodal').modal('close');
        }
        $('#custom_user_modal').modal('close');    
    }
});

$(document).on('click', '#customdoneprivacymodal', function () {
    if($('input[name=privacyradiomodal]:checked').length) {
        var $privacyValue = $('input[name=privacyradiomodal]:checked').val();
        if($.inArray($privacyValue, $post_privacy_array) !== -1) {
            $id = $(this).attr('data-privacy');
             
            $('.'+$id).each(function() {
                $(this).find('span').first().html($privacyValue);
            }); 

            if(isAlbumphotobox == 'yes') {
                setAlbumPrivacy(selectedpostid, $privacyValue, 'this', 'yes', '.'+$id);                        
            }

            $('#privacymodal').modal('close');
        }
    }
});

function privacymodal(obj) {
    setTimeout(function() {
        $this = $(obj);
        $isFetch = $($this).attr('data-fetch');
        $modeltag = $($this).attr('data-modeltag');
        $label = $($this).attr('data-label');
        $choosed = $($this).find('span').text().trim();

        var loaded = $($this).attr('data-loaded');

        if (typeof loaded !== typeof undefined && loaded !== false) {
            var isloaded = true;
        } else {
            var isloaded = false;
        }

        var customNot = ['lookupsettingprivacylabel', 'connectrequestsettingprivacylabel', 'connectlistprivacylabel', 'tagreviewprivacylabel'];
        $.ajax({
            url: "?r=site/privacymodal", 
            type: 'POST', 
            data: { 
                fetch: $isFetch, 
                modeltag: $modeltag,
                label: $label,
                choosed: $choosed,
                selectedpostid: selectedpostid
            },
            success: function (data) {
                var result = $.parseJSON(data);
                if(isloaded) {
                    customArray = customArray;
                    customArrayTemp = customArray;
                } else {
                    if(result.ids != undefined) {
                        customArray = result.ids;
                        customArrayTemp = result.ids;
                    }
                }

                if(result.html != undefined) {
                    $('#privacymodal').html(result.html);

                    if($.inArray($modeltag, customNot) !== -1) {
                        $('#privacymodal').find('.head.customhead').remove();    
                    }

                    if($modeltag == 'lookupsettingprivacylabel') {
                        $('#privacymodal').find('.head.privatehead').remove();    
                    }

                    if($modeltag == 'postreviewprivacylabel' || $modeltag == 'tagreviewprivacylabel') {
                        $('#privacymodal').find('.head').remove();    
                        $('#privacymodal').find('.uniquehead').show();    
                    } else {
                        $('#privacymodal').find('.uniquehead').remove();    
                    }

                    $($this).attr('data-loaded', true);
                    $('#privacymodal').modal('open');
                    $('#privacymodal').css('z-index', '2000');
                }
            }
        });
    },400);
} 

function custom_user_modal() {
    $.ajax({ 
        url: "?r=site/custom", 
        type: 'POST', 
        data: {ids: customArray},
        success: function (data) {
            var result = $.parseJSON(data);
            if(result.status != undefined && result.status == true) {
                var content = result.html;
                var userCount = customArray.length;
                if(userCount>1) {
                    var label = userCount + ' users selected';
                    $('#custom_user_modal').find('#customdone').removeClass('focoutTRV03');
                } else {
                    var label = userCount + ' user selected';
                    $('#custom_user_modal').find('#customdone').addClass('focoutTRV03');
                }
                $('#custom_user_modal').find('.selected_photo_text').html(label);
                $('#custom_user_modal').find('.person_box').html(content);
            } else {
                $('#custom_user_modal').find('.selected_photo_text').html('0 user selected.');
                $('#custom_user_modal').find('#customdone').addClass('focoutTRV03');
                getnolistfound('norecordfoundtag');
            }
        }
    });
}