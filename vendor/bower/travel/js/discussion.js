$(document).ready(function() {
	justifiedGalleryinitialize();
	lightGalleryinitialize();
});
	
$(document).on('click', '.compose_discus', function (e) {
	if($(this).hasClass('checkuserauthclassnv')) {
		checkuserauthclassnv();
	} else if($(this).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
	    $.ajax({ 
	        url: '?r=discussion/composenewdiscussionpopup',
	        success: function(data) {
	            $('#compose_discus').html(data);
	            setTimeout(function() { 
	                initDropdown();
	                $('.tabs').tabs();
	                $('#compose_discus').modal('open');
	            }, 400);
	        }
	    }); 
	}
});

$(document).ready(function(){ 
	$w = $(window).width();
	if ( $w > 739) {      
	$(".places-tabs .sub-tabs li a").click(function(){
	   $("body").removeClass("remove_scroller");
	}); 
	$(".tabs.icon-menu.tabsnew li a").click(function(){
	   $("body").removeClass("remove_scroller");
	}); 
	$(".mbl-tabnav").click(function(){
	   $("body").removeClass("remove_scroller");
	}); 
	$(".clicable.viewall-link").click(function(){
	   $("body").removeClass("remove_scroller");
	}); 
	} else {
	$(".places-tabs .sub-tabs li a").click(function(){
	   $("body").addClass("remove_scroller");
	}); 
	$(".clicable.viewall-link").click(function(){
	   $("body").addClass("remove_scroller");
	});         
	$(".tabs.icon-menu.tabsnew li a").click(function(){
	   $("body").addClass("remove_scroller");
	}); 
	$(".mbl-tabnav").click(function(){
	   $("body").removeClass("remove_scroller");
	});
	}

	$(".header-icon-tabs .tabsnew .tab a").click(function(){
		$(".bottom_tabs").hide();
	});

	$(".places-tabs .tab a").click(function(){
		$(".top_tabs").hide();
	});

	

	$('.footer-section').css('left', '0');
	$w = $(window).width();
	if($w <= 768) {
		$('.main-footer').css({
		   'width': '100%',
		   'left': '0'
		});
	} else {
		var $_I = $('.places-content.places-all').width();
		var $__I = $('.places-content.places-all').find('.container').width();

		var $half = parseInt($_I) - parseInt($__I);
		$half = parseInt($half) / 2;

		$('.main-footer').css({
		   'width': $_I+'px',
		   'left': '-'+$half+'px'
		});
	}

	aboutplace(3);
});

$(document.body).on('click', '.customeditpopup-modal-discussion', function(e) {
	var editpostid = $(this).data('editpostid');
	selectedpostid = editpostid;
	$.ajax({
		type: 'POST',
		url: '?r=discussion/edit-post-pre-set-discussion', 
		data:'editpostid='+editpostid,
		success: function(data) {
			if(data) {
				if(data == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} else if(data == 'checkuserauthclassg') {
					checkuserauthclassg();
				} else {
					$('#postopenmodal').modal('close');
					
					$('#compose_discus').html(data);
					setTimeout(function(){
						$('#compose_discus').modal('open');
						setPostBtnStatus();
					},400);
					e.preventDefault();
				}
				setTimeout(function(){
					initDropdown();
					setGeneralThings();
					setaddUserForTag(editpostid);
				},800);
			}
		}
	});        	
});

$(window).resize(function() {
	
	if($('#places-all').hasClass('active')) {
		$('.footer-section').css('left', '0');
		$w = $(window).width();
		if($w <= 768) {
		   $('.main-footer').css({
		      'width': '100%',
		      'left': '0'
		   });
		} else {
		   var $_I = $('.places-content.places-all').width();
		   var $__I = $('.places-content.places-all').find('.container').width();

		   var $half = parseInt($_I) - parseInt($__I);
		   $half = parseInt($half) / 2;

		   $('.main-footer').css({
		      'width': $_I+'px',
		      'left': '-'+$half+'px'
		   });
		}
	}
});

$(document).on('click', '.tablist .tab a', function(e) {
	$href = $(this).attr('href');
	$href = $href.replace('#', '');

	$('.places-content').removeClass().addClass('places-content '+$href);

	
	$this = $(this);
});

function addDiscussion() {
	applypostloader('SHOW');
	var $select = $('#compose_discus');
	var $w = $(window).width();
	var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
    var title = $select.find("#title").val();
    var status = $select.find('#textInput').val();
    
    if (status != "" && reg.test(status) == true) {
         status = status.replace(reg, " ");
    }
    if (title != "" && reg.test(title) == true) {
         title = title.replace(reg, " ");
    }

    var formdata;
    formdata = new FormData($('form')[1]);
    for(var i=0, len=storedFiles.length; i<len; i++) {
		formdata.append('imageFile1[]', storedFiles[i]); 
    }

    for(var i=0, len=storedFilesExsting.length; i<len; i++) {
    	$sadks = storedFilesExsting[i].name;
		formdata.append('imageFile2[]', $sadks); 
    }

    formdata.append("test", status);
    
    if($w > 568) {
    	$post_privacy = $select.find('#post_privacy').text().trim();
	} else {
		$post_privacy = $select.find('#post_privacy2').text().trim();
	}

    if($.inArray($post_privacy, $post_privacy_array) > -1) {
	} else {
		$post_privacy = 'Public';
	}
    
    var $comment_setting = 'Enable';
    if($('#sharing_setting_btns').find('.toolbox_disable_comments').is(':checked')) {
    	$comment_setting = 'Disable';
    }

    $link_title = $('#link_title').val();
    $link_url = $('#link_url').val();
    $link_description = $('#link_description').val();
    $link_image = $('#link_image').val();
   
	$(".post_loadin_img").css("display","inline-block");
	
	formdata.append("posttags", addUserForTag);
	formdata.append("post_privacy", $post_privacy);
	formdata.append("link_title",$link_title);
	formdata.append("link_url",$link_url);
	formdata.append("link_description",$link_description);
	formdata.append("link_image",$link_image);
	formdata.append("title", title);
	formdata.append("comment_setting",$comment_setting);
	formdata.append('custom', customArray);
	formdata.append("current_location",place);
	$.ajax({
		url: '?r=discussion/add-discussion-places',
		type: 'POST',
		data:formdata,
		async:false,         
		processData: false,
		contentType: false,
		success: function(data) {
			$select.modal('close');
			if(data == 'checkuserauthclassnv') {
				checkuserauthclassnv();
			} else if(data == 'checkuserauthclassg') {
				checkuserauthclassg();
			} else {
				newct = 0;
				lastModified = [];
                storedFiles = [];
                storedFilesExsting = [];
                storedFiles.length = 0;
                storedFilesExsting.length = 0;
                if($('#places-discussion').find('.post-list').find('.joined-tb').length) {
                	$('#places-discussion').find('.post-list').find('.joined-tb').parents('.post-holder').remove();
                }

               	$('#places-discussion').find('.post-list').prepend(data);

				setTimeout(function() { 
		            initDropdown();
					lightGalleryinitialize();
					//resizefixPostImages();
					//applypostloader('HIDE');
		        }, 400); 
			}	
		} 
	});
	return true;
}

function deleteDiscussion(p_uid, pid, isPermission=false){
	if(isPermission) {
		$.ajax({
			url: '?r=discussion/delete-discussion',
			type: 'POST',
			data: 'post_user_id=' + p_uid + "&pid=" + pid,
			success: function (data) {
				$(".discard_md_modal").modal("close");
				if(data != '1') {
					var share_post_id = data;
					var share_counter = $("#shareid_"+share_post_id).html();
					if(share_counter !='') {
						var share_counter = share_counter.replace('(', '');
						var share_counter = share_counter.replace(')', '');
						var share_counter = parseInt(share_counter)-1;
						if(share_counter >= 1) {
							$("#shareid_"+share_post_id).html(' ('+share_counter+')');
						} else {
							$("#shareid_"+share_post_id).html('');
						}
					} else {
						$("#shareid_"+share_post_id).html(' (1)');
					}
				}	
				$('#hide_'+pid).remove();
				var $totalbox = $('#places-discussion').find('.bborder.post-holder').length;
				if($totalbox <= 0) {
					$('#places-discussion').find('.cbox-title').find('span.lt').html('(0)');	
					$('#places-discussion').find('.content-box').find('.cbox-desc').html('<div class="post-holder bshadow"><div class="joined-tb"> <i class="mdi mdi-file-outline"></i> <p>Become a first to write a discussion for this place</p> </div></div>');
				} else {
					$('#places-discussion').find('.cbox-title').find('span.lt').html('('+$totalbox+')');	
				}
				Materialize.toast('Deleted.', 2000, 'green');
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Delete post?");
        btnKeep.html("Keep");
        btnDiscard.html("Delete");
        btnDiscard.attr('onclick', 'deleteDiscussion(\''+p_uid+'\', \''+pid+'\', true)');
        $(".discard_md_modal").modal("open");
	}
}

function edit_discussion(pid) {
	if (pid != '') {
		applypostloader('SHOW');
		var formdata;
		var $select = $('#compose_discus');
		formdata = new FormData($('form')[1]);

		var $w = $(window).width();
		var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
	    var title = $select.find("input.post_title").val();
	    var status = $select.find('#textInput').val();
	    
	    if (status != "" && reg.test(status) == true) {
	         status = status.replace(reg, " ");
	    }
	    if (title != "" && reg.test(title) == true) {
	         title = title.replace(reg, " ");
	    }

	    var formdata;
	    formdata = new FormData($('form')[1]);
	    for(var i=0, len=storedFiles.length; i<len; i++) {
			formdata.append('imageFile1[]', storedFiles[i]); 
	    }

	    for(var i=0, len=storedFilesExsting.length; i<len; i++) {
	    	$sadks = storedFilesExsting[i].name;
			formdata.append('imageFile2[]', $sadks); 
	    }

	    formdata.append("test", status);
	    
	    if($w > 568) {
	    	$post_privacy = $select.find('#post_privacy').text().trim();
		} else {
			$post_privacy = $select.find('#post_privacy2').text().trim();
		}

	    if($.inArray($post_privacy, $post_privacy_array) > -1) {
		} else {
			$post_privacy = 'Public';
		}
	    var $comment_setting = 'Enable';
	    if($('#sharing_setting_btns').find('.toolbox_disable_comments').is(':checked')) {
	    	$comment_setting = 'Disable';
	    }

	    $link_title = $('#link_title').val();
	    $link_url = $('#link_url').val();
	    $link_description = $('#link_description').val();
	    $link_image = $('#link_image').val();
	   
		$(".post_loadin_img").css("display","inline-block");
		
		formdata.append("posttags", addUserForTag);
		formdata.append("post_privacy", $post_privacy);
		formdata.append("link_title",$link_title);
		formdata.append("link_url",$link_url);
		formdata.append("link_description",$link_description);
		formdata.append("link_image",$link_image);
		formdata.append("title", title);
		formdata.append("comment_setting",$comment_setting);
		formdata.append('custom', customArray);
		formdata.append("current_location",place);
		formdata.append("pid",pid);

		$.ajax({
			type: 'POST',
			url: '?r=discussion/edit-discussion',
			data: formdata,
			processData: false,
			contentType: false,
			success: function (data) {
				//applypostloader('HIDE');
				$select.modal('close');
				Materialize.toast('Published.', 2000, 'green');
				newct = 0;
				lastModified = [];
                storedFiles = [];
                storedFilesExsting = [];
                storedFiles.length = 0;
                storedFilesExsting.length = 0;
				$('#hide_'+pid).remove();
				$('.nm-postlist').prepend(data);
			},
			 complete: function(){
			    initDropdown(); 
		   		justifiedGalleryinitialize();
				lightGalleryinitialize();
	           	/*fixImageUI('newpost'); 
		   		resizefixPostImages();*/
			}
		});
	}
}