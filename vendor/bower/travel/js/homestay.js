var property_type_array = ["House", "Apartment", "Condominium", "Farmstay", "Houseboat", "Bed and breakfast"];
var guests_room_array = ["Entire place", "Private room", "Shared room"];
var bath_array = ["Private", "Shared"];
var guest_type_array = ["Males", "Females", "Couples", "Families", "Students"];
var currency_array = ["USD", "EUR", "YEN", "CAD", "AUE"];
var homestay_facilities_array = ['Swimming', 'Cooking', 'Washing', 'Shower', 'Tv', 'Elevator', 'Parking', 'Wifi'];

$(document).ready(function() {
	justifiedGalleryinitialize();
	lightGalleryinitialize();
});
 
$(document).on('click', '.homestayCreateAction', function () {
	if($(this).hasClass('checkuserauthclassnv')) {
		checkuserauthclassnv();
	} else if($(this).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
	    $.ajax({
		    url: "?r=homestay/createhomestayhtml",
		    success: function (data) {
		    	$('#homestayCreateModal').html(data);
		    	setTimeout(function(){ 
		    		$('#homestayCreateModal').modal('open');
		    		initDropdown(); 
		    	},400);
		    }
		});
	}
});


$(document).on('click', '.homestayEditAction', function () {
	if($(this).hasClass('checkuserauthclassnv')) {
		checkuserauthclassnv();
	} else if($(this).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
		$id = $(this).attr('data-editid');
		if($id) {
			$.ajax({
				url: "?r=homestay/edithomestayhtml",
				type: 'POST',
				data: {id:$id},
				success: function(data) {
					$('#homestayEditModal').html(data);
			    	setTimeout(function(){ 
			    		$('#homestayEditModal').modal('open');
			    		initDropdown(); 
			    	},400);
				}
			});
		}
	}
});

/* Noor JS */
$(document).ready(function(){ 
	$w = $(window).width();
		if ( $w > 739) {      
		$(".places-tabs .sub-tabs li a").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".tabs.icon-menu.tabsnew li a").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".mbl-tabnav").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".clicable.viewall-link").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
	} else {
		$(".places-tabs .sub-tabs li a").click(function(){
		   $("body").addClass("remove_scroller");
		}); 
		$(".clicable.viewall-link").click(function(){
		   $("body").addClass("remove_scroller");
		});         
		$(".tabs.icon-menu.tabsnew li a").click(function(){
		   $("body").addClass("remove_scroller");
		}); 
		$(".mbl-tabnav").click(function(){
		   $("body").removeClass("remove_scroller");
		});
	}

	$(".header-icon-tabs .tabsnew .tab a").click(function(){
		$(".bottom_tabs").hide();
	});

	$(".places-tabs .tab a").click(function(){
		$(".top_tabs").hide();
	});



	// footer work for places home page only
	$('.footer-section').css('left', '0');
	$w = $(window).width();
	if($w <= 768) {
		$('.main-footer').css({
		   'width': '100%',
		   'left': '0'
		});
	} else {
		var $_I = $('.places-content.places-all').width();
		var $__I = $('.places-content.places-all').find('.container').width();

		var $half = parseInt($_I) - parseInt($__I);
		$half = parseInt($half) / 2;

		$('.main-footer').css({
		   'width': $_I+'px',
		   'left': '-'+$half+'px'
		});
	}

	$('.homestay-page').find('.collection-card-inner').find('img').css('opacity', '1');


});

$(window).resize(function() {
	// footer work for places home page only
	if($('#places-all').hasClass('active')) {
		$('.footer-section').css('left', '0');
		$w = $(window).width();
		if($w <= 768) {
		   $('.main-footer').css({
		      'width': '100%',
		      'left': '0'
		   });
		} else {
		   var $_I = $('.places-content.places-all').width();
		   var $__I = $('.places-content.places-all').find('.container').width();

		   var $half = parseInt($_I) - parseInt($__I);
		   $half = parseInt($half) / 2;

		   $('.main-footer').css({
		      'width': $_I+'px',
		      'left': '-'+$half+'px'
		   });
		}
	}
});

$(document).on('click', '.tablist .tab a', function(e) {
	$href = $(this).attr('href');
	$href = $href.replace('#', '');
	$('.places-content').removeClass().addClass('places-content '+$href);
	$this = $(this);
});

$(document).on('click', '.homestayreview', function (e) {
	if($(this).hasClass('checkuserauthclassnv')) {
		checkuserauthclassnv();
	} else if($(this).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
		$.ajax({
	        url: '?r=homestay/review',
	        success: function(data){
	        	$('#homestay_review').html(data);
	            setTimeout(function() { 
	                initDropdown();
	                $('.tabs').tabs();
	                $('#homestay_review').modal('open');
	            }, 400);
	        }
	    });
	}
});


function createhomestay(obj) {
	if($(obj).hasClass('checkuserauthclassnv')) {  
		checkuserauthclassnv();
	} else if($(obj).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
		applypostloader('SHOW');
		var validate = true;
		var title = $('#title').val();
		var property_type = $('#property_type').find('span').text().trim();
        var guests_room_type = $("input[name='guests_room_type']:checked").val();
		var bath = $("input[name='bath']:checked").val();
		var guest_type = [];
		$.each($("input[name='guest_type']"), function(e){
			if($(this).is(":checked")){
			} else {
	        	var guest_type_s_ = $(this).val();
	        	if(jQuery.inArray(guest_type_s_, guest_type_array) !== -1) {            
		            guest_type.push(guest_type_s_);
		        }
			}
		});
		var homestay_location = $('#homestay_location').val();
		var adult_guest_rate = $('#adult_guest_rate').val();
		var currency = $('#currency').find('span').text().trim();
		var description = $('#description').val();
		var rules = $('#rules').val();
        var homestay_facilities = [];
        $.each($(".createhomestay_facilities"), function(e){
			if($(this).find('.check-image').hasClass('active-class')) {
	        	var homestay_facilities_s_ = $(this).find('.check-image').attr('data-value');
	        	if(jQuery.inArray(homestay_facilities_s_, homestay_facilities_array) !== -1) {
		            homestay_facilities.push(homestay_facilities_s_);
		        }
			}
		});
  
		if (title == null || title == undefined || title == '') {
			validate = false;
        	Materialize.toast('Add title.', 2000, 'red');
        	$('#title').focus();
        	applypostloader('HIDE');
        	return false;
        }

		if(jQuery.inArray(property_type, property_type_array) !== -1) {
		} else {
        	validate = false;
        	Materialize.toast('Select property type.', 2000, 'red');
        	$('#property_type').focus();
        	applypostloader('HIDE');
        	return false;
        }

		if(jQuery.inArray(guests_room_type, guests_room_array) !== -1) {
		} else {
			validate = false;
        	Materialize.toast('Select guest room type.', 2000, 'red');
        	applypostloader('HIDE');
        	return false;
        }

		if(jQuery.inArray(bath, bath_array) !== -1) {
		} else {
        	validate = false;
        	Materialize.toast('Select bath.', 2000, 'red');
        	applypostloader('HIDE');
        	return false;
        }
 
		if (homestay_location == null || homestay_location == undefined || homestay_location == '') {
			validate = false;
        	Materialize.toast('Add location.', 2000, 'red');
        	$('#homestay_location').focus();
        	applypostloader('HIDE');
        	return false;
        }

        if (adult_guest_rate == null || adult_guest_rate == undefined || adult_guest_rate == '') {
			validate = false;
        	Materialize.toast('Enter guest rate.', 2000, 'red');
        	$('#adult_guest_rate').focus();
        	applypostloader('HIDE');
        	return false;
        }

        if (currency == null || currency == undefined || currency == '') {
        	validate = false;
        	Materialize.toast('Select currency.', 2000, 'red');
        	$('#adult_guest_rate').focus();
        	applypostloader('HIDE');
        	return false;
        }

        if (description == null || description == undefined || description == '') {
			validate = false;
        	Materialize.toast('Add description.', 2000, 'red');
        	$('#description').focus();
        	applypostloader('HIDE');
        	return false;
        }

        if(storedFiles.length < 3) {
        	validate = false;
        	Materialize.toast('Please upload three cover photos.', 2000, 'red');
        	applypostloader('HIDE');
        	return false;
        }

        if(validate) {
	        var fd;
		    fd = new FormData();

		    fd.append('title', title);
		    fd.append('property_type', property_type);
		    fd.append('guests_room_type', guests_room_type);
		    fd.append('bath', bath);
		    fd.append('guest_type', guest_type);
		    fd.append('homestay_location', homestay_location);
		    fd.append('adult_guest_rate', adult_guest_rate);
		    fd.append('currency', currency);
		    fd.append('description', description);
		    fd.append('rules', rules);
		    fd.append('guest_type', guest_type);
		    fd.append('homestay_facilities', homestay_facilities);

		   	for(var i=0, len=storedFiles.length; i<len; i++) {
		        fd.append('images[]', storedFiles[i]);
		    }

			$.ajax({ 
				url: '?r=homestay/createhomestay',
				type: 'POST',
				data: fd,
				contentType: 'multipart/form-data',
				processData: false,
				contentType: false,
				success: function(data) {
					//applypostloader('HIDE');
					var result = $.parseJSON(data);
					if(result.status != undefined && result.status == true) {
						Materialize.toast('Created.', 2000, 'green');
						window.location.href="";
                	}
				}
			});
		}
	}
}

function edithomestay(id, obj) {
	if($(obj).hasClass('checkuserauthclassnv')) {  
		checkuserauthclassnv();
	} else if($(obj).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
		applypostloader('SHOW');
		var validate = true;
		var title = $('#edittitle').val();
		var property_type = $('#editproperty_type').find('span').text().trim();
		var guests_room_type = $("input[name='editguests_room_type']:checked").val();
		var bath = $("input[name='editbath']:checked").val();
		var guest_type = [];
		$.each($("input[name='editguest_type']"), function(e){
			if($(this).is(":checked")){
			} else {
		    	var guest_type_s_ = $(this).val();
		    	if(jQuery.inArray(guest_type_s_, guest_type_array) !== -1) {            
		            guest_type.push(guest_type_s_);
		        }
			}
		});
		var homestay_location = $('#edithomestay_location').val();
		var adult_guest_rate = $('#editadult_guest_rate').val();
		var currency = $('#editcurrency').find('span').text().trim();
		var description = $('#editdescription').val();
		var rules = $('#editrules').val();
		var homestay_facilities = [];
		$.each($(".edithomestay_facilities"), function(e){
			if($(this).find('.check-image').hasClass('active-class')) {
	        	var homestay_facilities_s_ = $(this).find('.check-image').attr('data-value');
	        	if(jQuery.inArray(homestay_facilities_s_, homestay_facilities_array) !== -1) {
		            homestay_facilities.push(homestay_facilities_s_);
		        }
			}
		});

		if (title == null || title == undefined || title == '') {
			validate = false;
			Materialize.toast('Add title.', 2000, 'red');
			$('#edittitle').focus();
			applypostloader('HIDE');
			return false;
		}

		if(jQuery.inArray(property_type, property_type_array) !== -1) {
		} else {
			validate = false;
			Materialize.toast('Select property type.', 2000, 'red');
			$('#editproperty_type').focus();
			applypostloader('HIDE');
			return false;
		}

		if(jQuery.inArray(guests_room_type, guests_room_array) !== -1) {
		} else {
			validate = false;
			Materialize.toast('Select guest room type.', 2000, 'red');
			applypostloader('HIDE');
			return false;
		}

		if(jQuery.inArray(bath, bath_array) !== -1) {
		} else {
			validate = false;
			Materialize.toast('Select bath.', 2000, 'red');
			applypostloader('HIDE');
			return false;
		}

		if (homestay_location == null || homestay_location == undefined || homestay_location == '') {
			validate = false;
			Materialize.toast('Add location.', 2000, 'red');
			$('#edithomestay_location').focus();
			applypostloader('HIDE');
			return false;
		}

		if (adult_guest_rate == null || adult_guest_rate == undefined || adult_guest_rate == '') {
			validate = false;
			Materialize.toast('Enter guest rate.', 2000, 'red');
			$('#editadult_guest_rate').focus();
			applypostloader('HIDE');
			return false;
		}

		if (currency == null || currency == undefined || currency == '') {
			validate = false;
			Materialize.toast('Select currency.', 2000, 'red');
			$('#editadult_guest_rate').focus();
			applypostloader('HIDE');
			return false;
		}

		if (description == null || description == undefined || description == '') {
			validate = false;
			Materialize.toast('Add description.', 2000, 'red');
			$('#editdescription').focus();
			applypostloader('HIDE');
			return false;
		}

		if ($('#homestayEditModal').find('.custom-file').length) {
			validate = false;
		    Materialize.toast('Please upload three cover photos.', 2000, 'red');
		    applypostloader('HIDE');
		    return false;
		} else if ($('#homestayEditModal').find('.post-photos').find('.img-row').find('.img-box').length != 3) {
			validate = false;
		    Materialize.toast('Please upload three cover photos.', 2000, 'red');
		    applypostloader('HIDE');
		    return false;
		}

		if(validate) {
		    var fd;
		    fd = new FormData();

		    fd.append('title', title);
		    fd.append('property_type', property_type);
		    fd.append('guests_room_type', guests_room_type);
		    fd.append('bath', bath);
		    fd.append('guest_type', guest_type);
		    fd.append('homestay_location', homestay_location);
		    fd.append('adult_guest_rate', adult_guest_rate);
		    fd.append('currency', currency);
		    fd.append('description', description);
		    fd.append('rules', rules);
		    fd.append('guest_type', guest_type);
		    fd.append('homestay_facilities', homestay_facilities);
		    fd.append('id', id);

		   	for(var i=0, len=storedFiles.length; i<len; i++) {
		        fd.append('images[]', storedFiles[i]);
		    }

			$.ajax({
				url: '?r=homestay/edithomestay',
				type: 'POST',
				data: fd,
				contentType: 'multipart/form-data',
				processData: false,
				contentType: false,
				success: function(data) {
					//applypostloader('HIDE');
					var result = $.parseJSON(data);
					if(result.status != undefined && result.status == true) {
						Materialize.toast('Updated.', 2000, 'green');
						window.location.href="";
		        	}
				}
			});
		}
	}
}

function removepichomestay_modal($id, obj)
{   
    $this = $(obj);
    var $src = $this.parents('.img-box').find('img').attr('src');
    if($id) {   
        $.ajax({
            type: 'POST', 
            url: '?r=homestay/removepic',  
            data: {$id, $src},
            success: function(data) {
            	var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    Materialize.toast('Deleted', 2000, 'green');
                    $this.parents('.img-box').remove();

                    // Remove upload image input if 3 images is exists or uploaded....
                    if($('.modal.open').find('.upldimg').length >= 3) {
                        $('.modal.open').find('.custom-file').parents('.img-box').remove();
                    } else {
                        if(!$('.modal.open').find('.custom-file').length) {
                            $uploadBox = '<div class="img-box"> <div class="custom-file addimg-box add-photo ablum-add"> <span class="icont">+</span> <br><span class="">Update photo</span> <input class="upload custom-upload remove-custom-upload edit-collections-file-upload" id="edit-collections-file-upload" title="Choose a file to upload" data-class=".post-photos .img-row" type="file"> </div> </div>';
                            if($('.modal.open').find('.img-row').append($uploadBox));
                        }   
                    }
                }
            }
        });
    }
}

function addHomestayReview() { 
	applypostloader('SHOW');
	var $select = $('#homestay_review');

	var $w = $(window).width();
	var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
    var title = $select.find("#title").val();
    var status = $select.find('#textInput').val();
    
    if (status != "" && reg.test(status) == true) {
         status = status.replace(reg, " ");
    }
    if (title != "" && reg.test(title) == true) {
         title = title.replace(reg, " ");
    }

    var formdata;
    formdata = new FormData($('form')[1]);

    formdata.append("test", status);
    
    if($w > 568) {
    	$post_privacy = $select.find('#post_privacy').text().trim();
	} else {
		$post_privacy = $select.find('#post_privacy2').text().trim();
	}

    if($.inArray($post_privacy, $post_privacy_array) > -1) {
	} else {
		$post_privacy = 'Public';
	}
    var $share_setting = 'Enable';
    var $comment_setting = 'Enable';
    if($('#sharing_setting_btns').find('.toolbox_disable_sharing').is(':checked')) {
    	$share_setting = 'Disable';
    }
    if($('#sharing_setting_btns').find('.toolbox_disable_comments').is(':checked')) {
    	$comment_setting = 'Disable';
    }

    $link_title = $('#link_title').val();
    $link_url = $('#link_url').val();
    $link_description = $('#link_description').val();
    $link_image = $('#link_image').val();
   
   	var $totalStartFill = $select.find('.rating-stars').find('i.active').length;
	if($totalStartFill <=0) {
		Materialize.toast('Rate your self.', 2000, 'red');
		applypostloader('HIDE');
		return false;
	}

	$(".post_loadin_img").css("display","inline-block");
	
	formdata.append("posttags", addUserForTag);
	formdata.append("post_privacy", $post_privacy);
	formdata.append("link_title",$link_title);
	formdata.append("link_url",$link_url);
	formdata.append("link_description",$link_description);
	formdata.append("link_image",$link_image);
	formdata.append("title", title);
	formdata.append("share_setting",$share_setting);
	formdata.append("comment_setting",$comment_setting);
	formdata.append('custom', customArray);
	//formdata.append("current_location",place); 
	formdata.append("placereview", $totalStartFill);
	
	$.ajax({
		url: '?r=homestay/addreview',
		type: 'POST',
		data:formdata,
		async:false,        
		processData: false,
		contentType: false,
		success: function(data) {
			//applypostloader('HIDE');
			$select.modal('close');
			if(data == 'checkuserauthclassnv') {
				checkuserauthclassnv();
			} else if(data == 'checkuserauthclassg') {
				checkuserauthclassg(); 
			} else {
				$('.empty_review_block').remove();
				Materialize.toast('Published.', 2000, 'green');
				newct = 0;
				lastModified = [];
                storedFiles = []; 
                storedFilesExsting = [];
                storedFiles.length = 0;
                storedFilesExsting.length = 0;
				$(data).hide().prependTo(".collection").fadeIn(3000);
				setTimeout(function() { 
		            initDropdown();
		        }, 400);
			}	
		}
	}).done(function(){
		lightGalleryinitialize();
	   	//resizefixPostImages();
  	});
	return true;
}



function uploadphotoshomestay(id, obj) {
    if($(obj).hasClass('checkuserauthclassnv')) {
        checkuserauthclassnv();
    } else if($(obj).hasClass('checkuserauthclassg')) {
        checkuserauthclassg();
    } else {
        if(id != undefined && id != '') {  
            $.ajax({ 
                url: '?r=homestay/uploadphotoshomestay',
                type: 'POST', 
                data: {id},
                success: function(data) {
                    $('#uploadphotosHomestayModal').html(data);
                    setTimeout(function() { 
                        initDropdown();
                        $('#uploadphotosHomestayModal').modal('open'); 
                    }, 400);
                }
            });
        }
    }
}

function uploadphotoshomestaysave(id, obj) {
	if($(obj).hasClass('checkuserauthclassnv')) {  
		checkuserauthclassnv();
	} else if($(obj).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
		applypostloader('SHOW');
		var validate = true;
		if ($('#uploadphotosHomestayModal').find('.custom-file').length) {
			validate = false;
		    Materialize.toast('Please upload three cover photos.', 2000, 'red');
		    applypostloader('HIDE');
		    return false;
		} 

		if ($('#uploadphotosHomestayModal').find('.post-photos').find('.img-row').find('.img-box').length != 3) {
			validate = false;
		    Materialize.toast('Please upload three cover photos.', 2000, 'red');
		    applypostloader('HIDE');
		    return false;
		}

		if(validate) {
			var fd;
		    fd = new FormData();
		    fd.append('id', id);
		   	for(var i=0, len=storedFiles.length; i<len; i++) {
		        fd.append('images[]', storedFiles[i]);
		    }

			$.ajax({
				url: '?r=homestay/uploadphotoshomestaysave',
				type: 'POST',
				data: fd,
				contentType: 'multipart/form-data',
				processData: false,
				contentType: false,
				success: function(data) {
					//applypostloader('HIDE');
					var result = $.parseJSON(data);
					if(result.status != undefined && result.status == true) {
						Materialize.toast('Updated.', 2000, 'green');
						window.location.href="";
		        	}
				}
			});
		}
	}
}

function deleteHomestay(nid, isPermission=false){
	if(isPermission) {
		$.ajax({
			url: '?r=homestay/delete', 
			type: 'POST',
			data: {nid},
			success: function (data) {
				var result = $.parseJSON(data);
				if(result.status != undefined && result.status == true) {
					Materialize.toast('Homestay deleted', 2000, 'green');
					window.location.href="";
				}
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
		var btnKeep = $(".discard_md_modal .modal_keep");
		var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Delete homestay.");
		btnKeep.html("Keep");
		btnDiscard.html("Delete");
		btnDiscard.attr('onclick', 'deleteHomestay(\''+nid+'\', true)');
		$(".discard_md_modal").modal("open");
	}
}