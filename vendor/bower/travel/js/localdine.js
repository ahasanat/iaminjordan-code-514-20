var event_type_array = ["Aperitif", "Breakfast", "Brunch", "Cooking class", "Dinner", "Food tour", "Lunch", "Tasting", "Tea time", "Picnic"];
var cuisine_array = ["African", "American", "Antique", "Asian", "Barbecue", "Basque", "Belgian", "Brazilian", "British", "Cajun & Creole", "Cambodian", "Caribbean", "Catalan", "Chilean", "Chinese", "Creole", "Danish", "Dutch", "Eastern Europe", "European", "French", "Fusion", "German", "Greek", "Hawaiian", "Hungarian", "Icelandic", "Indian", "Indonesian", "Irish", "Italian", "Jamaican", "Japanese", "Korean", "Kurdish", "Latin American", "Malay", "Malaysian", "Mediterranean", "Mexican", "Middle Eastern", "Nepalese", "Nordic", "North African", "Organic", "Other", "Persian", "Peruvian", "Philippine", "Portuguese", "Russian", "Sami", "Scandinavian", "Seafood", "Singaporean", "South American", "Southern & Soul", "Spanish", "Sri Lankan", "Thai", "Turkish", "Vietnamese"];
var guests_array = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100"];
var meal_array = ["USD", "EUR", "YEN", "CAD", "AUE"];
var max_fields = 10; 
var x = 1;
var ex = 1;
$(document).ready(function() {
  justifiedGalleryinitialize();
  lightGalleryinitialize();
});

$(document).on('click', '#addDish', function(){
  x = 1;  
  if(x < max_fields){ 
    x++; 
    $('.modal.open').find(".dish-wrapper").append('<div class="dish-wrapper dishblock"><div class="fulldiv mobile275"><div class="frow"><div class="caption-holder"><label>Dish name</label></div><div class="detail-holder"><div class="input-field"><input type="text" placeholder="i.e Appetiser, Main Dish, Dessert" class="fullwidth locinput dishname "/></div></div></div></div><a href="#" class="remove-field"><i class="mdi mdi-close"></i></a><div class="fulldiv mobile275"><div class="frow"><div class="caption-holder mb0"><label>Summary</label></div><div class="detail-holder"><div class="input-field"><textarea class="materialize-textarea md_textarea item_tagline summary" placeholder="Tell your guest what you are cooking. Detail description get the most guests joining up!"></textarea></div></div></div></div></div></div>');
  }
});

$(document).on('click', '#editaddDish', function(){
  ex = 1;  
  if(ex < max_fields){ 
    ex++; 
    $('.modal.open').find(".dish-wrapper").append('<div class="dish-wrapper dishblock"><div class="fulldiv mobile275"><div class="frow"><div class="caption-holder"><label>Dish name</label></div><div class="detail-holder"><div class="input-field"><input type="text" placeholder="i.e Appetiser, Main Dish, Dessert" class="fullwidth locinput editdishname "/></div></div></div></div><a href="#" class="remove-field"><i class="mdi mdi-close"></i></a><div class="fulldiv mobile275"><div class="frow"><div class="caption-holder mb0"><label>Summary</label></div><div class="detail-holder"><div class="input-field"><textarea class="materialize-textarea md_textarea item_tagline editsummary" placeholder="Tell your guest what you are cooking. Detail description get the most guests joining up!"></textarea></div></div></div></div></div></div>');
  }
});

$(document).on('click', '#dineCreateModal .remove-field', function(e){
  e.preventDefault(); 
  $(this).parent('div').remove(); 
  x--;
});

$(document).on('click', '#dineEditModal .remove-field', function(e){
  e.preventDefault(); 
  $(this).parent('div').remove(); 
  ex--;
});

$(document).ready(function(){ 
   $w = $(window).width();
   if ( $w > 739) {      
      $(".places-tabs .sub-tabs li a").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
      $(".tabs.icon-menu.tabsnew li a").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
      $(".mbl-tabnav").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
      $(".clicable.viewall-link").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
   } else {
      $(".places-tabs .sub-tabs li a").click(function(){
         $("body").addClass("remove_scroller");
      }); 
      $(".clicable.viewall-link").click(function(){
         $("body").addClass("remove_scroller");
      });         
      $(".tabs.icon-menu.tabsnew li a").click(function(){
         $("body").addClass("remove_scroller");
      }); 
      $(".mbl-tabnav").click(function(){
         $("body").removeClass("remove_scroller");
      });
   }

   $(".header-icon-tabs .tabsnew .tab a").click(function(){
      $(".bottom_tabs").hide();
   });

   $(".places-tabs .tab a").click(function(){
      $(".top_tabs").hide();
   });

   // footer work for places home page only
   $('.footer-section').css('left', '0');
   $w = $(window).width();
   if($w <= 768) {
      $('.main-footer').css({
         'width': '100%',
         'left': '0'
      });
   } else {
      var $_I = $('.places-content.places-all').width();
      var $__I = $('.places-content.places-all').find('.container').width();

      var $half = parseInt($_I) - parseInt($__I);
      $half = parseInt($half) / 2;

      $('.main-footer').css({
         'width': $_I+'px',
         'left': '-'+$half+'px'
      });
   }

   $('.localdine-page').find('.collection-card-inner').find('img').css('opacity', '1');
});

$(window).resize(function() {
   if($('#places-all').hasClass('active')) {
      $('.footer-section').css('left', '0');
      $w = $(window).width();
      if($w <= 768) {
         $('.main-footer').css({
            'width': '100%',
            'left': '0'
         });
      } else {
         var $_I = $('.places-content.places-all').width();
         var $__I = $('.places-content.places-all').find('.container').width();

         var $half = parseInt($_I) - parseInt($__I);
         $half = parseInt($half) / 2;

         $('.main-footer').css({
            'width': $_I+'px',
            'left': '-'+$half+'px'
         });
      }
   }
});

$(document).on('click', '.tablist .tab a', function(e) {
   $href = $(this).attr('href');
   $href = $href.replace('#', '');
   $('.places-content').removeClass().addClass('places-content '+$href);
   $this = $(this);
});  

$(document).on('click', '.createlocaldine', function () {
  if($(this).hasClass('checkuserauthclassnv')) {  
    checkuserauthclassnv();
  } else if($(this).hasClass('checkuserauthclassg')) {
    checkuserauthclassg();
  } else {
    $.ajax({
      url: '?r=localdine/createlocaldine',
      success: function(data){
        $('#dineCreateModal').html(data);
          setTimeout(function() { 
              initDropdown();
              $('#dineCreateModal').modal('open'); 
          }, 400);
      }
    });
  }
});

$(document).on('click', '.editlocaldine', function () {
  if($(this).hasClass('checkuserauthclassnv')) {  
    checkuserauthclassnv();
  } else if($(this).hasClass('checkuserauthclassg')) {
    checkuserauthclassg();
  } else {

    $id = $(this).attr('data-editid');
    if($id) {
      $.ajax({
        url: '?r=localdine/editlocaldine',
        type: 'POST',
        data: {id: $id},
        success: function(data){
          $('#dineEditModal').html(data);
            setTimeout(function() { 
                initDropdown();
                $('#dineEditModal').modal('open'); 
            }, 400);
        }
      });
    }
  }
});

$(document).on('click', '.uploadphotolocaldine', function () {
  if($(this).hasClass('checkuserauthclassnv')) {  
    checkuserauthclassnv();
  } else if($(this).hasClass('checkuserauthclassg')) {
    checkuserauthclassg();
  } else {

    $id = $(this).attr('data-editid');
    if($id) {
      $.ajax({
        url: '?r=localdine/uploadphotoslocaldine',
        type: 'POST',
        data: {id: $id},
        success: function(data){
          $('#uploadphotosLocaldineModal').html(data);
            setTimeout(function() { 
                initDropdown();
                $('#uploadphotosLocaldineModal').modal('open'); 
            }, 400);
        }
      });
    }
  }
});

$(document).on('click', '.localdinereview', function (e) {
  if($(this).hasClass('checkuserauthclassnv')) {
    checkuserauthclassnv();
  } else if($(this).hasClass('checkuserauthclassg')) {
    checkuserauthclassg();
  } else {
    $.ajax({
          url: '?r=localdine/review',
          success: function(data){
            $('#localdine_review').html(data);
              setTimeout(function() { 
                  initDropdown();
                  $('.tabs').tabs();
                  $('#localdine_review').modal('open');
              }, 400);
          }
      });
  }
});

function createlocaldinesave() {  
  applypostloader('SHOW');
  var validate = true;
  var event_type = $('#event_type').find('span').text().trim();
  var cuisine = $('#cuisine').find('span').text().trim();
  var minguests = $('#minguests').find('span').text().trim();
  var maxguests = $('#maxguests').find('span').text().trim();
  var title = $('#title').val();
  var description = $('#description').val();
  var dishname = $('#dishname').val();
  var summary = $('#summary').val();
  var meal = $('#meal').val();
  var currency = $('#currency').find('span').text().trim();
  var whereevent = $('#whereevent').val();

  var fd;
  fd = new FormData();

  if(event_type_array.indexOf(event_type) !== -1){
  } else {
    validate = false;
    Materialize.toast('Select event type.', 2000, 'red');
    applypostloader('HIDE');
    return false;    
  }

  if (title == null || title == undefined || title == '') {
    validate = false;
    Materialize.toast('Enter title.', 2000, 'red');
    $('#title').focus();
    applypostloader('HIDE');
    return false; 
  }

  if (description == null || description == undefined || description == '') {
    validate = false;
    Materialize.toast('Enter description.', 2000, 'red');
    $('#description').focus();
    applypostloader('HIDE');
    return false; 
  }

  $summerySelector = $('.summary');
  $.each($summerySelector, function(i, v){
    $summaryCurrent = $(this).val();
    
    if($summaryCurrent == '') {
      validate = false;
      Materialize.toast('Enter summary.', 2000, 'red');
      $(v).focus();
      applypostloader('HIDE');
      return false;
    }

    fd.append('summary[]', $summaryCurrent);
  });

  $dishnameSelector = $('.dishname');
  $.each($dishnameSelector, function(i, v){
    $dishnameCurrent = $(this).val();

    if($dishnameCurrent == '') {
      validate = false;
      Materialize.toast('Enter dish name.', 2000, 'red');
      $(v).focus();
      applypostloader('HIDE');
      return false;
    }

    fd.append('dishname[]', $dishnameCurrent);
  });

  if (meal == null || meal == undefined || meal == '') {
    validate = false;
    Materialize.toast('Enter meal rate.', 2000, 'red');
    $('#meal').focus();
    applypostloader('HIDE');
    return false; 
  }

  if (currency == null || currency == undefined || currency == '') {
    validate = false;
    Materialize.toast('Select currency.', 2000, 'red');
    $('#currency').focus();
    applypostloader('HIDE');
    return false; 
  }

  if (whereevent == null || whereevent == undefined || whereevent == '') {
    validate = false;
    Materialize.toast('Enter host event location.', 2000, 'red');
    $('#whereevent').focus();
    applypostloader('HIDE');
    return false; 
  }

  if (storedFiles.length < 3) {
    validate = false;
    Materialize.toast('Please upload three cover photos.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  }

  for(var i=0, len=storedFiles.length; i<len; i++) {
      fd.append('images[]', storedFiles[i]);
  }
  fd.append('event_type', event_type);
  fd.append('cuisine', cuisine);
  fd.append('minguests', minguests);
  fd.append('maxguests', maxguests);
  fd.append('title', title);
  fd.append('description', description);
  fd.append('meal', meal);
  fd.append('currency', currency);
  fd.append('whereevent', whereevent);

  if(validate) {
    $.ajax({
      url: '?r=localdine/createlocaldinesave',
      type: 'POST',
      data: fd,
      contentType: 'multipart/form-data',
      processData: false,
      contentType: false,
      success: function(result) {
        //applypostloader('HIDE');
        $result = JSON.parse(result);
        if($result.status != undefined && $result.status == true) {
        $('#dineCreateModal').modal('close'); 
          Materialize.toast('Localdine created.', 2000, 'green');
          window.location.href="";
        }
      }
    });
  }
}

function editlocaldinesave($id, obj) {  
  applypostloader('SHOW');
  var validate = true;
  var event_type = $('#editevent_type').find('span').text().trim();
  var cuisine = $('#editcuisine').find('span').text().trim();
  var minguests = $('#editminguests').find('span').text().trim();
  var maxguests = $('#editmaxguests').find('span').text().trim();
  var title = $('#edittitle').val();
  var description = $('#editdescription').val();
  var dishname = $('#editdishname').val();
  var summary = $('#editsummary').val();
  var meal = $('#editmeal').val();
  var currency = $('#editcurrency').find('span').text().trim();
  var whereevent = $('#editwhereevent').val();

  var fd;
  fd = new FormData();

  if(event_type_array.indexOf(event_type) !== -1){
  } else {
    validate = false;
    Materialize.toast('Select event type.', 2000, 'red');
    applypostloader('HIDE');
    return false;    
  }

  if (title == null || title == undefined || title == '') {
    validate = false;
    Materialize.toast('Enter title.', 2000, 'red');
    $('#title').focus();
    applypostloader('HIDE');
    return false; 
  }

  if (description == null || description == undefined || description == '') {
    validate = false;
    Materialize.toast('Enter description.', 2000, 'red');
    $('#description').focus();
    applypostloader('HIDE');
    return false; 
  }

  $summerySelector = $('.editsummary');
  $.each($summerySelector, function(i, v){
    $summaryCurrent = $(this).val();
    
    if($summaryCurrent == '') {
      validate = false;
      Materialize.toast('Enter summary.', 2000, 'red');
      $(v).focus();
    applypostloader('HIDE');
      return false;
    }

    fd.append('summary[]', $summaryCurrent);
  });

  $dishnameSelector = $('.editdishname');
  $.each($dishnameSelector, function(i, v){
    $dishnameCurrent = $(this).val();

    if($dishnameCurrent == '') {
      validate = false;
      Materialize.toast('Enter dish name.', 2000, 'red');
      $(v).focus();
    applypostloader('HIDE');
      return false;
    }

    fd.append('dishname[]', $dishnameCurrent);
  });

  if (meal == null || meal == undefined || meal == '') {
    validate = false;
    Materialize.toast('Enter meal rate.', 2000, 'red');
    $('#meal').focus();
    applypostloader('HIDE');
    return false; 
  }

  if (currency == null || currency == undefined || currency == '') {
    validate = false;
    Materialize.toast('Select currency.', 2000, 'red');
    $('#currency').focus();
    applypostloader('HIDE');
    return false; 
  }

  if (whereevent == null || whereevent == undefined || whereevent == '') {
    validate = false;
    Materialize.toast('Enter host event location.', 2000, 'red');
    $('#whereevent').focus();
    applypostloader('HIDE');
    return false; 
  }

  if ($('#dineEditModal').find('.custom-file').length) {
    validate = false;
    Materialize.toast('Please upload three cover photos.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  } else if ($('#dineEditModal').find('.post-photos').find('.img-row').find('.img-box').length != 3) {
    validate = false;
    Materialize.toast('Please upload three cover photos.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  }

  for(var i=0, len=storedFiles.length; i<len; i++) {
      fd.append('images[]', storedFiles[i]);
  }
  fd.append('event_type', event_type);
  fd.append('cuisine', cuisine);
  fd.append('minguests', minguests);
  fd.append('maxguests', maxguests);
  fd.append('title', title);
  fd.append('description', description);
  fd.append('meal', meal);
  fd.append('currency', currency);
  fd.append('whereevent', whereevent);
  fd.append('id', $id);

  if(validate) {
    $.ajax({
      url: '?r=localdine/editlocaldinesave',
      type: 'POST',
      data: fd,
      contentType: 'multipart/form-data',
      processData: false,
      contentType: false,
      success: function(result) {
        //applypostloader('HIDE');
        $result = JSON.parse(result);
        if($result.status != undefined && $result.status == true) {
          $('#dineEditModal').modal('close'); 
          Materialize.toast('Localdine updated.', 2000, 'green');
          window.location.href="";
        }
      }
    });
  }  
}

function removepiclocaldine_modal($id, obj)
{   
    $this = $(obj);
    var $src = $this.parents('.img-box').find('img').attr('src');
    if($id) {   
        $.ajax({
            type: 'POST', 
            url: '?r=localdine/removepic',  
            data: {$id, $src},
            success: function(data) {
              var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    Materialize.toast('Deleted', 2000, 'green');
                    $this.parents('.img-box').remove();

                    // Remove upload image input if 3 images is exists or uploaded....
                    if($('.modal.open').find('.upldimg').length >= 3) {
                        $('.modal.open').find('.custom-file').parents('.img-box').remove();
                    } else {
                        if(!$('.modal.open').find('.custom-file').length) {
                            $uploadBox = '<div class="img-box"> <div class="custom-file addimg-box add-photo ablum-add"> <span class="icont">+</span> <br><span class="">Update photo</span> <input class="upload custom-upload remove-custom-upload edit-collections-file-upload" id="edit-collections-file-upload" title="Choose a file to upload" data-class=".post-photos .img-row" type="file"> </div> </div>';
                            if($('.modal.open').find('.img-row').append($uploadBox));
                        }   
                    }
                }
            }
        });
    }
}

function addLocaldineReview() { 
  applypostloader('SHOW');
  var $select = $('#localdine_review');

  var $w = $(window).width();
  var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
    var title = $select.find("#title").val();
    var status = $select.find('#textInput').val();
    
    if (status != "" && reg.test(status) == true) {
         status = status.replace(reg, " ");
    }
    if (title != "" && reg.test(title) == true) {
         title = title.replace(reg, " ");
    }

    var formdata;
    formdata = new FormData($('form')[1]);

    formdata.append("test", status);
    
    if($w > 568) {
      $post_privacy = $select.find('#post_privacy').text().trim();
  } else {
    $post_privacy = $select.find('#post_privacy2').text().trim();
  }

    if($.inArray($post_privacy, $post_privacy_array) > -1) {
  } else {
    $post_privacy = 'Public';
  }
    var $share_setting = 'Enable';
    var $comment_setting = 'Enable';
    if($('#sharing_setting_btns').find('.toolbox_disable_sharing').is(':checked')) {
      $share_setting = 'Disable';
    }
    if($('#sharing_setting_btns').find('.toolbox_disable_comments').is(':checked')) {
      $comment_setting = 'Disable';
    }

    $link_title = $('#link_title').val();
    $link_url = $('#link_url').val();
    $link_description = $('#link_description').val();
    $link_image = $('#link_image').val();
   
    var $totalStartFill = $select.find('.rating-stars').find('i.active').length;
  if($totalStartFill <=0) {
    Materialize.toast('Rate your self.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  }

  $(".post_loadin_img").css("display","inline-block");
  
  formdata.append("posttags", addUserForTag);
  formdata.append("post_privacy", $post_privacy);
  formdata.append("link_title",$link_title);
  formdata.append("link_url",$link_url);
  formdata.append("link_description",$link_description);
  formdata.append("link_image",$link_image);
  formdata.append("title", title);
  formdata.append("share_setting",$share_setting);
  formdata.append("comment_setting",$comment_setting);
  formdata.append('custom', customArray);
  //formdata.append("current_location",place); 
  formdata.append("placereview", $totalStartFill);
  
  $.ajax({
    url: '?r=localdine/addreview',
    type: 'POST',
    data:formdata,
    async:false,        
    processData: false,
    contentType: false,
    success: function(data) {
      //applypostloader('HIDE'); 
      $select.modal('close');
      if(data == 'checkuserauthclassnv') {
        checkuserauthclassnv();
      } else if(data == 'checkuserauthclassg') {
        checkuserauthclassg(); 
      } else {
        Materialize.toast('Published.', 2000, 'green');
        newct = 0;
        lastModified = [];
        storedFiles = [];
        storedFilesExsting = [];
        storedFiles.length = 0;
        storedFilesExsting.length = 0;
        $(".empty_review_block").remove();
        $(data).hide().prependTo(".collection").fadeIn(3000);
        setTimeout(function() { 
            initDropdown();
        }, 400);
      } 
    }
  }).done(function(){
    lightGalleryinitialize();
      //resizefixPostImages();
    });
  return true;
}

function uploadphotoslocaldine(id, obj) {
    if($(obj).hasClass('checkuserauthclassnv')) {
        checkuserauthclassnv();
    } else if($(obj).hasClass('checkuserauthclassg')) {
        checkuserauthclassg();
    } else {
        if(id != undefined && id != '') {  
            $.ajax({ 
                url: '?r=localdine/uploadphotoslocaldine',
                type: 'POST', 
                data: {id},
                success: function(data) {
                    $('#uploadphotosLocaldineModal').html(data);
                    setTimeout(function() { 
                        initDropdown();
                        $('#uploadphotosLocaldineModal').modal('open'); 
                    }, 400);
                }
            });
        }
    }
}

function uploadphotoslocaldinesave($id, obj) {  
  var validate = true;
  applypostloader('SHOW');
  var fd;
  fd = new FormData();
  for(var i=0, len=storedFiles.length; i<len; i++) {
      fd.append('images[]', storedFiles[i]);
  }
  fd.append('id', $id);

  if ($('#uploadphotosLocaldineModal').find('.custom-file').length) {
    validate = false;
    Materialize.toast('Please upload three cover photos.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  } else if ($('#uploadphotosLocaldineModal').find('.post-photos').find('.img-row').find('.img-box').length != 3) {
    validate = false;
    Materialize.toast('Please upload three cover photos.', 2000, 'red');
    applypostloader('HIDE');
    return false;
  }

  if(validate) {
    $.ajax({ 
      url: '?r=localdine/uploadphotoslocaldinesave',
      type: 'POST',
      data: fd,
      contentType: 'multipart/form-data',
      processData: false,
      contentType: false,
      success: function(result) {
        //applypostloader('HIDE');
         $result = JSON.parse(result);
         if($result.status != undefined && $result.status == true) {
          $('#uploadphotosLocaldineModal').modal('close'); 
            Materialize.toast('Localdine updated.', 2000, 'green');
            window.location.href="";
         }
      }
    });
  }
}

function deleteLocaldine(nid, isPermission=false){
  if(isPermission) {
    $.ajax({
      url: '?r=localdine/delete', 
      type: 'POST',
      data: {nid},
      success: function (data) {
        var result = $.parseJSON(data);
        if(result.status != undefined && result.status == true) {
          Materialize.toast('Local dine deleted', 2000, 'green');
          window.location.href="";
        }
      }
    });
  } else {
    var disText = $(".discard_md_modal .discard_modal_msg");
    var btnKeep = $(".discard_md_modal .modal_keep");
    var btnDiscard = $(".discard_md_modal .modal_discard");
    disText.html("Delete local dine.");
    btnKeep.html("Keep");
    btnDiscard.html("Delete");
    btnDiscard.attr('onclick', 'deleteLocaldine(\''+nid+'\', true)');
    $(".discard_md_modal").modal("open");
  }
}