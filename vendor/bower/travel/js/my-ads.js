/* Start Delete Ad Function */
function deleteAd(p_uid, pid, isPermission=false, isPermission1=false, isRefund=false, isCredit=false)
{
	var money = 18; 
	var refund = 22; 

	if(isPermission) { 
		if(isPermission1) {
			if(isRefund) {
				$.ajax({
					url: '?r=site/delete-post',
					type: 'POST',
					data: 'post_user_id=' + p_uid + "&pid=" + pid + "&refund=" + refund,
					success: function (data){
						$(".discard_md_modal").modal("close");
						Materialize.toast('Amount will be refunded shortley.', 2000, 'green');
						$('#hide_'+pid).remove();
					}
				});
			} else {
				$.ajax({
					url: '?r=site/delete-post',
					type: 'POST',
					data: 'post_user_id=' + p_uid + "&pid=" + pid + "&money=" + money,
					success: function (data){
						$(".discard_md_modal").modal("close");
						Materialize.toast('Amount has been credited to your account.', 2000, 'green');
						$('#hide_'+pid).remove();
					}
				});
			}
		} else {
			$(".discard_md_modal").modal("close");
	        setTimeout(function(){
			var disText = $(".discard_md_modal .discard_modal_msg");
		    var btnKeep = $(".discard_md_modal .modal_keep");
		    var btnDiscard = $(".discard_md_modal .modal_discard");
	    	disText.html("Please select one.");
	        btnKeep.html("Credit");
	        btnKeep.attr('onclick', 'deleteAd(\''+p_uid+'\', \''+pid+'\', true, true, false, true)');
	        btnDiscard.html("Refund");
	        btnDiscard.attr('onclick', 'deleteAd(\''+p_uid+'\', \''+pid+'\', true, true, true, false)');
	        	$(".discard_md_modal").modal("open");
	        },800);
		}	
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
    	disText.html("Deleting advert?");
        btnKeep.html("Keep");
        btnDiscard.html("Delete");
        btnDiscard.attr('onclick', 'deleteAd(\''+p_uid+'\', \''+pid+'\', true, false, false, false)');
        $(".discard_md_modal").modal("open");		
	}
}
/* End Delete Ad Function */

/* Start View Ads Detail Function */
function viewaddetails(adid)
{
	$(".adstats").html('');
	$.ajax({
		type: 'POST',
		url: '?r=ads/adstat',
		data: 'baseUrl='+baseUrl+'&adid='+adid,
		success: function(data)
		{
			$(".adstats").hide();
			$(".adstats").html(data);
			setTimeout(function() {
				$('.tabs').tabs();
				$(".adstats").show();
			}, 400);
		}
	});
}
/* End View Ads Detail Function */