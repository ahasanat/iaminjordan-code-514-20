$(document).on('keypress', '.materialize-textarea', function (e) {
	 var ids = $(this).attr('na');
		if(e.which === 13){
			var text = $(this).val();
			$.ajax({ 
				url: '?r=userwall/addreferalreply',  
				type: 'POST',
				data: "referal_id="+ids+'&text='+text,
				success: function(data)
				{
					$('.post_comment_'+ids).remove();
					$('.reply_'+ids).show();
					$('.reply_'+ids).html(data);
				}
			});
		}
});

function getNewVal(item)
{
    var category = item.value;
	var wall_user_id = "<?= $wall_user_id;?>";
	$.ajax({
        url: '?r=userwall/allreferal',  
        type: 'POST',
        data: "category="+category+"&wall_user_id="+wall_user_id,
        success: function(data)
        {
        	$("#tab-catreferal").html(data);
		}
    });
}

/*Connect Invite Code*/
function sendinvitereferal(user_id)
{
	if (user_id != '' )
	{
		$.ajax({
			type: 'POST',
			url: '?r=userwall/sendinvitereferal',
			data: "user_id="+user_id,
			success: function (data) {
				if (data)
				{
					$('.referal_invite_'+user_id).hide();
					$('.referal_invited_'+user_id).show();
				}
			}
		});
	}
}
/*Connect Search code*/
function Refer_Connect_Serach(){
	
	var search_val = $('#refral_connections').val();
	$.ajax({
			type: 'POST',
			url: '?r=userwall/searchreferalfrd',
			data: "search_val="+search_val,
			success: function (data) {
				if (data)
				{
					$('.refral_connections').html(data);
				}
			}
	});
	
}

/*Connect Search Event For Referal*/
$( document ).on( 'keyup', '#refral_connections', function(e) {
	Refer_Connect_Serach();
});


function delete_referencce(referal_id, isPermission=false)
{
	if(isPermission) {
		$.ajax({
			type: 'POST', 
			url: '?r=userwall/deletereferal',
			data: "referal_id="+referal_id,
			success: function (data) {
				$(".discard_md_modal").modal("close");
				if (data)
				{
					$('.dr_'+referal_id).hide();
					Materialize.toast('Deleted', 2000, 'green');
					referalCount();
				}
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
		var btnKeep = $(".discard_md_modal .modal_keep");
		var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Referal will be deleted permanently.");
		btnKeep.html("Keep");
		btnDiscard.html("Delete");
		btnDiscard.attr('onclick', 'delete_referencce(\''+referal_id+'\', true)');
		$(".discard_md_modal").modal("open");
	}
}

function getreferalcommentbox(postid) {
	if(postid) {
		$.ajax({
			type: 'POST',
			url: '?r=userwall/getreferalcommentbox',
			data: {postid},
			success: function (data) {
				if(data) {
					if($('#tab-catreferal').find('.post-holder.dr_'+postid).find('.post-data post_comment_'+postid).length) {
						$('#tab-catreferal').find('.post-holder.dr_'+postid).find('.post-data post_comment_'+postid).remove();
					}

					$('#tab-catreferal').find('.post-holder.dr_'+postid).append(data);
				}
			}
		});
	}
}

function replyDelete(main_id, referal_id, isPermission=false)
{
	if(isPermission) {
		$.ajax({
			type: 'POST',
			url: '?r=userwall/deletereferal',
			data: "referal_id="+main_id,
			success: function (data) {
				$(".discard_md_modal").modal("close");
				if (data) {
					$('.reply_'+referal_id).hide();
					$('.post_comment_'+referal_id).show();

					getreferalcommentbox(referal_id);

					Materialize.toast('Deleted', 2000, 'green');
				}
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
		var btnKeep = $(".discard_md_modal .modal_keep");
		var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Delete reply.");
		btnKeep.html("Keep");
		btnDiscard.html("Delete");
		btnDiscard.attr('onclick', 'replyDelete(\''+main_id+'\', \''+referal_id+'\', true)');
		$(".discard_md_modal").modal("open");
	}
}