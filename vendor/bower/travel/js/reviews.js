$(document).ready(function() {
	justifiedGalleryinitialize();
	lightGalleryinitialize();
});

$(document).ready(function(){ 
	$w = $(window).width();
	if ( $w > 739) {      
		$(".places-tabs .sub-tabs li a").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".tabs.icon-menu.tabsnew li a").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".mbl-tabnav").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".clicable.viewall-link").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
	} else {
		$(".places-tabs .sub-tabs li a").click(function(){
		   $("body").addClass("remove_scroller");
		}); 
		$(".clicable.viewall-link").click(function(){
		   $("body").addClass("remove_scroller");
		});         
		$(".tabs.icon-menu.tabsnew li a").click(function(){
		   $("body").addClass("remove_scroller");
		}); 
		$(".mbl-tabnav").click(function(){
		   $("body").removeClass("remove_scroller");
		});
	}

	$(".header-icon-tabs .tabsnew .tab a").click(function(){
		$(".bottom_tabs").hide();
	});

	$(".places-tabs .tab a").click(function(){
		$(".top_tabs").hide();
	});

	

	// footer work for places home page only
	$('.footer-section').css('left', '0');
	$w = $(window).width();
	if($w <= 768) {
		$('.main-footer').css({
		   'width': '100%',
		   'left': '0'
		});
	} else {
		var $_I = $('.places-content.places-all').width();
		var $__I = $('.places-content.places-all').find('.container').width();

		var $half = parseInt($_I) - parseInt($__I);
		$half = parseInt($half) / 2;

		$('.main-footer').css({
		   'width': $_I+'px',
		   'left': '-'+$half+'px'
		});
	}

	aboutplace(3);
});

$(window).resize(function() {
	

	if($('#places-all').hasClass('active')) {
		$('.footer-section').css('left', '0');
		$w = $(window).width();
		if($w <= 768) {
		   $('.main-footer').css({
		      'width': '100%',
		      'left': '0'
		   });
		} else {
		   var $_I = $('.places-content.places-all').width();
		   var $__I = $('.places-content.places-all').find('.container').width();

		   var $half = parseInt($_I) - parseInt($__I);
		   $half = parseInt($half) / 2;

		   $('.main-footer').css({
		      'width': $_I+'px',
		      'left': '-'+$half+'px'
		   });
		}
	}
});

$(document).on('click', '.compose_newreview', function (e) {
	if($(this).hasClass('checkuserauthclassnv')) {
		checkuserauthclassnv();
	} else if($(this).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
		selectStartPoint = '';
	    if(e.target.tagName == 'I') {
			var rate = $(e.target).data('value');
	    	$.ajax({
	            url: '?r=reviews/composenewreviewpopup',
	            success: function(data){
	                $('#compose_newreview').html(data);
	                setTimeout(function() { 
	                    initDropdown();
	                    $('.tabs').tabs();
	                    $('#compose_newreview').modal('open');
				            selectStartPoint = rate;
				            dummyStarHelp234IIII(rate);
	                }, 400);
	            }
	        });
	    } else {
	        //$(".profile_name").text(editperson[0].name);
	        $.ajax({
	            url: '?r=reviews/composenewreviewpopup',
	            success: function(data){
	                $('#compose_newreview').html(data);
	                setTimeout(function() { 
	                    initDropdown();
	                    $('.tabs').tabs();
	                    $('#compose_newreview').modal('open');
					        dummyStarHelp234IIII();
	                }, 400);
	            }
	        });
	    }
	}
});

$(document).on('click', '.tablist .tab a', function(e) {
	$href = $(this).attr('href');
	$href = $href.replace('#', '');

	$('.places-content').removeClass().addClass('places-content '+$href);

	
	$this = $(this);
});

function addPlaceReview(id) { 
	applypostloader('SHOW');
	var $select = $('#compose_newreview');
	var $w = $(window).width();
	var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
    var title = $select.find("#title").val();
    var status = $select.find('#textInput').val();
    
    if (status != "" && reg.test(status) == true) {
         status = status.replace(reg, " ");
    }
    if (title != "" && reg.test(title) == true) {
         title = title.replace(reg, " ");
    }

    var formdata;
    formdata = new FormData($('form')[1]);

    formdata.append("test", status);
    
    if($w > 568) {
    	$post_privacy = $select.find('#post_privacy').text().trim();
	} else {
		$post_privacy = $select.find('#post_privacy2').text().trim();
	}

    if($.inArray($post_privacy, $post_privacy_array) > -1) {
	} else {
		$post_privacy = 'Public';
	}
    var $share_setting = 'Enable';
    var $comment_setting = 'Enable';
    if($('#sharing_setting_btns').find('.toolbox_disable_sharing').is(':checked')) {
    	$share_setting = 'Disable';
    }
    if($('#sharing_setting_btns').find('.toolbox_disable_comments').is(':checked')) {
    	$comment_setting = 'Disable';
    }

    $link_title = $('#link_title').val();
    $link_url = $('#link_url').val();
    $link_description = $('#link_description').val();
    $link_image = $('#link_image').val();
   
   	var $totalStartFill = $select.find('.rating-stars').find('i.active').length;
	if($totalStartFill <=0) {
		Materialize.toast('Rate your self.', 2000, 'red');
		applypostloader('HIDE');
		return false;
	}

	$(".post_loadin_img").css("display","inline-block");
	
	formdata.append("posttags", addUserForTag);
	formdata.append("post_privacy", $post_privacy);
	formdata.append("link_title",$link_title);
	formdata.append("link_url",$link_url);
	formdata.append("link_description",$link_description);
	formdata.append("link_image",$link_image);
	formdata.append("title", title);
	formdata.append("share_setting",$share_setting);
	formdata.append("comment_setting",$comment_setting);
	formdata.append('custom', customArray);
	formdata.append("current_location",place); 
	formdata.append("placereview", $totalStartFill);
	$.ajax({
		url: '?r=reviews/add-place-review',
		type: 'POST',
		data:formdata,
		async:false,        
		processData: false,
		contentType: false,
		success: function(data) {
			//applypostloader('HIDE');
			$select.modal('close');
			if(data == 'checkuserauthclassnv') {
				checkuserauthclassnv();
			} else if(data == 'checkuserauthclassg') {
				checkuserauthclassg(); 
			} else {
				Materialize.toast('Published.', 2000, 'green');
				resetNewPost(id);	
				newct = 0;
				lastModified = [];
                storedFiles = [];
                storedFilesExsting = [];
                storedFiles.length = 0;
                storedFilesExsting.length = 0;

                if($('.nm-postlist').find('.joined-tb').length) {
                	$('.nm-postlist').find('.joined-tb').parents('.post-holder').remove();
                }

				$('.nm-postlist').prepend(data);
				setTimeout(function() { 
		            initDropdown();
		            lightGalleryinitialize();
		            //resizefixPostImages();
		        }, 400);
			}	
		}
	});
	return true;
}

function deletePlaceReview(p_uid, pid, isPermission=false){
	if(isPermission) {
		$.ajax({
			url: '?r=reviews/delete-place-review',
			type: 'POST',
			data: 'post_user_id=' + p_uid + "&pid=" + pid,
			success: function (data){
				$(".discard_md_modal").modal("close");
				$('#hide_'+pid).remove(); 
				var $totalbox = $('.nm-postlist').find('.bborder.post-holder').length;
				if($totalbox <= 0) {
					$('.nm-postlist').html('<div class="post-holder bshadow"><div class="joined-tb"> <i class="mdi mdi-file-outline"></i> <p>Become a first to review for this place</p> </div></div>');
				}

				Materialize.toast('Deleted.', 2000, 'green');
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Delete post?");
        btnKeep.html("Keep");
        btnDiscard.html("Delete");
        btnDiscard.attr('onclick', 'deletePlaceReview(\''+p_uid+'\', \''+pid+'\', true)');
        $(".discard_md_modal").modal("open");
	}
}

function edit_place_review(pid) {  
	if (pid != '') {
		applypostloader('SHOW');
		var $select = $('#compose_newreview');
		var $w = $(window).width();
		var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
	    var title = $select.find("input.title").val();
	    var status = $select.find('#textInput').val();
	    
	    if (status != "" && reg.test(status) == true) {
	         status = status.replace(reg, " ");
	    }
	    if (title != "" && reg.test(title) == true) {
	         title = title.replace(reg, " ");
	    }

	    var formdata;
	    formdata = new FormData($('form')[1]);
	   
	    formdata.append("test", status);
	    
	    if($w > 568) {
	    	$post_privacy = $select.find('#post_privacy').text().trim();
		} else {
			$post_privacy = $select.find('#post_privacy2').text().trim();
		}

	    if($.inArray($post_privacy, $post_privacy_array) > -1) {
		} else {
			$post_privacy = 'Public';
		}
	    var $share_setting = 'Enable';
	    var $comment_setting = 'Enable';
	    if($('#sharing_setting_btns').find('.toolbox_disable_sharing').is(':checked')) {
	    	$share_setting = 'Disable';
	    }
	    if($('#sharing_setting_btns').find('.toolbox_disable_comments').is(':checked')) {
	    	$comment_setting = 'Disable';
	    }

	    $link_title = $('#link_title').val();
	    $link_url = $('#link_url').val();
	    $link_description = $('#link_description').val();
	    $link_image = $('#link_image').val();
	   
	   	var $totalStartFill = $select.find('.rating-stars').find('i.active').length;
		if($totalStartFill <=0) {
			Materialize.toast('Rate your self.', 2000, 'red');
			applypostloader('HIDE');
			return false;
		}

		$(".post_loadin_img").css("display","inline-block");
		
		formdata.append("posttags", addUserForTag);
		formdata.append("post_privacy", $post_privacy);
		formdata.append("link_title",$link_title);
		formdata.append("link_url",$link_url);
		formdata.append("link_description",$link_description);
		formdata.append("link_image",$link_image);
		formdata.append("title", title);
		formdata.append("share_setting",$share_setting);
		formdata.append("comment_setting",$comment_setting);
		formdata.append('custom', customArray);
		formdata.append("current_location",place); 
		formdata.append("placereview", $totalStartFill);
		formdata.append("pid", pid);
		
		$.ajax({
			type: 'POST',
			url: '?r=reviews/edit-place-review',
			data: formdata,
			processData: false,
			contentType: false,
			success: function (data) {
				//applypostloader('HIDE');
				$select.modal('close');
				Materialize.toast('Published.', 2000, 'green');
				newct = 0;
				lastModified = [];
                storedFiles = [];
                storedFilesExsting = [];
                storedFiles.length = 0;
                storedFilesExsting.length = 0;
				$('#hide_'+pid).remove();
				$(".nm-postlist").prepend(data);
				setTimeout(function() { 
		            initDropdown();
		            justifiedGalleryinitialize();
					lightGalleryinitialize();
		   			//resizefixPostImages();
		        }, 400);
			}
		});
	}
}