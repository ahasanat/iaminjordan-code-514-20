/* document ready */
	$(document).ready(function() {
		/************ PAGES FUNCTIONS ************/
			allPages('load');
			//$("#pages-suggested").animateCss('fadeInUp');

		    $(".allpages").click(function() {
		        allPages();
		    });
		    $(".likedpages").click(function() {
		        likedPages();
		    });
		    $(".mypages").click(function() {
		        myPages();
		    });
		    
			var cropper = $('.cropper');
			if (cropper.length) {
			  $.each(cropper, function(i, val) {
			    var uploadCrop;
		                             
			    var readFile = function(input) {
			      if (input.files && input.files[i]) {
			        var reader = new FileReader();

			        reader.onload = function(e) {
			          uploadCrop.croppie('bind', {
			            url: e.target.result
			          });
			        };

			        reader.readAsDataURL(input.files[i]);
			      } else {
			        alert("Sorry - you're browser doesn't support the FileReader API");
			      }
			    };

			    uploadCrop = $('.js-cropping').croppie({ // TODO: fix so its selects right element
			      viewport: { 
			        width: 200,
			        height: 200
			      },
			      boundary: {
			        width: 350,
			        height: 355
			      },
			      enableOrientation: true
			    });

			    $('.js-cropper-upload').on('change', function() {
			      $('.crop').show(); // TODO: fix so its selects right element
			      readFile(this);
			    });
			    
			    $('.js-cropper-rotate--btn').on('click', function(ev) {
			      uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
			    });

			    $('.img-cancel-btn').on('click', function(ev) {
			      $('#compose_camera').modal('close');
			    });

			    $('.js-cropper-result--btn').on('click', function(ev) {
			      uploadCrop.croppie('result', 'canvas').then(function(resp) {
			        popupResult({
			          src: resp
			        });
			      });
			    });

			    var popupResult = function(result) {
			    	if(result.src != undefined && result.src != '') {
			    		$.ajax({
						    type: 'POST',
						    url: "?r=page/page-image-crop2",
						    data: {
						    	file: result.src
						   	},
						    success: function (data) {
						    	window.location.href='';
						    }
						});
					}
			    };
			  });
			}

			$('.new-tumb').carousel({
				dist:0,
				shift:0,
				padding:20,
			});

		    $('.carousel_master .carousel').carousel();
			var owl = $('.owl-carousel');
			owl.owlCarousel({
				margin: 10,
				loop: false,		
				dots: true,			
				nav: true,
				responsive: {
				  0: {
					items: 1
				  },
				  600: {
					items: 2
				  },
				  1000: {
					items: 3
				  }
				}
			});

			$(".custom-wall-links .tab a").click(function(){
				$(".activity-content").removeClass("showactivity");
			});
			$("#upload_img_action").click(function(){
				$(".cover-slider").removeClass("openDrawer");			
			});		 

			$(document).on('click', '.covercropbtn', function() {
				var images = $('#image-cropper').cropit('export');
				if(images != '') {
					$.ajax({ 
						url: '?r=page/page-image-crop', 
						type: 'POST',
						data: {images},
						success: function (data) {
							window.location.href='';
						}
					});
				}  

			});

			$(document).on('click', '#carousel .carousel-item', function(e) {
				if($(this).find('img').length) {
					$imgSrc = $(this).find('img').attr('src');
					if($imgSrc != '') {
						$.ajax({ 
							url: '?r=page/directsetcover', 
							type: 'POST',
							data: {$imgSrc},
							success: function (data) {
								Materialize.toast('Profile cover changed.', 2000, 'green');
								window.location.href='';
							}
						});
					}
				}
			});

			$(document).on('click', '.compose_newreview', function () {
				if($(this).hasClass('checkuserauthclassnv')) {
					checkuserauthclassnv();
				} else if($(this).hasClass('checkuserauthclassg')) {
					checkuserauthclassg();
				} else {
			        $.ajax({ 
			            url: '?r=page/bussinesspagereview',
			            success: function(data){
			                $('#compose_newreview').html(data);
			                setTimeout(function() { 
			                    initDropdown();
			                    $('.tabs').tabs(); 
			                    $('#compose_newreview').modal('open');
			                }, 400); 
			            }
			        });
			    }
		    });

			/* pages page  : vertical tabs manage - create page : mobile dropdown */
			$("body").on("click", ".pages-page .vertical-tabs .nav-tabs li a", function(e){
					
				var sparent = $(this).parents(".tabs-list");
				var ref = $(this).attr("href");
				var text = $(this).html();
				
				if($(this).parents(".dropdown").length > 0){
					resetPageCreateTabs();
					$(".pages-page .vertical-tabs .text-menu").find("a[href='"+ref+"']").parents("li").addClass("active");
				}else{			
					
					sparent.find(".dropdown").find('.dropdown-toggle').html(text+ " <span class='caret'></span>");
					sparent.find(".dropdown").find('.dropdown-toggle').val(text);
				}
			});
			/* end pages page  : vertical tabs manage - create page : mobile dropdown */
			
			/* manage create page - vertical tab dropdown responsive */
				$("body").on("click", ".createform .text-menu .nav-tabs li", function(e){			
					var thisHref = $(this).find("a").attr("href");				
					$(".createform .mbl-menu .nav-tabs li a").filter("[href='" + thisHref + "']").trigger("click");
				});
			/* end manage create page - vertical tab dropdown responsive */
			
			/* Start Rating From Page*/ 
			$("body").on("click", ".postreview", function(){
				var col = $(this).closest('form').attr('id');
				var getparntcls = $("#"+col).parent().attr('id');
				if(getparntcls)
				{
					addReview("#"+getparntcls);
				}
			})
			$("body").on("click", ".popup-postreview", function(){		
				var col = $(this).parents('.popup-content');
				var getparntcls = col.find(".new-post").attr('id');
				if(getparntcls) {
					addReview("#"+getparntcls);
				}
						
			})
			/* End Rating From Page*/
				
			/* Start Connect Search From Page*/
			 $( document ).on( 'keyup', '.page_search', function(e) {
				var id = $(this).attr('data-id');
				var textValue = $(this).val();
				var baseurl = $("#baseurl").val();
				data = 'key='+textValue+'&baseurl='+baseurl;
				$.ajax({
					url: "?r=page/search-pages", 
					type: 'GET',
					data: data,
					success: function (data) {				
						$("."+id).find('.row').html(data).show();
					}
				});
			 });
			 /* End Connect Search From Page*/
			 
			$(document).on('click', '.compose_review', function (e) {
		        selectStartPoint = '';
		        if(e.target.tagName == 'I') {
		        	var rate = $(e.target).data('value');
		            selectStartPoint = rate; 
		            $.ajax({ 
				        url: '?r=page/bussinesspagereview',
				        success: function(data){
				            $('#compose_newreview').html(data);
				            setTimeout(function() { 
				                initDropdown(); 
				                $('.tabs').tabs(); 
				                $('#compose_newreview').modal('open');
				                dummyStarHelp234IIII(selectStartPoint);
				            }, 400); 
				        }
				    });
		        } else {
		            $(".profile_name").text(editperson[0].name);
		            $.ajax({ 
				        url: '?r=page/bussinesspagereview',
				        success: function(data){
				            $('#compose_newreview').html(data);
				            setTimeout(function() { 
				                initDropdown();
				                $('.tabs').tabs(); 
				                $('#compose_newreview').modal('open');
		                		dummyStarHelp234IIII();
				            }, 400); 
				        }
				    });
		        }
		    });

		    $('#image-cropper').cropit({
		        height: 188,
		        width: 355,
		        smallImage: "allow",
		        onImageLoaded: function (x) {
		            
		            $(".cropit-preview").removeClass("class-hide");
		            $(".cropit-preview").addClass("class-show");

		            $(".btn-save").removeClass("class-hide");
		            $(".btn-save").addClass("class-show");
		            $("#removeimg").removeClass("class-hide");
		            $("#removeimg").addClass("class-show");
		        }
		    });
		/************ END PAGES FUNCTIONS ************/
	});
/* end document ready */
	
/* START Design Code */
function likepage(pid) {
	$.ajax({
	  url: '?r=page/like-page',
	  type: 'POST',
	  data: 'page_id=' + pid,
	  success: function (data) 
		{
		}	
	});
}

/* FUN mark notification as read */
	function markNotRead(obj,nid){
		var sparent=$(obj).parents("li"); 
		if(nid != '')
		{
			$.ajax({
				url: '?r=notification/markasread',
				type: 'POST',
				data: 'nid=' + nid,
				success: function (data)
				{
					if(data)
					{
						sparent.addClass("read");
						if($(obj).parents(".noti-listing").hasClass("page-activity")){
							$(obj).attr("onclick","markRead(this)");
							$(obj).attr("title","Mark as unread");
						}
					}   
				}
			});
		}
	}
/* FUN end mark notification as read */

	function open_edit(obj) {
		addUserForAccountSettingsArray = [];
		addUserForAccountSettingsArrayTemp = [];
		close_all_edit();

		var obj=$(obj).parents(".settings-group");
				
		var editmode=obj.children(".edit-mode").css("display");
                  
		if(editmode=="none"){
			obj.children(".normal-mode").slideUp(300);
			obj.children(".edit-mode").slideDown(300);
		}
		else{
			obj.children(".normal-mode").slideDown(300);
			obj.children(".edit-mode").slideUp(300);
		}
		
		initDropdown();
		/*setTimeout(function(){
			setGeneralThings();
		},400);*/
	}

	function close_all_edit(){
	   var count=0;
	   $(".settings-ul .settings-group").each(function(){
			     
		   var editmode=$(this).children(".edit-mode").css("display");		
		
		   if(editmode!="none"){
				
				$(this).children(".normal-mode").slideDown(300);
				$(this).children(".edit-mode").slideUp(300);
			}
	   });
	}
	
	function close_edit(obj) {
		if($(obj).parents('li').find('.getvalue').length) {
		   var name = $(obj).data('nm');
		   if(name != undefined && name != '') {
			var lispan = $(obj).parents('li').find('.getvalue').html().trim();
			$('.'+name+'li .edit-mode #'+name).val(lispan);
			setting(); 
			$('.'+name+'li .normal-mode .row .security-setting.'+name+'display').html(lispan);
		   } /*else {
			setting();
		   }*/
		  }


		var objParent=$(obj).parents(".settings-group");
		var emode=objParent.children(".edit-mode");
		var nmode=objParent.children(".normal-mode");
		var editmode=emode.css("display");

		if(editmode=="none") {
		nmode.slideUp(300);
		emode.slideDown(300);
		} else {
		nmode.slideDown(300);
		emode.slideUp(300);
		}
	}

/* create page : mobile dropdown */
	function resetPageCreateTabs(){
		$(".pages-page .vertical-tabs .text-menu ul.nav-tabs li").each(function(){
			$(this).removeClass("active");
		});
	}
/* end create page : mobile dropdown */

/* tab box : tabs navigation */
	function textCaps(type) {
		var adtype = $("#"+type).val();
		adtype = adtype.charAt(0).toUpperCase() + adtype.slice(1);   
		$("#"+type).val(adtype);
	}
	
	function createpage()
	{
		var pageCatDrop = $("#pageCatDrop1").val();
		var pagename = $("#page_title").val();
		var pageshort = $("#pageshort1").val();
		var pagedesc = $("#pagedesc1").val(); 
		var pagesite = $("#pagesite1").val();
		var pageid = $("#pageid").val();
		var bustown = $("#placelocationsearch").val();
		var busemail = $("#busemail1").val();
		var pagesitere = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;

		if(pageCatDrop == '')
		{
			Materialize.toast('Please select page services', 2000, 'red');
			$("#pageCatDrop").focus(); 
			return false;
		}
		if(pagename == '')
		{
			Materialize.toast('Page title', 2000, 'red');
			$("#page_title").focus();
			return false;
		}
		if(pagesite != '' && !pagesitere.test(pagesite))
		{
			Materialize.toast('Proper bussness web site', 2000, 'red');
			$("#pagesite1").focus();
			return false;
		}
		
		if(pageshort == '')
		{
			Materialize.toast('Short description', 2000, 'red');
			$("#pageshort1").focus();
			return false;
		}
		if(bustown == '')
		{
			Materialize.toast('Business address', 2000, 'red');
			$("#placelocationsearch").focus(); 
			return false;
		}
		if(busemail == '')
		{
			Materialize.toast('Business email id', 2000, 'red');
			$("#busemail1").focus();
			return false;
		}
		
		if(!document.getElementById("agreeemailpage1").checked)
		{
			Materialize.toast('Please ticked on verify email', 2000, 'red');
			$("#busemail1").focus();
			return false;
		}
		
		var formdata;
		formdata = new FormData();
		formdata.append("pageCatDrop", pageCatDrop);
		formdata.append("pagename", pagename);
		formdata.append("pageshort", pageshort);
		formdata.append("pagedesc", pagedesc);
		formdata.append("pagesite", pagesite);
		formdata.append("pageid", pageid);  
		formdata.append("bustown", bustown);
		formdata.append("busemail", busemail);
		$('#add_page_modal').modal('close');
		$.ajax({
			url: '?r=page/createpage', 
			type: 'POST',
			data: formdata,
			processData: false,
			contentType: false,
			success: function (data)
			{
				var result = $.parseJSON(data);
				if(result.auth == 'checkuserauthclassnv') {
					checkuserauthclassnv();  
				} 
				else if(result.auth == 'checkuserauthclassg') {
					checkuserauthclassg();
				} 
				else {
					if(result['msg'] == 'success')
					{
						Materialize.toast('Page created.', 2000, 'green');
						var page_id = result['page_id'];
						$("#pageid").val(page_id);
						likepage(page_id);
						VerifyPageMail(busemail,page_id);
					}
				}
			}
		});
	}
	
	function VerifyPageMail(verifyemailid,pageid)
	{
		var formdata;
		formdata = new FormData(); 
		formdata.append("verifyemailid", verifyemailid);
		formdata.append("pageid", pageid);

		$.ajax({
			url: '?r=page/verifyemail', 
			type: 'POST',
			data: formdata,
			processData: false,
			contentType: false,
			success: function (data) {
				window.location.href = "?r=page/detail&id="+pageid;
			}
		});
	}
	
	function navigateTabs(){ 
		var status = $("#user_status").val();
		if(status == 0)
		{  
			return false;
		}

			var pageCatDrop = $("#pageCatDrop").val();
			var pagename = $("#pagename").val();
			var pageshort = $("#pageshort").val();
			var pagedesc = $("#pagedesc").val(); 
			var pagesite = $("#pagesite").val();
			var pageid = $("#pageid").val();
			
			var pagesitere = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;

			if(pageCatDrop == '')
			{
				Materialize.toast('Select category.', 2000, 'red');
				$("#email").focus(); 
				return false;
			}
			if(pagename == '')
			{
				Materialize.toast('Enter page name.', 2000, 'red');
				$("#email").focus();
				return false;
			}
			if(pagesite != '' && !pagesitere.test(pagesite))
			{
				Materialize.toast('Enter valid website.', 2000, 'red');
				$("#email").focus();
				return false;
			}

			var formdata;
			formdata = new FormData();
			formdata.append("pageCatDrop", pageCatDrop);
			formdata.append("pagename", pagename);
			formdata.append("pageshort", pageshort);
			formdata.append("pagedesc", pagedesc);
			formdata.append("pagesite", pagesite);
			formdata.append("pageid", pageid);

			$.ajax({
				url: '?r=page/createpage', 
				type: 'POST',
				data: formdata,
				processData: false,
				contentType: false,
				success: function (data)
				{
					var result = $.parseJSON(data);

					if(result.auth == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} 
					else if(result.auth == 'checkuserauthclassg') {
						checkuserauthclassg();
					} 
					else {
						var result = $.parseJSON(data);
						if(result['msg'] == 'success')
						{
							var page_id = result['page_id'];
							$("#pageid").val(page_id);
							
							likepage(page_id);
						}
						else
						{
						}
					}
				}
			});
		
		
			var busaddress = $("#busaddress").val();
			var bustown = $("#autocomplete").val();
			var buscode = $("#buscode").val();
			var busemail = $("#busemail").val();
			var busphone = $("#busphone").val();
			var busstart = $("#datepicker").val();
			var bustype = $("#bustype").val();
			var pageid = $("#pageid").val();
			var country_code = $("#country_code").val();

			var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
			var ptn = /([0-9\s\-]{1,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;

			if(busaddress == '')
			{
				Materialize.toast('Enter business address.', 2000, 'red');
				$("#busaddress").focus();
				return false;
			}
			if(bustown == '')
			{
				Materialize.toast('Enter business city.', 2000, 'red');
				$("#bustown").focus();
				return false;
			}
			if(busemail == '')
			{
				Materialize.toast('Enter business email.', 2000, 'red');
				$("#busemail").focus();
				return false;
			}
			if(busemail != '' && ! pattern.test(busemail))
			{
				Materialize.toast('Enter valid business email.', 2000, 'red');
				$("#busemail").focus();
				return false;
			}
			if(busphone == '')
			{
				Materialize.toast('Enter business phone.', 2000, 'red');
				$("#busphone").focus();
				return false;
			}
			if(busphone != '' && ! ptn.test(busphone))
			{
				Materialize.toast('Enter valid business phone.', 2000, 'red');
				$("#busphone").focus();
				return false;
			}

			var formdata;
			formdata = new FormData();
			formdata.append("busaddress", busaddress);
			formdata.append("bustown", bustown);
			formdata.append("buscode", buscode);
			formdata.append("busemail", busemail);
			formdata.append("busphone", busphone);
			formdata.append("busstart", busstart);
			formdata.append("bustype", 'default');
			formdata.append("pageid", pageid);
			formdata.append("country_code", country_code);

			$.ajax({
				url: '?r=page/updatebusinessdetails', 
				type: 'POST',
				data: formdata,
				processData: false,
				contentType: false,
				success: function (data)
				{
					var result = $.parseJSON(data);
					if(result['msg'] == 'success')
					{
						$("#verifyemailid").val(busemail);
					}
					else
					{
						Materialize.toast('Start with first step of create page.', 2000, 'red');
						return false;
					}
				}
			});
		
		
			var verifyemailid = $("#verifyemailid").val();
			var pageid = $("#pageid").val();

			var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
			
			if(!document.getElementById("agreeemailpage").checked)
			{
				Materialize.toast('Ticked on verify email.', 2000, 'red');
				return false;
			}
			if(verifyemailid == '')
			{
				Materialize.toast('Enter company email.', 2000, 'red');
				$("#verifyemailid").focus();
				return false;
			}
			if(verifyemailid != '' && ! pattern.test(verifyemailid))
			{
				Materialize.toast('Enter valid company email.', 2000, 'red');
				$("#verifyemailid").focus();
				return false;
			}
			if(!document.getElementById("pagetncagreed").checked)
			{
				Materialize.toast('Agreed to the terms.', 2000, 'red');
				return false;
			}

			var formdata;
			formdata = new FormData();
			formdata.append("verifyemailid", verifyemailid);
			formdata.append("pageid", pageid);

			$.ajax({
				url: '?r=page/verifyemail', 
				type: 'POST',
				data: formdata,
				processData: false,
				contentType: false,
				success: function (data)
				{
					if(data)
					{
						Materialize.toast('Verify your page from your email.', 2000, 'green');
						return false;
					}
					else
					{
						Materialize.toast('Start with first step of create page.', 2000, 'red');
						return false;
					}
				}
			});
			
			window.location.href = "?r=page/detail&id="+pageid;
			
	}	
/* end tab box : tabs navigation */

/* FUN like / unlike page */	
	function doPageLike(event,col_id,obj){
		
		var isNoClick=$(event.target).hasClass("noClick");

		event.stopPropagation();
		if(isNoClick)
		{
		   // Follow clicked
		   var stat=$(obj).html();

			if(stat=="Like"){
				// start following
				$(obj).html("Liked");
				$(obj).addClass("disabled");
			}else{
				// do unfollow
				$(obj).html("Like");
				$(obj).removeClass("disabled");
			}
		}
	}
/* FUN end like / unlike page */

/* FUN start like page function */
	function recentPageLikeBox(obj) {
		$this = $(obj);
		var $pageid = $($this).attr('data-pageid');
		if($pageid) {
			$.ajax({
			  	url: '?r=page/like-page', 
			  	type: 'POST',
			  	data: 'page_id=' + $pageid,
			  	success: function (data) {
					if(data == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} else if(data == 'checkuserauthclassg') {
						checkuserauthclassg();
					} else {
						var result = $.parseJSON(data);
						var count =  result.like_count;

						if(count > 0) {
                            if(count > 1) {
                                $($this).html(count +' Likes');
                            } else {
                            	$($this).html(count +' Like');
                            }
                        } else {
                        	$($this).html('&nbsp;');
                        } 
					}
				}
			});
		}
	}

	function pageLike(event,pid,obj){
		var htmlContent = $(obj).html();
		var result = htmlContent.match(/Liked/i);
		var status = $("#page_status").val();
		if(status == 0){
			return false;
		}
	
		var isNoClick=$(event.target).hasClass("noClick");
	    event.stopPropagation();
	    if(isNoClick) {
		  	if(pid) {
				$.ajax({
				  	url: '?r=page/like-page', 
				  	type: 'POST',
				  	data: 'page_id=' + pid,
				  	success: function (data) {
						if(data == 'checkuserauthclassnv') {
							checkuserauthclassnv();
						} else if(data == 'checkuserauthclassg') {
							checkuserauthclassg();
						} else {
						var result = $.parseJSON(data);
						var likestatus =  result.status;
						var names =  result.names;
						var count =  result.like_count;
						var cou = 'Like ';
						if(count > 0)
						{
							cou = ''+count+' liked this page';
							names = ''+names+' liked this page';
						}
						if(count == 0)
						{
							names = cou = 'Become a first to like this page';
						}
						if(likestatus == 1)
						{
							$(".likestatus_"+pid).html('Liked');
							$(".likestatus_"+pid).addClass("disabled");
							$('.likeaction').attr('title','Liked');
							$('.likeaction').addClass('active');
							if(count >= 1) {
								$(obj).parents('.col').find('.pagelikecounteer').html(count+' Likes');
							} else {
								$(obj).parents('.col').find('.pagelikecounteer').html('Likes');
							}
							Materialize.toast('Page liked', 2000, 'green');
						}
						if(likestatus == 0)
						{
							$(".likestatus_"+pid).html('Like');
							$(".likestatus_"+pid).removeClass("disabled");
							$('.likeaction').attr('title','Like');
							$('.likeaction').removeClass('active');
							if(count >= 1){
								$(obj).parents('.col').find('.pagelikecounteer').html(count+' Likes');
							}
							else
							{
								$(obj).parents('.col').find('.pagelikecounteer').html('Likes');
							}
							Materialize.toast('Page disliked', 2000, 'black');
						} 
						$(".likecount_"+pid).html(cou);
						$('.liketitle_'+pid).html(names);
					  }
					}	
				});
		 	}
		} else {
		   window.location.href = "?r=page/detail&id="+pid;
	    }		
	}
/* FUN end like page function */

/* FUN start page invite for like functions */
	function cancelinvite(fid){
		$('.invite_'+fid).hide();
	}
	function sendinvite(fid,pid){
		var status = $("#page_status").val();
		if(status == 0){
			return false;
		}
		if (fid != '' && pid != '')
		{
			$.ajax({
				type: 'POST',
				url: '?r=page/sendinvite',
				data: "fid="+fid+"&pid="+pid,
				success: function (data) {
					if (data)
					{
						$('.events_'+fid).hide();
						$('.sendinvitation_'+fid).show();
					}
				}
			});
		}
	}
/* FUN end page invite for like functions */

/* FUN crop popup function */
	function cropPageChange(obj) {
		var file = $('#uploadPagePhoto').prop("files");
		var msg='';
		if (file.length >0) {
			var nm = file[0].name;
			var ext = nm.substr(nm.lastIndexOf('.') +1).toLowerCase();
			var _URL = window.URL || window.webkitURL;

			if($.inArray(ext, ['gif','png','jpg','jpeg','tif']) == -1) {
				Materialize.toast('Upload photo allow only (jpg / gif / jpeg / png / tif) image extension.', 2000, 'red');
			} else {
				img = new Image();
				img.onload = function () {
					if(img.width >= 150 && img.height >= 150) {
						$( ".cropControlCrop" ).trigger( "click" );
						navigateTabs('pagescreate-contact', obj);
						return false;
					} else {
						Materialize.toast('Your photo must be at least 150x150 pixels.', 2000, 'red');
					}
				};
				img.src = _URL.createObjectURL(file[0]);
			}
		} else {
			Materialize.toast('Upload profile picture.', 2000, 'red');
		}
		return false;				
	}	
/* FUN end crop popup function */

/* END Design Code */

function allPages($isload='')
{ 
    $.ajax({
        url: '?r=page/allpages',  
        type: 'POST',
        data: "baseUrl="+baseUrl,
        success: function(data)
        {
            if($isload == 'load') {
            	$("#pages-suggested").animateCss('fadeInUp');
            	$("#pages-suggested").find('.page_search').addClass('animated fadeInUp');
        	}
            $("#pages-suggested").find('.row').html(data);
            setTimeout(function() { 
            	gridBoxImgUINew('#pages-suggested .gridBox127');
            }, 400);
        }
    });
}
/* Start Like Page Function */
function likedPages()
{
    $.ajax({
        url: '?r=page/likedpages',  
        type: 'POST',
        data: "baseUrl="+baseUrl,
        success: function(data)
        {
            $("#pages-liked").find('.row').html(data);
            setTimeout(function() { 
            	gridBoxImgUINew('#pages-liked .gridBox127');
            }, 400);
        }
    });
}
/* End Like Page Function */

/* Start View All My Pages Function */
function myPages()
{
   $.ajax({
        url: '?r=page/mypages',  
        type: 'POST',
        data: "baseUrl="+baseUrl, 
        success: function(data)
        {
            $("#pages-yours").find('.row').html(data);
            $(".createpage").click(function() {
                createPage();
            });
            $(".promotepages").click(function() {
                promotePages();
            });

            gridBoxImgUINew('#pages-yours .gridBox127');

			if($('#uploadPagePhoto').length) {
	            document.getElementById('uploadPagePhoto').onchange = function (evt)
	            {
	                var tgt = evt.target || window.event.srcElement,
	                files = tgt.files;
	                var fr = new FileReader(); 
	                fr.onload = function (e)
	                {   
	                	var croppicContainerPreloadOptions;
						if($(".page-wrapper").hasClass("settings-wrapper")){
							croppicContainerPreloadOptions = {
								cropUrl:'?r=site/profile-image-crop',
								loadPicture:e.target.result,		 
								enableMousescroll:true,
								onBeforeImgUpload: function(){ $('#profCrop').html(''); },
								onAfterImgCrop:function(){ location.reload();},		 
							}			
						}else{
							croppicContainerPreloadOptions = {
								cropUrl:'?r=page/page-image-crop',
								loadPicture:e.target.result,
								enableMousescroll:true
							}
						}
	                }
	                fr.readAsDataURL(files[0]);	
	            }
	        }
        }
    });
}
