/* page load event handler */
$(document).ready(function(){
	/* listing all trips */
	alltrip();
	$('.alltrip').click(function() {
		$('.trip-results').hide();
		/* listing all trips */
		alltrip();
	});
	$('.yourtrip').click(function() {
		$('.trip-results').hide();
		/* listing all your trips */
		yourtrip();
	});
	google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('trip_cur_loc'));
    });

    aboutplace(3);

/*	$(document).ready(function() {
		justifiedGalleryinitialize();
		lightGalleryinitialize();
	});*/
})

/* location search key press event handler */
$("#trip_cur_loc").keypress(function(e) {
    if(e.which == 13) {
   		callSearchLog();	
    }
});

/* continent */
function continent(trip){
	$.ajax({
        url: '?r=tripstory/continent', 
        type: 'POST',
        data: "baseUrl="+baseUrl+"&trip="+trip,
        success: function(data) {
            $(".continent").html(data);
		}
    });
}

/* search log calling */
function callSearchLog() {
	var textValue = $('#trip_cur_loc').val();
	var tabname = $('.c_click').attr('tabname');
	if(tabname == 'alltrip'){
		alltrip('undefined',textValue);
	}else if(tabname == 'yourtrip'){
		yourtrip('undefined',textValue);
	}
}

/* listing all trips */
function alltrip(country,searchval){
	$.ajax({
        url: '?r=tripstory/alltrip',
        type: 'POST',
        data: "baseUrl="+baseUrl+"&country="+country+"&searchval="+searchval,
        success: function(data) {
        	if(country == undefined || country == 'undefined') {
        		continent('yourtrip');
        	}
        	$("#tripstory-all").html(data);
		    $('.c_click').attr('tabname','alltrip');
		  	setTimeout(function(){
		  		fixImageUI();
		  		justifiedGalleryinitialize();
				lightGalleryinitialize();
		  	},300);
		    if($('#searcharea1').hasClass('expanded')) {
				$('#searchformforrecenttrips')[0].reset();
				$('#searcharea1').removeClass('expanded');
			}
		}
	}).done(function(){
       setTimeout(function(){initDropdown(); },500);
    });
}

/* listing your trip */
function yourtrip(country,searchval){
	$('.searchtrip').val('');
    $.ajax({
        url: '?r=tripstory/yourtrip', 
        type: 'POST',
        data: "baseUrl="+baseUrl+"&country="+country+"&searchval="+searchval,
        success: function(data) {
        	if(country == undefined || country == 'undefined') {
        		continent('yourtrip');
        	}
			$("#yourtripdata").html(data);
			$('.c_click').attr('tabname','yourtrip');
			setTimeout(function(){
				fixImageUI();
				justifiedGalleryinitialize();
				lightGalleryinitialize();
			},300);
			if($('#searcharea1').hasClass('expanded')) {
				$('#searchformforrecenttrips')[0].reset();
				$('#searcharea1').removeClass('expanded');
			}
		}
		
    }).done(function(){
           setTimeout(function(){initDropdown(); },500);
      });
}

/* list all trips */
function country_trip(country,count){
	$('.trip-results').show();
	$('.trip_count').html(count);
	$('.trip_c').html(country);
	var tabname = $('.c_click').attr('tabname');
	if(tabname == 'alltrip'){
		alltrip(country);
	}else if(tabname == 'yourtrip'){
		yourtrip(country);
	}
}