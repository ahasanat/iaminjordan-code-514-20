<?php

class HTMLPurifier_Injector_LinkifyTest extends HTMLPurifier_InjectorHarness
{

    public function setup()
    {
        parent::setup();
        $this->config->set('AutoFormat.Linkify', true);
    }

    public function testLinkifyURLInRootNode()
    {
        $this->assertResult(
            'https://example.com',
            '<a href="https://example.com">https://example.com</a>'
        );
    }

    public function testLinkifyURLInInlineNode()
    {
        $this->assertResult(
            '<b>https://example.com</b>',
            '<b><a href="https://example.com">https://example.com</a></b>'
        );
    }

    public function testBasicUsageCase()
    {
        $this->assertResult(
            'This URL https://example.com is what you need',
            'This URL <a href="https://example.com">https://example.com</a> is what you need'
        );
    }

    public function testIgnoreURLInATag()
    {
        $this->assertResult(
            '<a>https://example.com/</a>'
        );
    }

    public function testNeeded()
    {
        $this->config->set('HTML.Allowed', 'b');
        $this->expectError('Cannot enable Linkify injector because a is not allowed');
        $this->assertResult('https://example.com/');
    }

    public function testExcludes()
    {
        $this->assertResult('<a><span>https://example.com</span></a>');
    }

}

// vim: et sw=4 sts=4
